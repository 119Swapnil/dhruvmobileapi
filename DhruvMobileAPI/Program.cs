﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.IO;

namespace DhruvMobileAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug("init main function");
                CreateWebHostBuilder(args).Build().Run();
            }   
            catch (Exception ex)
            {
                logger.Error(ex.Message, "Error in init");
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }

        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
               .ConfigureLogging(logging =>
               {
                      logging.ClearProviders();
                      logging.SetMinimumLevel(LogLevel.Information);
                      
               })
               .UseNLog()
               .UseWebRoot(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"))
               .UseIISIntegration()
               .UseStartup<Startup>();
    }
}
