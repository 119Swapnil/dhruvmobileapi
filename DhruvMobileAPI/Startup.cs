﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISG.TraversAPI.Model.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;

namespace DhruvMobileAPI
{
    public class Startup
    {
        Logger logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        private string SecurityKey = "xsusd733@54", Issure = "http://localhost:44303", Audience = "24";

        public IConfiguration _Configuration;

        public Startup(IConfiguration configuration)
        {
            _Configuration = configuration;       
        }

        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                //How to read appsettings values
                //https://www.youtube.com/watch?v=nIoDpTPIle8
               
               
                //https://stackoverflow.com/questions/47711983/jwtsecuritytoken-returning-wrong-expiration-time
                services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
                services.AddSwaggerGen(c =>
                {

                    c.SwaggerDoc("v3", new Info
                    {
                        Version = "v3",
                        Title = "MyDhruv MobileApp Api",
                        Description = "ASP.NET Core Web API"
                    });

                    c.AddSecurityDefinition("Bearer",
                        new ApiKeyScheme
                        {
                            In = "header",
                            Description = "Please enter into field the word 'Bearer' following by space and JWT",
                            Name = "Authorization",
                            Type = "apiKey"
                        });
                    c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[]{ } }
                });

                });
                // Configure Compression level
                services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);

                // Add Response compression services
                services.AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                //options.EnableForHttps = true;
                });

                services.Configure<AppSettings>(_Configuration.GetSection("AuthenticationValues"));
                // SecurityKey=services.Configure<>
                SecurityKey = _Configuration["AuthenticationValues:Key"];
                Issure = _Configuration["AuthenticationValues:Issuer"];
                Audience = _Configuration["AuthenticationValues:Audience"];
                var Key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey));
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        //What to validate
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        //Setup validate data
                        ValidIssuer = Issure,
                        ValidAudience = Audience,
                        ClockSkew = TimeSpan.FromDays(24),
                        RequireExpirationTime = true,
                        IssuerSigningKey = Key
                    };
                });
            }
            catch (Exception e)
            {

                logger.Error("Startup Error " + e.Message);
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            try
            {
                if (env.IsDevelopment())
                {

                    app.UseDeveloperExceptionPage();
                    app.UseStaticFiles();
                    app.UseStaticFiles(new StaticFileOptions
                    {
                        FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"))
                    });
                    app.UseHsts();
                    app.UseHttpsRedirection();
                    app.UseAuthentication();
                    app.UseResponseCompression();
                    app.UseMvc();
                    app.UseSwagger();
                    app.UseSwaggerUI(c =>
                    {

                        c.SwaggerEndpoint("/swagger/v3/swagger.json", "DhruvMobileApi");
                    });
                }
                else
                {
                    logger.Trace("in hosting release");
                    app.UseStaticFiles();
                    app.UseStaticFiles(new StaticFileOptions
                    {
                        FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"))
                    });
                    app.UseHsts();
                    app.UseHttpsRedirection();
                    app.UseExceptionHandler(errorApp =>
                    {
                        errorApp.Run(async context =>
                        {
                            var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                            var exception = errorFeature.Error;
                            logger.Error("InStartup Exception- " + exception);
                            // log the exception etc..
                            // produce some response for the caller
                        });
                    });
                    app.UseAuthentication();
                    app.UseResponseCompression();
                    app.UseMvc();
                    app.UseSwagger();
                    app.UseSwaggerUI(c =>
                    {
                        c.SwaggerEndpoint("../swagger/v3/swagger.json", "DhruvMobileApi");
                    });
                    logger.Trace("Startup debug ");
                }
            }catch(Exception e)
            {
                logger.Error("InStartup Exception- " + e);
            }

        }
    }
}
