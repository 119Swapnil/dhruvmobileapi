﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Model.Entities.Notification;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.Extensions.Configuration;

using Microsoft.AspNetCore.Authorization;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;

namespace DhruvMobileAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly NotificationService _notificationService;
        private readonly IConfiguration _configuration;
        private string connectionString, APISecretKey, APISenderId;

        public NotificationController(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _notificationService = new NotificationService(new NotificationRepository(connectionString));
            APISecretKey = _configuration.GetSection("FireBase")["WebAPISecretKey"];
            APISenderId = _configuration.GetSection("FireBase")["WebAPISenderId"];
        }

        [HttpPost("/PushNotification")]
        public async Task<bool> PushNotification(string to, string body, string title)
        {
            return await _notificationService.PushNotification(to, body, title, APISecretKey, APISenderId);
        }

        [HttpPost("/RegisterUserForNotification")]
        public bool RegisterUserForNotification(string id, string token = "dah6bhQflyI:APA91bHvy0APC6q1WlBXGFbWIzJUL2-v4V3q5zbNm1r_ekGTqstTcBnJOf5O2ixUTI20F7l-Fg82Br79jEgIytK_XsfmhD8_z9XvpK3d6IeAQP5B4aj55e7EmdPrfAnbddful_K7kMpj")
        {
            return _notificationService.RegisterUserForNotification(id, token);
        }

        [HttpPost("/SentNotificationMessage")]
        public async Task<bool> SentNotificationMessage(string message, List<Client> listUser)
        {
            return await _notificationService.SentNotificationMessage(message, listUser, APISecretKey, APISenderId);
        }
    }
}