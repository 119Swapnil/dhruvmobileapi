﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.Extensions.Configuration;
using ISG.DhruvMobileAPI.Repository.Implementations;
using System.Net.Http;
using System.Text;
using Microsoft.IdentityModel.Protocols;
using System.Security.Cryptography;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;

namespace DhruvMobileAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly ClientService _userService;
        private readonly IConfiguration _configuration;
        private string connectionString;
        private string _tlconnString;

        public AccountController(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _tlconnString = _configuration.GetConnectionString("BrochureDatabase");
            //_userService = new ClientService(new ClientRepository(connectionString, _tlconnString));
        }

        //[HttpPost]
        //[Route("loginclient")]
        //public async Task<IActionResult> GetUserInformationAsync([FromBody] Client _userResource)
        //{
        //    try
        //    {
        //        Client model;
        //        var password = HashMD5(_userResource.OnlinePassword);
        //        model = await _userService.GetClientInformationAsync(_userResource.EMailId, password);
        //        return Json(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        [HttpPost]
        [Route("testpassword")]
        public string TestPassword(string value)
        {
            var encryptpassword = HashMD5(value);
            return encryptpassword;
        }

        [HttpPost]
        [Route("reversepassword")]
        public string ReversTestPassword(string value)
        {
            var encryptpassword = HashMD5Decrypt(value);
            return encryptpassword;
        }

        [HttpPost]
        [Route("reversepassword1")]
        private string HashMD5Decrypt(string value)
        {
            try
            {
                string key = _configuration["ConnectionStrings:EncryptionKey"];
                string decryptText = value.Replace("-", "/").Replace("_", "+").Replace(",", "=").Replace(".", "?").Replace(";", "&");
                byte[] inputArray = Convert.FromBase64String(decryptText);
                TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
                tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tripleDES.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
                tripleDES.Clear();
                return UTF8Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception e)
            {
                return "";
            }
        }

        [HttpPost]
        [Route("reversepassword2")]
        private string HashMD5(string value)
        {
            try
            {
                var MD5 = System.Security.Cryptography.MD5.Create();
                var inputBytes = Encoding.ASCII.GetBytes(value);
                var hash = MD5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                for (var i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

    }
}