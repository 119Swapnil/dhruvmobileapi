﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Entities.Weather;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DhruvMobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : Controller
    {
        private string baseurl;
        private string imageurl;
        private string apikey;
        private WeatherService _weatherService;

        public WeatherController(IConfiguration configuration)
        {
           baseurl = configuration.GetSection("Weather")["BaseUrl"];
           imageurl = configuration.GetSection("Weather")["WeatherImgUrl"];
           apikey = configuration.GetSection("Weather")["ApiKey"];

            _weatherService = new WeatherService(new WeatherRepository(baseurl, apikey));

        }

        [HttpGet]
        [Route("AccuCityLocId")]
        public async Task<List<string>> GetAccuCityLocId(string cityliststring)
        {
            try
            {
                var citylist = JsonConvert.DeserializeObject<List<Position>>(cityliststring);


                var res = await _weatherService.GetCityLocId(citylist);
                return res;
            }catch(Exception e)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("WeatherByPosition")]
        public async Task<IActionResult> GetWeekWeatherByCordinates(double lat, double lng, DateTime date)
        {
            return Json(await _weatherService.GetWeatherByGeoPosAsync(lat, lng, date));
        }

        [HttpGet]
        [Route("AccuWeather5Days")]
        public async Task<IActionResult> GetAccuWeather5Days(string cityid)
        {
            return Json(await _weatherService.GetAccuWeather5DaysAsync(cityid));
        }

        [HttpGet]
        [Route("AccuWeather12Hours")]
        public async Task<IActionResult> GetAccuWeather12Hours(string cityid)
        {
            return Json(await _weatherService.GetAccuWeather12HoursAsync(cityid));
        }


    }
}