﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Flight;

using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DhruvMobileAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : Controller
    {
        private readonly IConfiguration _configuration;
        private string connectionString;
        private readonly FlightService _flightService;


        public FlightController(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _flightService = new FlightService(new FlightRepository(connectionString));
        }

        [HttpGet]
        [Route("GetFlights")]
        public async Task<IActionResult> GetFlightListAsync(int itineraryno)
        {
            return Json(await _flightService.GetFlightListAsync(itineraryno));
        }

        [HttpGet]
        [Route("GetFlightPassengers")]
        public async Task<IActionResult> GetFlightPassengerAsync(int itineraryno)
        {
            return Json(await _flightService.GetFlightPassenger(itineraryno));
        }
    }
}