﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ISG.DhruvMobileAPI.Model.Entities.Document;
using ISG.DhruvMobileAPI.Repository.Implementations;

using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using ISG.DhruvMobileAPI.Model.Common;
using Microsoft.AspNetCore.Authorization;
using ISG.DhruvMobileAPI.Model.Entities.Pointer;
using ISG.DhruvMobileAPI.Model.Entities.Common;
using ISG.Common;
using System.IO;

namespace DhruvMobileAPI.Controllers
{
  [Authorize]
  [Route("api/[controller]")]
  [ApiController]
  public class DocumentController : Controller
  {
    private readonly DocumentService _documentStoreService;
    private readonly IConfiguration _configuration;
    private IHostingEnvironment _hostingEnvironment;
    private string connectionString, docconnectionString, pntrConnectionstring, commonconnectionString, presetsConnectionString;
    private readonly PointerService _pointerStoreService;
    private readonly CommonService _commonService;
    string defaultBranchCode = string.Empty;

    public DocumentController(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
    {
      _configuration = configuration;
      connectionString = _configuration.GetConnectionString("MyAppDatabase");
      docconnectionString = _configuration.GetConnectionString("DocStoreDatabase");
      pntrConnectionstring = _configuration.GetConnectionString("Sales97Database");
      commonconnectionString = _configuration.GetConnectionString("MyAppDatabase");
      presetsConnectionString = _configuration.GetConnectionString("PresetsDatabase");
      _documentStoreService = new DocumentService(new DocumentRepository(docconnectionString, connectionString));
      _pointerStoreService = new PointerService(new PointerRepository(pntrConnectionstring));
      _commonService = new CommonService(new CommonRepository(commonconnectionString, presetsConnectionString));
      _hostingEnvironment = hostingEnvironment;
      defaultBranchCode = _configuration["DefaultBranchCode"];
    }

    [Consumes("application/json", "application/json-patch+json", "multipart/form-data")]
    [HttpPost("/api/adddocuments")]
    [DisableRequestSizeLimit]
    public async Task<IActionResult> AddDocument([FromForm]string documentData, IFormFile formFile)
    //public async Task<List<AccountDocument>> AddDocument(string documentData, IFormFile formFile)
    {
      int itineraryNo;
      string baseURL;
      string virtualCabinetUrl = string.Empty;
      try
      {
        if (!string.IsNullOrWhiteSpace(documentData))
        {
          var accountDocumentResource = JsonConvert.DeserializeObject<Document>(documentData);
          if (accountDocumentResource != null)
          {
            itineraryNo = accountDocumentResource.ItineraryNo.ReturnString().ToInt();
            baseURL = GetBaseURL(itineraryNo);
            var response = await _documentStoreService.SaveAccountDocumentAsync(accountDocumentResource
            , baseURL,
            formFile);

            if (response != null)
            {
              if (response.Result)
              {
                virtualCabinetUrl = _commonService.GetDhruvAppVirtualCabinetUrl(itineraryNo);
                var obj = response.listObject as List<Document>;
                foreach (var item in obj)
                {
                  var url = virtualCabinetUrl + @"\" + item.DocPath;
                  item.DocUrl = url.Replace("\\", "/");
                }

                return Json(response);
              }
              else
              {
                return Json(response);
              }
            }
          }
          return Json(new BaseModel()
          {
            Result = false,
            Message = "Empty document object found" + documentData
          });
          //    {
          //        if (formFile != null)
          //        {
          //            if (formFile.Length > 0)
          //            {
          //                var _itineraryDirectory = directoryName + accountDocumentResource.ItineraryNo.ToString();
          //                var _filepath = (!string.IsNullOrWhiteSpace(directoryPath) ? directoryPath : _hostingEnvironment.ContentRootPath) + _itineraryDirectory;
          //                Directory.CreateDirectory(_filepath);

          //                var fileurl = _rootPath + _itineraryDirectory + "\\" + formFile.FileName;
          //                accountDocumentResource.DocumentFilePath = fileurl.Replace("\\", "/");

          //                _filepath = Path.Combine(_filepath, formFile.FileName);

          //                using (var stream = new FileStream(_filepath, FileMode.Create))
          //                {
          //                    await formFile.CopyToAsync(stream);
          //                }
          //                //var _document = new AccountDocument
          //                //{
          //                //    AccountCode = accountDocumentResource.AccountCode,
          //                //    ItineraryNo = accountDocumentResource.ItineraryNo,
          //                //    DocumentFilePath = accountDocumentResource.DocumentFilePath,
          //                //    DocDesc = accountDocumentResource.DocDesc,
          //                //    DocCode = accountDocumentResource.DocCode,
          //                //    DocumentTitle = formFile.FileName
          //                //};
          //                //Save Document details in database after document add part is pending
          //                //_document = await _documentService.SaveAccountDocument(_document);

          //                accountDocumentResource.DocumentTitle = formFile.Name;

          //                List<AccountDocument> accountDocuments = new List<AccountDocument>();
          //                accountDocuments = await _documentService.SaveAccountDocumentAsync(accountDocumentResource);

          //                //return accountDocuments;
          //            }
          //        }
          //    }
          //    return Ok();
          //}
          //else
          //{
          //    return NotFound();
          //}
          //return Json(result);
        }
        else
        {
          return Json(new BaseModel()
          {
            Result = false,
            Message = "Empty document object found" + documentData
          });
        }

      }
      catch (Exception e)
      {
        return Json(new BaseModel()
        {
          Result = false,
          Message = e.Message
        });
      }
    }

    private String GetBaseURL(int itineraryNo)
    {
      int publishVersion = 0;
      List<string> companyBranchCode = new List<string>();
      string baseURL, branchCode = string.Empty, companyCode = string.Empty;

      publishVersion = _commonService.GetLatestPublishVersion(itineraryNo);
      companyBranchCode = _commonService.GetCompanyBranchCode(itineraryNo, publishVersion);

      if (companyBranchCode != null && companyBranchCode.Count > 0)
      {
        branchCode = companyBranchCode[1] != null ? companyBranchCode[1] : "";
        companyCode = companyBranchCode[0] != null ? companyBranchCode[0] : "";
      }

      baseURL = _pointerStoreService.GetPointerValueBy(Pointer.PhysicalPathUrlExternal.Code, companyCode, branchCode, defaultBranchCode);
      return baseURL;
    }

    [HttpGet("/api/deletedocument")]
    public async Task<bool> DeleteDocumentAsync(int SeqNo, int ItineraryNo)
    {
      Document doc = new Document();
      string baseURL, filepath;
      try
      {
        if (SeqNo > 0)
        {
          var documents = _documentStoreService.GetDocumentAsync(ItineraryNo);
          doc = documents.Result.Where(d => d.SeqNo == SeqNo).FirstOrDefault();
          baseURL = GetBaseURL(ItineraryNo);
          filepath = baseURL + @"\" + doc.DocPath;
          //var _itineraryDirectory = directoryName + itineraryHeaderSeqNo.ToString();
          //var _filepath = (!string.IsNullOrWhiteSpace(directoryPath) ? directoryPath : _hostingEnvironment.ContentRootPath) + _itineraryDirectory + "\\" + documentTitle;
          //_dirpath = _rootPath + directoryName + itineraryHeaderSeqNo + "\\" + documentTitle;
          //_dirpath = _dirpath.Replace("\\", "/");
          if (System.IO.File.Exists(filepath))//If file exists then only delete
          {
            System.IO.File.Delete(filepath);
          }
          return await _documentStoreService.DeleteDocumentAsync(SeqNo);

        }
        else
        {
          return false;
        }
      }
      catch (Exception ex)
      {
        return false;
        // throw new Exception(ex.Message);
      }
    }

    [HttpGet("/api/getcountrydetail")]
    public async Task<BaseModel> GetCountryDetailAsync(int Itineraryno)
    {
      try
      {
        //connectionString = _configuration.GetConnectionString("SteppesDatabase");
        //var _documentService = new DocumentService(new DocumentRepository(docconnectionString, connectionString));
        BaseModel listCountry = await _documentStoreService.GetCountryDetails(Itineraryno);
        return listCountry;
      }
      catch (Exception e)
      {
        return new BaseModel()
        {
          Result = false,
          Message = e.Message,

        };
      }
    }

    [HttpGet("/api/getdocument")]
    public async Task<List<Document>> GetDocumentAsync(int ItineraryNo)
    {
      string baseURL = string.Empty;
      try
      {
        List<Document> aactDocuments = await _documentStoreService.GetDocumentAsync(ItineraryNo);
        baseURL = _commonService.GetDhruvAppVirtualCabinetUrl(ItineraryNo.ReturnString().ToInt());
        foreach (var item in aactDocuments)
        {
          var url = baseURL + @"\" + item.DocPath;
          item.DocUrl = url.Replace("\\", "/");
        }
        return aactDocuments;
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }
  }
}