﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;
using ISG.DhruvMobileAPI.Model.Entities.Tour;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DhruvMobileAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TourController : Controller
    {
        private readonly IConfiguration _configuration;
        private string connectionString;
        private string _tlconnString;
        private readonly TourService _tourService;
        public TourController(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _tlconnString = _configuration.GetConnectionString("BrochureDatabase");
            _tourService = new TourService(new TourRepository(connectionString, _tlconnString));
        }

        [HttpGet]
        [Route("UserTours")]
        public async Task<IActionResult> GetUserTourList(string accountcode, string branchCode)
        {
            return Json(await _tourService.GetUserToursAsync(accountcode, branchCode));
        }

        [HttpGet]
        [Route("TLToursList")]
        public async Task<IActionResult> GetTLTourList(string accountcode)
        {
            return Json(await _tourService.GetTLToursAsynch(accountcode));
        }

        [HttpGet]
        [Route("TravelExpert")]
        public async Task<IActionResult> GetAllTravelExpert(int ItineraryNo)
        {
            try
            {
                return Json(await _tourService.GetItineraryExpert(ItineraryNo));


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        [HttpGet]
        [Route("ImportantInformation")]
        public async Task<IActionResult> GetItineraryImportantInformationAsync(int ItineraryNo)
        {
            try
            {
                return Json(await _tourService.GetItineraryImportantInformation(ItineraryNo));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        [Route("CompanyContactDetails")]
        public async Task<IActionResult> GetCompanyContactDetailsAsync(string Itineraryno)
        {
            try
            {
                return Json(await _tourService.GetCompanyContactDetailsAsync(Itineraryno));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        [Route("TourSupplierContacts")]
        public async Task<IActionResult> GetSupplierContactAsync(string Itineraryno)
        {
            try
            {
                return Json(await _tourService.GetSupplierContactAsync(Itineraryno));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}