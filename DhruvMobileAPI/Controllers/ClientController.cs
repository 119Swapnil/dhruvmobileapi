﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DhruvMobileAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : Controller
    {
        private IConfiguration _configuration;
        private string connectionString;
        private string _tlconnString;
        private string _salesString;
        private ClientService _userService;
        private string SecurityKey = "", Issure = "", Audience = "", ExpireTime = "24";
        public ClientController(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration["ConnectionStrings:MyAppDatabase"];
            _tlconnString = _configuration.GetConnectionString("BrochureDatabase");
            _salesString = _configuration.GetConnectionString("Sales97Database");
            _userService = new ClientService(new ClientRepository(connectionString, _tlconnString, _salesString));
            SecurityKey = _configuration.GetSection("AuthenticationValues")["Key"];
            Issure = _configuration.GetSection("AuthenticationValues")["Issuer"];
            Audience = _configuration.GetSection("AuthenticationValues")["Audience"];
            ExpireTime = _configuration.GetSection("AuthenticationValues")["Expire"];
        }



        //[HttpPost]
        //[Route("authclient")]
        //public async Task<IActionResult> GetUserInformationAsync([FromBody] Client _userResource)
        //{
        //    try
        //    {
        //        Client model;
        //        var password = HashMD5(_userResource.OnlinePassword);
        //        model = await _userService.GetClientInformationAsync(_userResource.EMailId, password);
        //        if(model.Result == true)
        //        {
        //           model.ApiToken = GetToken();
        //            return Json(model);
        //        }
        //        else
        //        {
        //            model.Message = model.Message;
        //            return Json(model);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new Client()
        //        {
        //            Result = false,
        //            Message = ex.Message
        //        });
        //    }
        //}

        //private string HashMD5(string value)
        //{
        //        try
        //        {
        //            var MD5 = System.Security.Cryptography.MD5.Create();
        //            var inputBytes = Encoding.ASCII.GetBytes(value);
        //            var hash = MD5.ComputeHash(inputBytes);

        //            var sb = new StringBuilder();
        //            for (var i = 0; i < hash.Length; i++)
        //            {
        //                sb.Append(hash[i].ToString("X2"));
        //            }
        //            return sb.ToString();
        //        }
        //        catch (Exception)
        //        {
        //            return "";
        //        }
        //}






        //[HttpPost]
        //[Route("token")]
        //public string GetToken()
        //{
        //    try
        //    {
        //        var Key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey));
        //        var signingcredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256Signature);

        //        int expt = Convert.ToInt32(ExpireTime);

        //        var token = new JwtSecurityToken(
        //              issuer: Issure,
        //                audience: Audience,
        //                expires: DateTime.UtcNow.AddDays(30),
        //                signingCredentials: signingcredentials);
        //        // Return Token
        //        return new JwtSecurityTokenHandler().WriteToken(token);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        [HttpGet]
        [Route("GetClientPaxCount")]
        public async Task<IActionResult> GetClientPaxCountAsync(string TourCode, DateTime DepDate)
        {
            return Json(await _userService.GetPaxCountAsync(TourCode, DepDate));
        }

        [HttpGet]
        [Route("GetTourClientsDetails")]
        public async Task<IActionResult> GetClientsDetailsAsync(string TourCode, DateTime DepDate)
        {
            return Json(await _userService.GetClientsWithDetails(TourCode, DepDate));
        }
    }
} 