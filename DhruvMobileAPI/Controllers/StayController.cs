﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Stay;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DhruvMobileAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StayController : Controller
    {
        private StayService _stayService;

        public StayController(IConfiguration configuration)
        {
            IConfiguration _configuration = configuration;
            string connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _stayService = new StayService(new StayRepository(connectionString));
        }

        [HttpGet]
        [Route("GetStayList")]
        public async Task<IActionResult> GetStayList(int itineraryno)
        {
            try
            {
                return Json(await _stayService.GetStayListAsync(itineraryno));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //[HttpGet]
        //[Route("StayImageGallery")]
        //public async Task<IActionResult> GetStayImageGallery(string hotelcode, string itineraryno)
        //{
        //    try
        //    {
        //        return Json(await _stayService.GetStayImageGalleryAsync(hotelcode, itineraryno));
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

        //[HttpGet]
        //[Route("StayFacility")]
        //public async Task<IActionResult> GetStayFacility(string hotelcode)
        //{
        //    try
        //    {
        //        return Json(await _stayService.GetStayFacilityAsync(hotelcode));
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}
    }
}