﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;
using ISG.DhruvMobileAPI.Model.Entities.TourLeader;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Web;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DhruvMobileAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : Controller
    {
        Logger _logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        private readonly IConfiguration _configuration;
        private string connectionString;
        private string salesString;
        private string _tlconnString;
        private ClientService _userService;
        private TourLeaderService _tourLeaderService;
        private string SecurityKey = "", Issure = "", Audience = "", ExpireTime = "24";
        public AuthenticationController(IConfiguration configuration)
        {

            _logger.Info("InAuthCont constr before");
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("MyAppDatabase");
            salesString = _configuration.GetConnectionString("Sales97Database");
            _tlconnString = _configuration.GetConnectionString("BrochureDatabase");
            _userService = new ClientService(new ClientRepository(connectionString, _tlconnString, salesString));
            _tourLeaderService = new TourLeaderService(new TourLeaderRepository(connectionString, _tlconnString));
            SecurityKey = _configuration.GetSection("AuthenticationValues")["Key"];
            Issure = _configuration.GetSection("AuthenticationValues")["Issuer"];
            Audience = _configuration.GetSection("AuthenticationValues")["Audience"];
            ExpireTime = _configuration.GetSection("AuthenticationValues")["Expire"];
            _logger.Info("InAuthCont constr after");
        }

        [HttpPost]
        [Route("authclient")]
        public async Task<IActionResult> GetUserInformationAsync(string EmailId, string Password)
        {
            try
            {
                Client model;
                model = await _userService.GetClientInformationAsync(EmailId, Password);
                if (model.Result == true)
                {
                    model.ApiToken = GetToken();
                    return Json(model);
                }
                else
                {
                    model.Message = model.Message;
                    return Json(model);
                }

            }
            catch (Exception ex)
            {
                return Json(new Client()
                {
                    Result = false,
                    Message = ex.Message
                });
            }
        }

        [HttpPost]
        [Route("authtourleader")]
        public async Task<IActionResult> LoginTourLeaderAsync([FromBody]TourLeader tourLeader)
        {

            try
            {
                TourLeader leader;
                tourLeader.Password = await HashMD5(tourLeader.Password);
                leader = await _tourLeaderService.TourLeaderLoginAsync(tourLeader);

                if (leader != null)
                {
                    leader.ApiToken = GetToken();
                    return Json(new BaseModel()
                    {
                        Result = true,
                        Message = "tourleader login sucessfully",
                        classobject = leader
                    });
                }
                else
                {

                    return Json(new BaseModel()
                    {
                        Result = false,
                        Message = "tourleader failed to login",
                        classobject = leader
                    });

                }

            }
            catch (Exception e)
            {
                return Json(new BaseModel()
                {
                    Result = false,
                    Message = e.Message
                });

            }
        }



        [HttpPost]
        [Route("authtourleader1")]
        private string GetToken()
        {
            try
            {
                var Key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey));
                var signingcredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256Signature);

                int expt = Convert.ToInt32(ExpireTime);

                var tokenDescripter = new SecurityTokenDescriptor
                {
                    Subject = new System.Security.Claims.ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, "1"),
                        new Claim(ClaimTypes.Role, "Travler")
                    }),
                    Issuer = Issure,
                    Audience = Audience,
                    Expires = DateTime.UtcNow.AddDays(14),
                    SigningCredentials = signingcredentials
                };

                //var token = new JwtSecurityToken(
                //      issuer: Issure,
                //        audience: Audience, 
                //        expires: DateTime.UtcNow.AddDays(30),
                //        signingCredentials: signingcredentials);


                // Return Token
                var tokenhandler = new JwtSecurityTokenHandler();
                var token = tokenhandler.CreateToken(tokenDescripter);
                return tokenhandler.WriteToken(token);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        [HttpPost]
        [Route("authtourleader2")]
        private Task<string> HashMD5(string value)
        {
            try
            {
                var MD5 = System.Security.Cryptography.MD5.Create();
                var inputBytes = Encoding.ASCII.GetBytes(value);
                var hash = MD5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                for (var i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return Task.FromResult(sb.ToString());
            }
            catch (Exception)
            {
                return Task.FromResult("");
            }
        }



    }
}