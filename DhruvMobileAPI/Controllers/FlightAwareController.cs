﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Entities.FlightAware;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DhruvMobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightAwareController : Controller
    {
        private FlightAwareService _flightawareService;

        public FlightAwareController()
        {
            _flightawareService = new FlightAwareService(new FlightAwareRepository());
        }

        [HttpGet]
        [Route("FlightAwareApi3")]
        public async Task<IActionResult> GetFlightInfoAndStatusAsync(string flightno)
        {
            return Json(await _flightawareService.GetFlightInfoAndStatusAsync(flightno));
        }
    }
}