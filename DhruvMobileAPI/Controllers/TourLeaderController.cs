﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Tour;
using ISG.DhruvMobileAPI.Model.Entities.TourLeader;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DhruvMobileAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TourLeaderController : Controller
    {
        private readonly IConfiguration _configuration;
        private string connectionString;
        private string _tlconnString;
        private readonly TourLeaderService _tourLeaderService;

        public TourLeaderController(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _tlconnString = _configuration.GetConnectionString("BrochureDatabase");
            _tourLeaderService = new TourLeaderService(new TourLeaderRepository(connectionString, _tlconnString));
        }

        [HttpGet]
        [Route("TourLeaderList")]
        public async Task<IActionResult> GetTourLeadersAsync(int itineraryno)
        {
            return Json(await _tourLeaderService.GetTourLeaderListAsync(itineraryno));
        }




    }
}