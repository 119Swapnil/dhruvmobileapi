﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;

namespace DhruvMobileAPI.Controllers
{
    [Authorize]
    [Route("api/Itinerary")]
    [ApiController]
    public class ItineraryController : Controller
    {
        private readonly IConfiguration _configuration;
        private string connectionString;
        private readonly ItineraryService _itineraryService;
        public ItineraryController(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _itineraryService = new ItineraryService(new ItineraryRepository(connectionString));
        }


        //[HttpGet]
        //[Route("TourItineraryBrief")]
        //public async Task<IActionResult> GetBriefItineraryAsync(int Itineraryno)
        //{
        //    return Json(await _itineraryService.GetBriefItineraryAsync(Itineraryno));
        //}


        //[HttpGet]
        //[Route("TourItineraryComponent")]
        //public async Task<IActionResult> GetTourItineraryAsync(int Itineraryno, int Dayno)
        //{
        //    return Json(await _itineraryService.GetDayWiseItineraryAsync(Itineraryno, Dayno));
        //}

        [HttpGet]
        [Route("GetCompleteItinerary")]
        public async Task<IActionResult> GetTourCompleteItineraryAsync(int Itineraryno)
        {
            return Json(await _itineraryService.GetCompleteItineraryAsync(Itineraryno));
        }
    }
}