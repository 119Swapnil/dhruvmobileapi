﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Entities.Dmc;
using ISG.DhruvMobileAPI.Repository.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DhruvMobileAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DMCController : Controller
    {
        private string _connectionString;
        private string _tlconnString;
        private DmcService _dmcService;
        private IConfiguration _configuration;

        public DMCController(IConfiguration configuration)
        {
            this._configuration = configuration;
            _connectionString = _configuration.GetConnectionString("MyAppDatabase");
            _tlconnString = _configuration.GetConnectionString("BrochureDatabase");
            _dmcService = new DmcService(new DmcRepository(_connectionString, _tlconnString));
        }


        [HttpGet]
        [Route("GetDmcListWithDetails")]
        public async Task<IActionResult> GetDmcListWithDetailsAsync(string TourCode)
        {
            return Json(await _dmcService.GetDmcListWithDetails(TourCode));
        }
    }
}