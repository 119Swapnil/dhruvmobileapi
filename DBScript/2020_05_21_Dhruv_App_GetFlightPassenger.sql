use myapp
go
ALTER PROCEDURE [dbo].[Dhruv_App_GetFlightPassenger]
	@itineraryNo int,
	@ClientCode  nvarchar(10)

AS
BEGIN
	DECLARE @PublishVersion int
	Set @PublishVersion = (Select max(publishversion)  
	From myapp.dbo.BkgItineraryHeader as bih 
	Where bih.ItineraryNo = @itineraryNo) --And bih.BranchCode = 'RB')



	/*Select tc.*,c.nameasonpp,c.fname,c.lname,c.mname,c.title ,idc.vehicle,idc.airline_name airlinename,
	case isnull(idc.pnrno,'') when '' then tc.pnrno else idc.pnrno end pnrno
	from myapp.dbo.TravelClients as tc 
	LEFT JOIN myapp.dbo.itinerarydaycomponent as idc ON idc.ItineraryNo = tc.ItineraryNo and idc.PublishVersion = tc.PublishVersion	
	  and idc.seqno_travel  = tc.seqno_travel  and idc.cpnttype in ('iflt','dlft')
	LEFT JOIN myapp.dbo.Client as c ON c.ItineraryNo = tc.ItineraryNo and c.PublishVersion = tc.PublishVersion	
	and c.ClientCode COLLATE DATABASE_DEFAULT = tc.ClientCode COLLATE DATABASE_DEFAULT	
	Where tc.ItineraryNo = @itineraryNo And tc.PublishVersion = @PublishVersion
		and (isnull(tc.ticketno,'')<>'' or isnull(idc.pnrno,tc.pnrno) <>'')
	--And tc.ClientCode = @ClientCode*/
    SELECT idc.vehicle, idc.airline_name airlinename,tcs.seqno_travel,tcs.ticketno ETicketNo, tcs.clientcode, 
       Isnull(cl.title, '') + ' ' + Isnull(cl.fname, '') + ' ' + Ltrim(Isnull(mname, '') + ' ') + Isnull(cl.lname, '') ClientName, 
       tcs.seqno_travel, Ltrim(Isnull(cl.title, '') + ' ' + Isnull(cl.fname, '') + ' ' + Isnull(cl.lname, '')) ClientNameAsOnPP, 
       tcs.seatnumber AS seatNo, tcs.frequentflyerno  AS FlyerNo, 
       CASE Isnull(idc.pnrno, '') WHEN '' THEN tcs.pnrno ELSE idc.pnrno END pnrno, 
       cl.firstnameonpp, cl.lastnameonpp, cl.fname AS FirstName, cl.lname AS LastName 
        FROM   myapp.dbo.travelclients tcs 
               LEFT JOIN myapp.dbo.itinerarydaycomponent AS idc 
                      ON idc.itineraryno = tcs.itineraryno 
                         AND idc.publishversion = tcs.publishversion 
                         AND idc.seqno_travel = tcs.seqno_travel 
                         AND idc.cpnttype IN ( 'iflt', 'dflt' ) 
               LEFT JOIN myapp..client cl 
                      ON cl.clientcode = tcs.clientcode 
                         AND cl.itineraryno = tcs.itineraryno 
                         AND cl.publishversion = tcs.publishversion 
        WHERE  tcs.itineraryno = @ItineraryNo 
               AND tcs.publishversion = @PublishVersion 
	
	
END