use myapp
go
ALTER PROCEDURE [dbo].[DhruvAppTvlrGetFlightList]
	@itineraryNo int

AS
BEGIN

	DECLARE @PublishVersion int
	Set @PublishVersion = (Select max(bih.PublishVersion)  
	From myapp.dbo.BkgItineraryHeader as bih  
	Where bih.ItineraryNo = @itineraryNo)
			
	 
	
	Select * from myapp.dbo.ItineraryDayComponent as idc
	Where idc.ItineraryNo = @itineraryNo
	And (idc.CpntType = 'IFLT' Or idc.CpntType = 'DFLT')
	And idc.PublishVersion = @PublishVersion

END