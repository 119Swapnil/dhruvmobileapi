use myapp
go

ALTER PROCEDURE [dbo].[DhruvAppTvlrGetImportantInformation]
@ItineraryNo int
as
Begin
    Declare @PublishVersion int
	Set @PublishVersion = (select max(bih.publishversion) 
	from myapp.dbo.BkgItineraryHeader bih 
	where bih.ItineraryNo = @itineraryNo )

    select ItineraryNo, seqno,  CountryCode Country_Code, CountryName Country_Name, cast (QuoteText as nvarchar (max)) [Description], 'CO' as AdditionalInformation 
	from 	BkgItineraryCountry
	where ItineraryNo =@ItineraryNo
	And PublishVersion = @PublishVersion
End