use myapp
go

ALTER PROCEDURE [dbo].[DhruvAppTvlrGetCountryDetails]
@ItineraryNo int
as
Begin
    Declare @PublishVersion int
	Set @PublishVersion = (select max(bih.publishversion) from myapp.dbo.BkgItineraryHeader bih 
	where bih.ItineraryNo = @itineraryNo )

    select BIC.ItineraryNo,BIC.seqno,BIC.CountryName,BIC.CountryCode,
    (select top 1 replace(replace(replace (IsNull(itinImg.ImagePath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/'), '.jpg', '.jpg') 
    from myapp.dbo.ItineraryImages itinImg
    where itinImg.ItineraryNo = bic.ItineraryNo and itinImg.publishversion = bic.PublishVersion
    and itinImg.CpntType = 'COUN' and itinImg.CpntCode =BIC.CountryCode) DocumentImagePath
    from myapp.dbo.BkgItineraryCountry BIC
    where BIC.publishversion = @PublishVersion  and bic.ItineraryNo = @itineraryNo
End