use myapp
go

ALTER PROCEDURE [dbo].[DhruvAppTvlrGetStayGalleryImages]
		@hotelCode int,
		@ItineraryNo int = 0
AS
BEGIN
	Declare @PublishVersion int,@BranchCode nvarchar(10),@CompanyCode nvarchar(10),@pointerValue nvarchar(200)
    select top 1 @PublishVersion = publishversion, @BranchCode =BranchCode,@CompanyCode=CompanyCode
      from myapp.dbo.bkgitineraryheader bih where bih.ItineraryNo = @itineraryNo order by publishversion desc

    select 1 rowNum,id.ImgCaption,id.HotelImageType,id.ImgRight,id.ExpiryDate,id.ImgType,
    case right(img.ImagePath,4)
        when '.jpg' then 
    replace(replace(replace (IsNull(img.ImagePath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/'), '.jpg', '.jpg')
        when '.png' then 
    replace(replace(replace (IsNull(img.ImagePath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/'), '.png', '.png')
        else 
    replace(replace (IsNull(img.ImagePath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/')
    end ImagePath from myapp.dbo.ItineraryImages img 
    left join Presets.dbo.imgdetails id on id.ProductType = 'STAY' And id.ProductCode COLLATE DATABASE_DEFAULT = img.CpntCode COLLATE DATABASE_DEFAULT 
            And id.imgCode COLLATE DATABASE_DEFAULT = img.imgCode COLLATE DATABASE_DEFAULT  And id.QualityCode >= 1
    where img.ItineraryNo = @itineraryNo and img.publishversion = @PublishVersion
     and img.CpntType = 'stay' and img.CpntCode=@hotelCode
    union
    Select Top 3 2 rowNum, id.ImgCaption,id.HotelImageType,id.ImgRight,id.ExpiryDate,id.ImgType,
    case right(id.ImgCrPath,4)
        when '.jpg' then 
    replace(replace(replace (IsNull(id.ImgCrPath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/'), '.jpg', '.jpg')
        when '.png' then 
    replace(replace(replace (IsNull(id.ImgCrPath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/'), '.png', '.png')
        else 
    replace(replace (IsNull(id.ImgCrPath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/')
    end ImagePath
    From Presets.dbo.imgdetails id
    Where id.ProductType = 'STAY' And id.ProductCode = @hotelCode And id.QualityCode >= 1
    and id.imgcode collate SQL_Latin1_General_CP1_CI_AS not in (
      select img.imgcode from myapp.dbo.ItineraryImages img 
      where img.ItineraryNo = @itineraryNo and img.publishversion = @PublishVersion
       and img.CpntType = 'stay' and img.CpntCode=@hotelCode)
END