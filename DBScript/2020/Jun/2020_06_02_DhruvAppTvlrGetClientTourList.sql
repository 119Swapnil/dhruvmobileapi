use myapp
go
ALTER PROCEDURE [dbo].[DhruvAppTvlrGetClientTourList]
	@Clientcode varchar(10),
    @BranchCode varchar(10)='XXXX'

AS
Begin

	Select bih.BranchCode,
			bih.SeqNo,
			bih.PublishVersion,
			bih.ItineraryNo,
		   bih.Itinerary_Name,
		   bih.Itinerary_Short_Note,
		   bih.NoOfDays,
		   bih.FromDate,
		   bih.ToDate,
		   bih.BranchCode,
		   bih.NoOfPax_Adult,
		   bih.Quoteno,
		   bih.BookingNo,
		   bih.NoOfPax_Infant,
		   bih.NofPax_Child,
		   bih.ItineraryFileUri,
		   bih.Notes,
		   bih.IsCurrentItin,
		   bih.IsCurrentQuote,case bih.ClientType when 'GRP' then 1 else 0 end IsGroup ,
		   bih.StatusCode,
		   bih.ItineraryNo,
		   bih.PublishVersion,
		   (selecT top 1 Description from myapp.dbo.ItineraryCoverLetter icl
            where icl.ItineraryNo = bih.ItineraryNo And icl.PublishVersion  = bih.PublishVersion) Description,
           (selecT top 1 case right(iimg.ImagePath,4) when '.jpg' then 
			replace(replace(replace (IsNull(iimg.ImagePath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/'), '.jpg', '.jpg')
				when '.png' then 
			replace(replace(replace (IsNull(iimg.ImagePath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/'), '.png', '.png')
				else 
			replace(replace (IsNull(iimg.ImagePath,''),'i:\dhruv\doc\images\',(SELECT TOP 1 [Value] collate SQL_Latin1_General_CP1_CI_AS FROM sales97.dbo.Pointer WHERE Code = 'HstIURL' AND CompanyCode = 'ITUK') + 'ilimages/'), '\', '/')
			end ImagePath from myapp.dbo.ItineraryImages as iimg 
            where iimg.ItineraryNo = bih.ItineraryNo And iimg.PublishVersion  = bih.PublishVersion
            And iimg.CpntType ='page' And iimg.PageInfo = 'cover' And iimg.CpntCode = 1 and bih.BranchCode = @BranchCode
            order by iimg.seqno desc) ImagePath
			From myapp.dbo.BkgItineraryHeader as bih
            Where isnull(bih.FrzInd,0)=0
			and bih.IsCurrentItin = 1 and bih.BranchCode = @BranchCode 
            and bih.ClientCode = @Clientcode and bih.publishversion = (select max(bih1.publishversion) from myapp.dbo.bkgitineraryheader bih1 
            Where  bih1.ClientCode = @Clientcode And bih.ItineraryNo = bih1.ItineraryNo and bih1.BranchCode = @BranchCode)
END