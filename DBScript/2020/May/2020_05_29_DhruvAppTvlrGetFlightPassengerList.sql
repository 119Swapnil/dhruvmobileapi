use myapp
go
ALTER PROCEDURE [dbo].[DhruvAppTvlrGetFlightPassengerList]
	@itineraryNo int

AS
BEGIN
	DECLARE @PublishVersion int
	Set @PublishVersion = (Select max(publishversion)  
	From myapp.dbo.BkgItineraryHeader as bih 
	Where bih.ItineraryNo = @itineraryNo and isnull(bih.FrzInd,0)=0) 

    SELECT idc.vehicle as vehicleNo, idc.airline_name airlinename,tcs.ticketno ETicketNo, tcs.clientcode, 
       Isnull(cl.title, '') + ' ' + Isnull(cl.fname, '') + ' ' + Ltrim(Isnull(mname, '') + ' ') + Isnull(cl.lname, '') ClientName, 
       tcs.seqno_travel, Ltrim(Isnull(cl.title, '') + ' ' + Isnull(cl.fname, '') + ' ' + Isnull(cl.lname, '')) ClientNameAsOnPP, 
       tcs.seatnumber AS seatNo, tcs.frequentflyerno  AS FlyerNo, 
       CASE Isnull(idc.pnrno, '') WHEN '' THEN tcs.pnrno ELSE idc.pnrno END pnrno
      
        FROM   myapp.dbo.travelclients tcs 
               LEFT JOIN myapp.dbo.itinerarydaycomponent AS idc 
                      ON idc.itineraryno = tcs.itineraryno 
                         AND idc.publishversion = tcs.publishversion 
                         AND idc.seqno_travel = tcs.seqno_travel 
                         AND idc.cpnttype IN ( 'iflt', 'dflt' ) 
               LEFT JOIN myapp..client cl 
                      ON cl.clientcode = tcs.clientcode 
                         AND cl.itineraryno = tcs.itineraryno 
                         AND cl.publishversion = tcs.publishversion 
        WHERE  tcs.itineraryno = @ItineraryNo 
               AND tcs.publishversion = @PublishVersion
               and isnull(tcs.FrzInd,0)=0 and isnull(idc.FrzInd,0)=0 and isnull(cl.FrzInd,0)=0

			
END