use myapp
go

ALTER PROCEDURE [dbo].[DhruvAppTvlrGetContactDetails]
	@ItineraryNo int

AS
Begin
    Declare @PublishVersion int
    Set @PublishVersion = (select max(bih.publishversion) 
	from myapp.dbo.bkgitineraryheader bih 
	where bih.ItineraryNo = @itineraryNo)

    SELECT br.BranchCode,Branch_Name,add1,Add2,Add3,City,State,Country,PinCode,Contact_Telephone1,Contact_Telephone2
    Contact_Fax,Contact_EmergencyMobile,Contact_Email,Text_OfficeHours,Text_EmergencyMobile,RegisteredIn--,WebsiteURL
     FROM presets..Branch br
    left join myapp..bkgitineraryheader bih
    on br.BranchCode=bih.BranchCode
    where bih.ItineraryNo=@itineraryNo and bih.publishversion=@PublishVersion    
    and isnull(br.FrzInd,0)=0 and isnull(bih.FrzInd,0)=0 
End