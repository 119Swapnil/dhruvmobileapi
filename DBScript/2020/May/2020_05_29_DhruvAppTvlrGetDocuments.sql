use myapp
go

ALTER PROCEDURE [dbo].[DhruvAppTvlrGetDocuments]
		@ItineraryNo int

AS
BEGIN
	Declare @PublishVersion int
 Select top 1 @PublishVersion = bih.publishversion 
from myapp.dbo.bkgitineraryheader bih where bih.ItineraryNo = @ItineraryNo
order by seqno desc

select distinct * from 
(selecT SeqNo,DocDesc,DocPath,DocType,QuoteNo,BkgNo as BookingNo,Accountcode,ItineraryNo,[FileName],DocumentType,DocumentSubType,DhruvAppInd
 from Mailstore.dbo.SECorrespondence 
where ItineraryNo=@itineraryNo and isnull(DhruvAppInd,0)=1 and isnull(FrzInd,0)=0
Union All
 Select sed.CorresSeqno as SeqNo, sed.DocDesc, sed.DocPath, sed.DocType, sed.QuoteNo, sed.BookingNo , sed.AccountCode, sed.ItineraryNo, 
sed.[FileName],sed.DocumentType, sed.DocumentSubType ,0 as DhruvAppInd
 From MyApp.dbo.SECorrespondence sed
Where sed.itineraryNo = @itineraryNo and sed.PublishVersion = @PublishVersion and isnull(sed.FrzInd,0)=0) doc
    

END