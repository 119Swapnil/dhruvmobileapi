use myapp
go

CREATE PROCEDURE [dbo].[DhruvApp_Tvlr_GetSupplierContactList] @itineraryNo INT
                                                         
AS 
  BEGIN 
Declare @PublishVersion int
If Exists(Select Top 1 bih.ItineraryNo from myapp.dbo.BrochureItineraryHeader bih where bih.ItineraryNo = @itineraryNo)
 Set @PublishVersion = (Select max(bih.publishVersion) from myapp.dbo.BrochureItineraryHeader bih where bih.ItineraryNo = @itineraryNo)
else
 Set @PublishVersion = (select max(bih.publishversion) from myapp.dbo.bkgitineraryheader bih where bih.ItineraryNo = @itineraryNo)

SELECT IDC.CpntCode SupplierCode,IDC.CpntName SupplierName,HotelAddress as Address,HotelTelephone ContactNo,'' as Email,
'' as EmergencyNo,IDC.CpntType
FROM   myapp.dbo.ItineraryDayComponent IDC
inner join myapp.dbo.BkgItineraryDays bid on bid.ItineraryNo = IDC.ItineraryNo and bid.PublishVersion = IDC.PublishVersion and bid.ItinDate = IDC.DepDate
WHERE  IDC.ItineraryNo = @itineraryNo  and IDC.CpntType='STAY'
       AND IDC.PublishVersion = (SELECT Max(publishversion)
                                 FROM   myapp.dbo.bkgitineraryheader bih
                                 WHERE  bih.ItineraryNo = @itineraryNo ) 
--order by IDC.Seqno asc
union all

select distinct ite.ExpertCode SupplierCode,ite.ExpertName SupplierName,'' as Address,
ite.PhoneNo as ContactNo,ite.EmailId as Email,'' as EmergencyNo,'' as CpntType
from myapp.dbo.ItineraryTravelExpert ite
where ite.ItineraryNo=@itineraryNo and ite.publishversion = @PublishVersion
union all

SELECT DISTINCT isc.AccountCode SupplierCode, isc.AccountName as SupplierName,isc.Country as Address,'' as ContactNo
,isc.Email as Email,isc.EmergencyNo as EmergencyNo,ISNULL(isc.Cpnt_Type,'') as CpntType
               FROM   myapp.dbo.ItinerarySuppilerContactDetails isc 
       LEFT JOIN myapp.dbo.ItineraryDayComponent idc ON idc.ItineraryNo = isc.ItineraryNo 
                 AND idc.publishversion = isc.publishversion AND idc.suppliercode = isc.accountcode 
WHERE  isc.ItineraryNo = @itineraryNo 
       AND isc.publishversion = @PublishVersion 
union all

 select idc.Airline_code SupplierCode,idc.Airline_name SupplierName,'' as Address,idc.AirlineContactNo as ContactNo
,'' as Email,'' as EmergencyNo,idc.cpnttype
    from myapp.dbo.ItineraryDayComponent IDC
    where ItineraryNo=@itineraryNo and CpntType in ('IFLT','DFLT') and publishversion = @PublishVersion

End
