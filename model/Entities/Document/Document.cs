﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Document
{
    public class Document
    {
        private string docUrl;

        public string DocDesc { get; set; }
        public string DocPath
        {
            get; set;
        }
        public string DocUrl
        { get; set; }


        public string CrUId { get; set; }
        public DateTime CrDate { get; set; }
        public DateTime CrTime { get; set; }
        public string UpdPlace { get; set; }
        public string DocType { get; set; }
        public int QuoteNo { get; set; }
        public int BkgNo { get; set; }
        public int ItineraryNo { get; set; }
        public string Accountcode { get; set; }
        public string FileName { get; set; }
        public string DocumentType { get; set; }
        public string DocumentSubType { get; set; }
        public string Extension { get; set; }
        public bool PublishToMyDhruvInd { get; set; }
        //public bool FromMyDhruvINd { get; set; }
        public bool DhruvAppInd { get; set; }
        public int SeqNo { get; set; }
    }
}