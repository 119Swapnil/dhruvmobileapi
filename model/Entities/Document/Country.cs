﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Document
{
    public class Country
    {
        public string ItineraryNo { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string DocumentImagePath { get; set; }
    }
}
