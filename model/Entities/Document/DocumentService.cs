﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using Microsoft.AspNetCore.Http;

namespace ISG.DhruvMobileAPI.Model.Entities.Document
{
    public class DocumentService
    {
        private IDocumentRepository _documentRepository;
        public DocumentService(IDocumentRepository documentRepository)
        {
            _documentRepository = documentRepository;
        }

        public async Task<BaseModel> SaveAccountDocumentAsync(Document accountDocument, string rootPath, IFormFile formFile)
        {
            return await _documentRepository.SaveAccountDocumentAsync(accountDocument, rootPath, formFile);
        }

        public async Task<bool> DeleteDocumentAsync(int seqNo)
        {
            return await _documentRepository.DeleteDocumentAsync(seqNo);
        }

        public async Task<List<Document>> GetDocumentAsync(int ItineraryCode)
        {
            return await _documentRepository.GetDocumentAsync(ItineraryCode);
        }

        public async Task<BaseModel> GetCountryDetails(int itinerayno)
        {
            return await _documentRepository.GetCountryDetail(itinerayno);
        }

    }
}
