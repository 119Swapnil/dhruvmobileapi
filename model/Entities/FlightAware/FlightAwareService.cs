﻿using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.FlightAware
{
    public class FlightAwareService
    {
        private IFlightAwareRepository flightAwareRepository;

        public FlightAwareService()
        {

        }

        public FlightAwareService(IFlightAwareRepository flightAwareRepository)
        {
            this.flightAwareRepository = flightAwareRepository;
        }

        public async Task<object> GetFlightInfoAndStatusAsync(string flightno)
        {
            return await flightAwareRepository.FlightAwareVer3(flightno);
        }


    }
}
