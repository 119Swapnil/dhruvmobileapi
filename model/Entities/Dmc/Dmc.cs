﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Dmc
{
    public class Dmc
    {
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string Add1 { get; set; }
        public string Add2 { get; set; }
        public string Add3 { get; set; }
        public string Add4 { get; set; }
        public string Add5 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public List<DmcContact> Contactdetails { get; set; }

    }
}
