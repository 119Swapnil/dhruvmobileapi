﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Dmc
{
    public class DmcContact
    {
        public string DataTypeCode { get; set; }
        public string DataTypeName { get; set; }
        public string DataValue { get; set; }

    }
}
