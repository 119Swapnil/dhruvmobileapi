﻿using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.Dmc
{
    public class DmcService
    {
        private IDmcRepository _dmcRepository;

        public DmcService(IDmcRepository dmcRepository)
        {
            this._dmcRepository = dmcRepository;
        }

        public async Task<ISG.DhruvMobileAPI.Model.Common.BaseModel> GetDmcListWithDetails(string tourcode)
        {
            return await _dmcRepository.GetDmcListWithDetailsAsync(tourcode);
        }
    }
}
