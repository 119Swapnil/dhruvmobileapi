﻿
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.Stay
{
    public class StayService
    {
        private IStayRepository _stayRepository;

        public StayService(IStayRepository stayRepository)
        {
            _stayRepository = stayRepository;
        }
            
        public async Task<BaseModel> GetStayListAsync(int itineraryno)
        {
           return await _stayRepository.GetStayListAsync(itineraryno);
        }

        public async Task<BaseModel> GetStayImageGalleryAsync(string hotelcode, string itineraryno)
        {
            return await _stayRepository.GetStayGalleryImagesAsync(hotelcode, itineraryno);
        }

        public async Task<BaseModel> GetStayFacilityAsync(string hotelcode)
        {
            return await _stayRepository.GetStayFacilityAsync(hotelcode);
        }
    }
}
