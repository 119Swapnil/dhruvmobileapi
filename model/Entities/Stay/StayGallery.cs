﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Stay
{
    public class StayGallery
    {
        public string ImgCaption { get; set; }
        //public string HotelImageType { get; set; }
        //public string ImgRight { get; set; }
        //public string ExpiryDate { get; set; }
        //public string ImgType { get; set; }
        public string ImagePath { get; set; }
    }
}
