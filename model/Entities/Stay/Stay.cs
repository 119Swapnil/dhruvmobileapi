﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Stay
{
    public class Stay 
    {

        public Boolean AllPax { get; set; }
        public DateTime ArrDate { get; set; }
        public string CpntName { get; set; }
        public string CpntCode { get; set; }
        public string CpntDescription { get; set; }
        public string ComponentText { get; set; }
        public DateTime DepDate { get; set; }
        public string FromPlace { get; set; }
        public int NoOfNights { get; set; }
        public int NoOfPax { get; set; }
        public int NoOfPax_Child { get; set; }
        public int NoOfPax_Infant { get; set; }
        public int NoOfRoom { get; set; }
        public string HotelEmailId { get; set; }
        public string HotelRating { get; set; }
        public string HotelTelephone { get; set; }
        public string MealBasis_Code_Short { get; set; }
        public string MealBasis_Desc { get; set; }
        public string RmType_Code_G { get; set; }
        public string RmType_Desc { get; set; }
        public string ImagePath { get; set; }

        public IEnumerable Stay_Gallery { get; set; }

        public IEnumerable Stay_Facility { get; set; }

    }
}
