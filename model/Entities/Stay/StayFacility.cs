﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Stay
{
    public class StayFacility
    {
        public string FacilityCode { get; set; }
        public string FacilityDesc { get; set; }
    }
}
