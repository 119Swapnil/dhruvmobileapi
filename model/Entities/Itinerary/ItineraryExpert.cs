﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Itinerary
{
   public class ItineraryExpert : BaseModel
    {
        public int Expertid { get; set; }
        public string ExpertCode { get; set; }
        public String ExpertFname { get; set; }
        public string ExpertLname { get; set; }
        public string ExpertText { get; set; }
        public string Expertdetailinfo { get; set; }
        public string ContactNumber { get; set; }
        public string EmailId { get; set; }
        public string Designation { get; set; }
        public string ImagePath { get; set; }
        public bool IsOwner { get; set; }
    }


    public class ItineraryExpertModel
    {
        public bool Success { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
        public object TourExpertList { get; set; }
    }
}
