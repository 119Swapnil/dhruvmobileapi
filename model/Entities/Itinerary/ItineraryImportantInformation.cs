﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Itinerary
{
  public  class ItineraryImportantInformation
    {
        public int ItineraryNo { get; set; }
        public int SeqNo { get; set; }
        public string Country_Code { get; set; }
        public string Country_Name { get; set; }

        public string Description { get; set; }
        public string AdditionalInformation { get; set; }
    }

    public class ItineraryImportantInformationModel
    {
        public bool Success { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
        public object ImportantInformation { get; set; }
    }
}
