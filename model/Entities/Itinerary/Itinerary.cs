﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Itinerary
{
    public class Itinerary : BaseModel
    {
       

        public DateTime ItinDate { get; set; }

        public string CpntType { get; set; }
        public string CpntCode { get; set; }
        public string CpntName { get; set; }

        public string FrCityCode { get; set; }
        public string ToCityCode { get; set; }
        public string FrCityName { get; set; }
        public string ToCityName { get; set; }

        public string FrAirportName { get; set; }
        public string ToAirportName { get; set; }

        public string NoOfNights { get; set; }

        public DateTime DepDate { get; set; }
        public TimeSpan DepTime { get; set; }
        public DateTime ArrDate { get; set; }
        public TimeSpan ArrTime { get; set; }

        public string Vehicle { get; set; }
        public string CpntDescription { get; set; }

        public string Proc_Type { get; set; }
        public string Proc_Type_Sub { get; set; }

        public string Cpnt_Type_Desc { get; set; }

        public string ClassOfTravel_Code { get; set; }
        public string CallOfTravel_Desc { get; set; }

        public string DepartureTerminal { get; set; }
        public string ArrivalTerminal { get; set; }

        public string Airline_Code { get; set; }
        public string Airline_Name { get; set; }

        public string OccType_Code { get; set; }
        public string OccType_Desc { get; set; }

        public string MealBasic_Code_Short { get; set; }
        public string MealBasic_Desc { get; set; }

        public string RmType_Code_G { get; set; }
        public string RmType_Desc { get; set; }

        public string NoOfRoom { get; set; }
        public string PNRNo { get; set; }
        public string SeatNos { get; set; }
        
        public bool OwnArranggement { get; set; }

        public string ETckt_URL { get; set; }

        public string TravelHeader { get; set; }
        public string TravelHeaderDescription { get; set; }
        public string ComponentText { get; set; }

        public int NoOfStops { get; set; }

        public string ComponentIcon { get; set; }

        public int AllPax { get; set; }
        public int NoOfPax { get; set; }

        public int NoOfPax_Child { get; set; }

        public int NoOfPax_Infant { get; set; }

        public string CpntDescription_Raw { get; set; }

        public string FromPlace { get; set; }

        public string ToPlace { get; set; }

        public string TransDesc { get; set; }
        public string FrAirportCode { get; set; }
        public string toAirportCode { get; set; }
        public double latitude { get; set; } = 0.0D;
        public double longitude { get; set; } = 0.0D;




    }
}
