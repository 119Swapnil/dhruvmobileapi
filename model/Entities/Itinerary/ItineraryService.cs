﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Tour;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
namespace ISG.DhruvMobileAPI.Model.Entities.Itinerary
{
  public class ItineraryService
    {
        private IItineraryRepository _itineraryRepository;
        public ItineraryService(IItineraryRepository itineraryRepository)
        {
            _itineraryRepository = itineraryRepository;
        }


        public async Task<BaseModel> GetDayWiseItineraryAsync(int ItineraryNo, int DayNo)
        {
            return await _itineraryRepository.GetDayWiseItinerary(ItineraryNo, DayNo);
        }

        public async Task<BaseModel> GetBriefItineraryAsync(int ItineraryNo)
        {
            return await _itineraryRepository.GetBriefItineraryAsync(ItineraryNo);
        }

        public async Task<BaseModel> GetCompleteItineraryAsync(int ItineraryNo)
        {
            return await _itineraryRepository.GetCompleteItineraryAsync(ItineraryNo);
        }




        public static implicit operator ItineraryService(TourService v)
        {
            throw new NotImplementedException();
        }
    }
}
