﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Itinerary
{
    public class ItineraryBrief
    {
        public int DayNo { get; set; }
        public DateTime FromDate { get; set; }
        public string City { get; set; }
        public string Component { get; set; }
        public string DayTitle { get; set; }
        public string Description { get; set; }
        public string ItinInBrief { get; set; }
        public string ItinAccom { get; set; }
        public string ShowPlaceTextInd { get; set; }
        public string Bf_MealBasic_Desc { get; set; }
        public string Ln_MealBasic_Desc { get; set; }
        public string Dn_MealBasic_Desc { get; set; }
        public DateTime TransferStartTime { get; set; }
        public string TransferDuration { get; set; }
        public string ImagePath { get; set; }
        public List<Itinerary> DayWiseItinerary { get; set; }
    }
}
