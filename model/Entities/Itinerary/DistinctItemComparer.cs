﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Itinerary
{
    public class DistinctItemComparer : IEqualityComparer<ItineraryBrief>
    {
        public bool Equals(ItineraryBrief x, ItineraryBrief y)
        {
            return x.DayNo == y.DayNo &&
                 x.FromDate == y.FromDate;
        }

        public int GetHashCode(ItineraryBrief obj)
        {
            return obj.DayNo.GetHashCode() ^ obj.FromDate.GetHashCode();
        }
    }
}
