﻿using System;
using System.Collections.Generic;
using System.Text;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;

using System.Threading.Tasks;

using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;

namespace ISG.DhruvMobileAPI.Model.Entities.Notification
{
    public class NotificationService
    {
        private INotificationRepository _notificationRepository;
        public NotificationService(INotificationRepository notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }

        public async Task<bool> PushNotification(string to, string body, string title, string APISecretKey, string APISenderId)
        {
            return await _notificationRepository.PushNotification(to, body, title, APISecretKey, APISenderId);
        }

        public bool RegisterUserForNotification(string id, string token)
        {
            return _notificationRepository.RegisterUserForNotification(id, token);
        }

        public async Task<bool> SentNotificationMessage(string message, List<Client> listUser, string APISecretKey, string APISenderId)
        {
            return await _notificationRepository.SentNotificationMessage(message, listUser, APISecretKey, APISenderId);
        }
    }
}
