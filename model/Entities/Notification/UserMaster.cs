﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Notification
{
    public class UserMaster
    {
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string AccountCode { get; set; }
        public string MobileNumber { get; set; }
        public string ConsultantId { get; set; }
        public string ConsultantEmailId { get; set; }
        public string LastRegistrationDetailSentMode { get; set; }
        public DateTime? LastRegistrationDetailSentOn { get; set; }
        public string LastRegistrationDetailSentBy { get; set; }
        public string LastOtpsent { get; set; }
        public DateTime? LastOtpsentOn { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedAt { get; set; }
        public string Salutation { get; set; }
        public int SeqNo { get; set; }
        public DateTime? LastRegistrationOtprequestedOn { get; set; }
        public string LastRegistrationOtpsent { get; set; }
        public DateTime? LastRegistrationOtpsentOn { get; set; }
        public bool? IsregistrationOtpvalidationDone { get; set; }
        public DateTime? RegistrationOtpvalidationRequestReceivedOn { get; set; }
        public DateTime? LastResetPasswordOtprequestedOn { get; set; }
        public string LastResetPasswordOtpsent { get; set; }
        public DateTime? LastResetPasswordOtpsentOn { get; set; }
        public bool? IsresetPasswordOtpvalidationDone { get; set; }
        public DateTime? ResetPasswordOtpvalidationRequestReceivedOn { get; set; }
        public int? LastResetPasswordLogSeqNo { get; set; }
        public int? LastRegistrationLogSeqNo { get; set; }
        public string DeviceIdentificationNumber { get; set; }
        public string FCMToken { get; set; }
    }
}
