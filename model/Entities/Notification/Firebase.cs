﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.Notification
{
    public class Firebase
    {
        [JsonProperty(PropertyName = "to")]
        public string To { get; set; }

        [JsonProperty(PropertyName = "priority")]
        public string Priority { get; set; }

        [JsonProperty(PropertyName = "content_available")]
        public bool Content_Available { get; set; }

        [JsonProperty(PropertyName = "notification")]
        public NotificationModel Notification { get; set; }

        public bool Result { get; set; }
    }

    public class NotificationModel
    {
        public string Body { get; set; }
        public string title { get; set; }
        public string sound { get; set; }
    }
}
