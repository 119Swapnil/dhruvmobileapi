﻿using ISG.DhruvMobileAPI.Model.Common;

using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.Flight
{
    public class FlightService
    {
        private IFlightRepository _flightRepository;

        public FlightService(IFlightRepository flightRepository)
        {
            _flightRepository = flightRepository;
        }


        public async Task<BaseModel> GetFlightListAsync(int itineraryno)
        {
            return await _flightRepository.GetFlightListAsync(itineraryno);
        }

        public async Task<BaseModel> GetFlightPassenger(int itineraryno)
        {
            return await _flightRepository.GetFlightPassenger(itineraryno);
        }

    }
}
