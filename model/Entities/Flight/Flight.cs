﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Flight
{
    public class Flight 
    {
       
        public string FrCityCode { get; set; }
        public string ToCityCode { get; set; }
        public string FrCityName { get; set; }
        public string ToCityName { get; set; }

        public string FrAirportName { get; set; }
        public string ToAirportName { get; set; }

        public string NoOfNights { get; set; }

        public DateTime DepDate { get; set; }
        public TimeSpan DepTime { get; set; }
        public DateTime ArrDate { get; set; }
        public TimeSpan ArrTime { get; set; }

        public string Vehicle { get; set; }

        public string ClassOfTravel_Code { get; set; }
        public string CallOfTravel_Desc { get; set; }

        public string DepartureTerminal { get; set; }
        public string ArrivalTerminal { get; set; }
       
        public string Airline_Code { get; set; }
        public string Airline_Name { get; set; }

        public string PNRNo { get; set; }
        public string SeatNos { get; set; }

        public bool OwnArranggement { get; set; }

        public string ETckt_URL { get; set; }

        public int NoOfStops { get; set; }

        public string FrAirportCode { get; set; }
        public string toAirportCode { get; set; }

        public string TravelHeader { get; set; }
        public string TravelHeaderDescription { get; set; }
        public string FrPoint { get; set; }
        public string ToPoint { get; set; }
        public string ProviderName { get; set; }
        public string Cpnt_Type_Desc { get; set; }
        public string CpntType { get; set; }

    }
}
