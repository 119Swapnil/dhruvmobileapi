﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Flight
{
    public class FlightPassenger
    {
        public string VehicleNo { get; set; }
        public string AirlineName { get; set; }
        public string Pnrno { get; set; }
        public string ClientNameAsOnPP { get; set; }
        public string ETicketNo { get; set; }
        public string clientcode { get; set; }
        public string ClientName { get; set; }
        public string SeatNo { get; set; }
        public string SeatFlyerNo { get; set; }
    }
}
