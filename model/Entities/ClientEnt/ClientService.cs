﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;

namespace ISG.DhruvMobileAPI.Model.Entities.ClientEnt
{
    public class ClientService
    {
        private IClientRepository _clientRepository;
        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task<Client> GetClientInformationAsync(string EmailId, string Password)
        {
            return await _clientRepository.AuthenticateUserAsync(EmailId, Password);
        }

        public async Task<BaseModel> GetPaxCountAsync(string accountcode, DateTime fromdate)
        {
            return await _clientRepository.GetPaxCountAsync(accountcode, fromdate);
        }

        public async Task<object> GetClientsWithDetails(string tourCode, DateTime depDate)
        {
            return await _clientRepository.GetClientsWithDetails(tourCode, depDate);
        }
    }
}
