﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.ClientEnt
{
    public class NextOfKin
    {
        public string NOFK_Title { get; set; }
        public string NOFK_FName { get; set; }
        public string NOFK_MName { get; set; }
        public string NOFK_LName { get; set; }
        public Address NOFK_Address { get; set; }
        public IEnumerable<ContactNo> NOFK_ContactNo { get; set; }

    }

    public class Address
    {
        public string Add1 { get; set; }
        public string Add2 { get; set; }
        public string Add3 { get; set; }
        public string Add4 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        

    }

    public class ContactNo
    {
        public string DataTypeCode { get; set; }
        public string DataTypeName { get; set; }
        public string DataValue { get; set; }
    }
}
