﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.ClientEnt
{
    public class Passport
    {
        public DateTime DOB { get; set; }
        public string TitleOnPP { get; set; }
        public string FirstNameOnPP { get; set; }
        public string MiddleNameOnPP { get; set; }
        public string LastNameOnPP { get; set; }
        public string DocNo { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime IssueDate { get; set; }
        public string IssuePlace { get; set; }
        public string Nationality { get; set; }
    }
}
