﻿using ISG.DhruvMobileAPI.Model.Common;

using System;
using System.ComponentModel.DataAnnotations;

namespace ISG.DhruvMobileAPI.Model.Entities.ClientEnt
{
    public class Client      
    {

        public bool Result { get; set; }

        public string Message { get; set; }

        public string AccountCode { get; set; }

        public int  BkgNo { get; set; }

        public int QuoteNo { get; set; }

        [Required(ErrorMessage = "Enter Project Name!")]
        public string EMailId { get; set; }

        [Required(ErrorMessage = "Enter Project Name!")]
        public string OnlinePassword { get; set; }

        public string Title { get; set; }

        public string FName { get; set; }

        public string MName { get; set; }

        public string LName { get; set; }

        public DateTime DOB { get; set; }

        public string FCMToken { get; set; }

        public string ApiToken { get; set; }

        public string TicketNo   { get; set; }

        public string ticketinfo { get; set; }

        public string ItinClientName { get; set; }

        public String NameAsOnPP { get; set; }

        public string PNRNo { get; set; }
        public string SeatNumber { get; set; }

        public Passport Passport_info { get; set; }
        public NextOfKin NOFK_info { get; set; }

    }

    public class PaxCount
    {
        public int Adults { get; set; }
        public int Children { get; set; }
        public int Infant { get; set; }
    }


}

