﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Common
{
    public class Pointer
    {
        public static readonly GenericCodeDescription NoofDaysForAPI = new GenericCodeDescription("APIDAYS", "Error. No of days invalid.");
        public static readonly GenericCodeDescription PhysicalPathUrlExternal = new GenericCodeDescription("PHYPRFEX", "Physical path url prefix external");
    }
}