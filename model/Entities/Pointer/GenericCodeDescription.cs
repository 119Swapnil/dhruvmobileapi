﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Common
{
  public class GenericCodeDescription
  {
    public string Code { get; set; }
    public string Description { get; set; }
    public string AdditionalInformation { get; set; }
    public string Display
    {
      get { return this.Description + (this.Code!=null && this.Code!=""?(" (" + this.Code + ")"):""); }
    }
    public Dictionary<string, object> ExtraData { get; set; }
    public GenericCodeDescription()
    {
      ExtraData = new Dictionary<string, object>();
    }
    public GenericCodeDescription(string code, string description)
      : this()
    {
      Code = code;
      Description = description;
    }
    public GenericCodeDescription(string code, string description, string additionalInformation)
      : this(code, description)
    {
      AdditionalInformation = additionalInformation;
    }
  }
  public class GenericCodeDescriptionCollection : List<GenericCodeDescription>
  {
    public GenericCodeDescription GetGenericCodeDescriptionBy(string Code)
    {
      return this.Find(p => p.Code.ToLower() == Code.ToLower());
    }
  }

  public class GenericCodeDescriptionWithType
  {
    public string Code { get; set; }
    public string Description { get; set; }
    public string TypeCode { get; set; }
    public string TypeDescription { get; set; }
    public GenericCodeDescriptionWithType(string code, string description, string typeCode, string typedescription)
    {
      Code = code;
      Description = description;
      TypeCode = typeCode;
      TypeDescription = typedescription;
    }
  }

  public class GenericCodeDescriptionWithTypeCollection : List<GenericCodeDescriptionWithType>
  {
  }
}
