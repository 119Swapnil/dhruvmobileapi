﻿using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;

namespace ISG.DhruvMobileAPI.Model.Entities.Pointer
{
    public class PointerService
    {
        private IPointerRepository _pointerRepository;
        public PointerService(IPointerRepository pointerRepository)
        {
            _pointerRepository = pointerRepository;
        }

        public string GetPointerValueBy(string pointerCode, string companyCode, string branchCode, string defaultBranchCode)
        {
            return _pointerRepository.GetPointerValueBy(pointerCode, companyCode, branchCode, defaultBranchCode);
        }
    }
}
