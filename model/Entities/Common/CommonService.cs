﻿using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Common
{
  public class CommonService
  {
    private ICommonRepository _commonRepository;
    public CommonService(ICommonRepository commonRepository)
    {
      _commonRepository = commonRepository;
    }
    public int GetLatestPublishVersion(int itineraryNo)
    {
      return _commonRepository.GetLatestPublishVersion(itineraryNo);
    }
    public List<string> GetCompanyBranchCode(int itineraryNo, int publishVersion)
    {
      return _commonRepository.GetCompanyBranchCode(itineraryNo, publishVersion);
    }
    public string GetDhruvAppVirtualCabinetUrl(int itineraryNo)
    {
      return _commonRepository.GetDhruvAppVirtualCabinetUrl(itineraryNo);
    }
  }
}
