﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.TourLeader
{
    public class TourLeader
    {
       

        public string TourLeaderCode { get; set; }
        public string TourLeaderName { get; set; }
        public string TourLeaderBlurb { get; set; }
        public string TourLeaderImage { get; set; }
        public string TourLeaderDesignation { get; set; } 
        public string QuoteText { get; set; }
        public string ImagePath { get; set; }
        public string TourLeaderBlurb_Raw { get; set; }
        public string TLusercode { get; set; }
        public string Password { get; set; }
        public string ApiToken { get; set; }
    }
}
