﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.TourLeader
{
    public class TourLeaderService
    {
        private ITourLeaderRepository _tourLeaderRepository;

        public TourLeaderService(ITourLeaderRepository tourLeaderRepository)
        {
            _tourLeaderRepository = tourLeaderRepository;
        }

        public async Task<BaseModel> GetTourLeaderListAsync(int itineraryno)
        {
           return await _tourLeaderRepository.GetTourLeaderListAsync(itineraryno);
        }

        public async Task<TourLeader> TourLeaderLoginAsync(TourLeader tourLeader)
        {
            return await _tourLeaderRepository.AuthenticateTL(tourLeader);
        }
    }
}
