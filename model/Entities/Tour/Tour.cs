﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Tour
{
    public class Tour
    {
        public string TourCode { get; set; }
        public string ItineraryNo { get; set; }
        public string TourName { get; set; }
        public string Itinerary_Name { get; set; }
        public string Itinerary_Short_Note { get; set; }
        public int NoOfDays { get; set; }
        public int NoofNights { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        //  public int NoOfPax_Adult { get; set; }
        public int Quoteno { get; set; }
        public int BookingNo { get; set; }
        //  public int NoOfPax_Infant { get; set; }
        //  public int NofPax_Child { get; set; }
        public string ItineraryFileUri { get; set; }
        public string Notes { get; set; }
        public bool IsCurrentItin { get; set; }
        public bool IsCurrentQuote { get; set; }
        public string StatusCode { get; set; }
        public string ImagePath { get; set; }
        public string PageInfo { get; set; }
        public string Description { get; set; }
        public bool IsGroup { get; set; }
        public string BranchCode { get; set; }
    }
}
