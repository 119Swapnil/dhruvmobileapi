﻿using System.Collections;
using System.Collections.Generic;

namespace ISG.DhruvMobileAPI.Model.Entities.Tour
{
    public class ImportantInfo
    {
        public string Country_Code { get; set; }
        public string Country_Name { get; set; }
        public string Description { get; set; }
        public string AdditionalInformation { get; set; }

        public IEnumerable CountryInfoList { get; set; }
    }

    public class CountryInfo
    {
        public string ProductType { get; set; }
        public int ProductCode { get; set; }
        public string  TextType { get; set; }
        public string TextTypeDesc { get; set; }
        public string MemoText { get; set; }
        public string ProductTypeCode { get; set; }
        public string ProductTypeDesc { get; set; }


    }
}
