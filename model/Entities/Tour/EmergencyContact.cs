﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Tour
{
    public class EmergencyContact
    {
        public string ContactType { get; set; }
        public string ContactName { get; set; }
        public string ContactEmailid { get; set; }
        public string ContactNumber { get; set; }

    }
}
