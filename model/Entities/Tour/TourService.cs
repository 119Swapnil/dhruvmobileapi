﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;
using ISG.DhruvMobileAPI.Model.Entities.TourLeader;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.Tour
{
    public class TourService
    {
        private ITourRepository _tourRepository;

        public TourService(ITourRepository tourRepository)
        {
            _tourRepository = tourRepository;
        }

        public async Task<BaseModel> GetUserToursAsync(string clientCode, string branchCode)
        {
            return await _tourRepository.GetTourList(clientCode, branchCode);
        }

        public async Task<BaseModel> GetItineraryExpert(int ItineraryNo)
        {
            return await _tourRepository.GetItineraryExpert(ItineraryNo);
        }

        public async Task<BaseModel> GetItineraryImportantInformation(int ItineraryNo)
        {
            return await _tourRepository.GetItineraryImportantInformation(ItineraryNo);
        }


        public async Task<object> GetCompanyContactDetailsAsync(string itineraryno)
        {
            return await _tourRepository.GetCompanyContactDetails(itineraryno);
        }

        public async Task<object> GetTLToursAsynch(string accountcode)
        {
            return await _tourRepository.GetTLTours(accountcode);
        }
        public async Task<object> GetSupplierContactAsync(string itineraryno)
        {
            return await _tourRepository.GetSupplierContacts(itineraryno);
        }
    }
}
