﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Entities.Weather
{
    public class Position
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
