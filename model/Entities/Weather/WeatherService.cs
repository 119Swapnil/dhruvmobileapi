﻿using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.Entities.Weather
{
    public class WeatherService
    {
        private IWeatherRepository weatherRepository;

       

        public WeatherService(IWeatherRepository weatherRepository)
        {
            this.weatherRepository = weatherRepository;
        }

        public async Task<object> GetWeatherByGeoPosAsync(double lat, double lng, DateTime date)
        {
           var res = await weatherRepository.GetWeatherByGeoPosAsync(lat, lng, date);
            return res;
        }

        public async Task<List<string>> GetCityLocId(List<Position> positions)
        {

           var res = await weatherRepository.GetLocationKeyAsync(positions);
            return res;
        }

        public async Task<object> GetAccuWeather5DaysAsync(string cityid)
        {
            return await weatherRepository.AccuWeather5DaysAsync(cityid);
        }

        public async Task<object> GetAccuWeather12HoursAsync(string cityid)
        {
            return await weatherRepository.AccuWeather12HoursAsync(cityid);
        }
    }
}
