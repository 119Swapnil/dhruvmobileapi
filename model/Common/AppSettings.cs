﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.TraversAPI.Model.Common
{
  public  class AppSettings
    {
        public string key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
    }
}
