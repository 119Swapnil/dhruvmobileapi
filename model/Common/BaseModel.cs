﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.Common
{
    public class BaseModel
    {
        public bool Result { get; set; }
        public string Message { get; set; }
        public IEnumerable listObject { get; set; } = null;
        public object classobject { get; set; }

        public BaseModel(bool result, string errmsg)
        {
            this.Result = result;
            this.Message = errmsg;
        }

        public BaseModel()
        {
        }
    }
}
