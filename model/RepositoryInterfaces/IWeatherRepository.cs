﻿using ISG.DhruvMobileAPI.Model.Entities.Weather;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IWeatherRepository
    {
        Task<object> GetWeatherByGeoPosAsync(double lat, double lon, DateTime date);
        Task<List<string>> GetLocationKeyAsync(List<Position> listcities);
        Task<object> AccuWeather5DaysAsync(string cityid);
        Task<object> AccuWeather12HoursAsync(string cityid);
    }
}
