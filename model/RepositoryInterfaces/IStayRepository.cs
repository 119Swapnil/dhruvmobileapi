﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Stay;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IStayRepository
    {
        Task<BaseModel> GetStayListAsync(int itineraryno);
        Task<BaseModel> GetStayFacilityAsync(string hotelcode);
        Task<BaseModel> GetStayGalleryImagesAsync(string hotelcode, string Itineraryno);
    }
}
