﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IPointerRepository
    {
        string GetPointerValueBy(string pointerCode, string companyCode, string branchCode, string defaultBranchCode);
    }
}
