﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Document;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IDocumentRepository
    {
        Task<BaseModel> SaveAccountDocumentAsync(Document accountDocument, string rootPath, IFormFile formFile);
        Task<bool> DeleteDocumentAsync(int seqNo);
        //List<AccountDocument> GetDocument(int ItineraryHeaderSeqNo);
        Task<List<Document>> GetDocumentAsync(int itineraryNo);
        Task<BaseModel> GetCountryDetail(int itineraryNo);
    }
}
