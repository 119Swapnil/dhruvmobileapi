﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;


namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IClientRepository
    {
        Task<Client> AuthenticateUserAsync(string EmailId, string Password);
        Task<BaseModel> GetPaxCountAsync(string accountcode, DateTime fromdate);
        Task<BaseModel> GetClientsWithDetails(string tourcode, DateTime fromdate);
    }
}
    