﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Flight;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IFlightRepository
    {
        Task<BaseModel> GetFlightListAsync(int itineraryno);
        Task<BaseModel> GetFlightPassenger(int itineraryno);
    }
}
