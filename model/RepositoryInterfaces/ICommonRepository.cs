﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
  public interface ICommonRepository
  {
    int GetLatestPublishVersion(int itineraryNo);
    List<string> GetCompanyBranchCode(int itineraryNo, int publishVersion);
    string GetDhruvAppVirtualCabinetUrl(int itineraryNo);
  }
}
