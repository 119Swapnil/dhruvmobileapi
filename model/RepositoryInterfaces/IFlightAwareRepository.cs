﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IFlightAwareRepository
    {
        Task<object> FlightAwareVer3(string flightno);
    }
}
