﻿using ISG.DhruvMobileAPI.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface IDmcRepository
    {
        Task<BaseModel> GetDmcListWithDetailsAsync(string tourcode);
    }
}
