﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.TourLeader;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface ITourLeaderRepository
    {

        Task<BaseModel> GetTourLeaderListAsync(int itineraryno);
        Task<TourLeader> AuthenticateTL(TourLeader tourLeader);
    }
}
