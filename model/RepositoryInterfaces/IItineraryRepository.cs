﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;
namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
   public interface IItineraryRepository
    {
        Task<BaseModel> GetDayWiseItinerary(int ItineraryNo, int DayNo);
        Task<BaseModel>  GetBriefItineraryAsync(int ItineraryNo);
        Task<BaseModel> GetCompleteItineraryAsync(int ItineraryNo);
    }
}
