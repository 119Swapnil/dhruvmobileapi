﻿using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;
using ISG.DhruvMobileAPI.Model.Entities.Tour;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface ITourRepository
    {
        Task<BaseModel> GetTourList(string clientcode, string branchCode);
        Task<BaseModel> GetItineraryExpert(int ItineraryNo);
        Task<BaseModel> GetItineraryImportantInformation(int ItineraryNo);
        Task<BaseModel> GetCompanyContactDetails(string ItineraryNo);
        Task<BaseModel> GetTLTours(string accountcode);
        Task<BaseModel> GetSupplierContacts(string ItineraryNo);
    }
}
