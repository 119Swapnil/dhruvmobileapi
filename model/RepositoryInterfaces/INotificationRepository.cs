﻿using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Model.RepositoryInterfaces
{
    public interface INotificationRepository
    {
        Task<bool> PushNotification(string to, string body, string title, string APISecretKey, string APISenderId);
        bool RegisterUserForNotification(string id, string token);
        Task<bool> SentNotificationMessage(string message, List<Client> listUser, string APISecretKey, string APISenderId);
    }
}
