﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISG.DhruvMobileAPI.Repository.Common
{
    public class DataConstants
    {
        // SP Constants
        public const string DHRUV_APP_ValidateUserLogin = "DhruvAppTvlrValidateUserLogin";
        public const string DHRUV_APP_GetUserTourList = "DhruvAppTvlrGetClientTourList";
        public const string DHRUV_APP_TourLeaderList = "DhruvAppTvlrTourLeaderList";
        public const string DHRUV_APP_GetItineraryDayWiseComponent = "DhruvAppTvlrGetItineraryDayComponent";
        public const string DHRUV_APP_GetItineraryBrief = "DhruvAppTvlrGetItineraryInBrief";
        //public const string DHRUV_APP_GetCompleteItinerary = "Dhruv_App_GetCompleteItinerary";
        public const string DHRUV_APP_GetStayList = "DhruvAppTvlrGetStayList";
        public const string DHRUV_APP_GetFlightList = "DhruvAppTvlrGetFlightList";
        public const string DHRUV_APP_GetFlightPassengers = "DhruvAppTvlrGetFlightPassengerList";
        public const string DHRUV_APP_GetStayGalleryImages = "DhruvAppTvlrGetStayGalleryImages";
        public const string DHRUV_APP_GetStayFacility = "DhruvAppTvlrGetStayFacility";
        public const string DHRUV_APP_GetCountryDetails = "DhruvAppTvlrGetCountryDetails";
        public const string DHRUV_APP_GetDocuments = "DhruvAppTvlrGetDocuments";
        //public const string Dhruv_App_GetDocumentPublishVersion = "Dhruv_App_GetDocumentPublishVersion";
        public const string Dhruv_App_GetContactDetails = "DhruvAppTvlrGetContactDetails";
        public const string Dhruv_App_GetSupplierContactList = "DhruvAppTvlrGetSupplierContactList";
        //public const string DHRUVTLAPP_GetUserInformation = "GetUserInformation";
        public const string DHRUVAPP_GetTravelExpert = "DhruvAppTvlrTravelExpert";
        public const string DHRUVTLAPP_GetPointerValue = "sp_GetPointerValue";
        public const string DHRUVAPP_GetImportantInformation = "DhruvAppTvlrGetImportantInformation";
        public const string DHRUVAPP_GetCountryInformation = "DhruvAppTvlrGetCountryInfo";
        //public const string DHRUVTLAPP_GetTourLeader = "dhruvtvlapp_TourLeader";
        public const string DHRUVTLAPP_GetUserFireBaseToken = "Dhruv_App_GetUserFireBaseToken";
        //public const string DHRUVTLAPP_GetDocument = "dhruvtlapp_GetDocument";
        //public const string DHRUVTLAPP_GetDocumentBySeqNo = "dhruvtlapp_GetDocumentBySeqNo";
        public const string DHRUVTLAPP_TLTours = "DhruvAppTldrTourList";
        public const string DHRUVTLAPP_Login = "DhruvAppTldrLogin";
        public const string DHRUVTLAPP_DmcList = "DhruvAppTldrDmc";
        public const string DHRUVTLAPP_DmcContact = "DhruvAppTldrDmcContact";
        public const string DHRUVTLAPP_ClientPaxCount = "DhruvAppTldrPaxCount";
        public const string DHRUVTLAPP_ClientList = "DhruvAppTldrGetTourClients";
        public const string DHRUVTLAPP_ClientPPInfo = "DhruvAppTldrGetClientPPDetails";
        public const string DHRUVTLAPP_ClientnextkinName = "DhruvAppTldrGetClientNokName";
        public const string DHRUVTLAPP_ClientnextkinCntno = "DhruvAppTldrGetClientNokCntno";
        public const string DHRUVTLAPP_ClientDetails = "DhruvAppTldrGetClientDetails";
    }
}