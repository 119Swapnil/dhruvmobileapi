﻿using Dapper;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;
using ISG.DhruvMobileAPI.Model.Entities.Tour;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class TourRepository : ITourRepository
    {
        private string _connectionString;
        private string _tlconnString;

        public TourRepository(string connectionString, string tlconnString)
        {
            _connectionString = connectionString;
            _tlconnString = tlconnString;
        }


        public async Task<BaseModel> GetTourList(string clientcode, string branchCode)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<Tour>(sql: DataConstants.DHRUV_APP_GetUserTourList,
                                                param: new { Clientcode = clientcode, BranchCode = branchCode },
                                               commandType: CommandType.StoredProcedure);
                    if (result.ToList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Tour list retrived sucessfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Tour list not found for clientcode " + clientcode,
                            listObject = result
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive tour list, Error: " + e.Message

                };
            }
        }

        public async Task<BaseModel> GetItineraryExpert(int ItineraryNo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var multi = await connection.QueryAsync(sql: DataConstants.DHRUVAPP_GetTravelExpert,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                      commandType: CommandType.StoredProcedure);
                    if (multi.ToList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Tour expert list retrive successfully",
                            listObject = multi
                        };

                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Tour expert not found for itineraryno " + ItineraryNo,
                            listObject = multi
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive toue expert, Error :" + e.Message
                };

            }
        }

        public async Task<BaseModel> GetItineraryImportantInformation(int ItineraryNo)
        {
            try
            {
                
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var multi = await connection.QueryAsync<ImportantInfo>(sql: DataConstants.DHRUVAPP_GetImportantInformation,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                       commandType: CommandType.StoredProcedure);

                  //  var listCoutry = multi.ToList();

                    if (multi.AsList().Count != 0)
                    {

                        foreach(var item in multi)
                        {
                            var listinfo = await GetCountryInfo(ItineraryNo, item.Country_Code);

                            if (listinfo.Result)
                            {
                                item.CountryInfoList = listinfo.listObject;
                            }

                        }


                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Important information retrive successfully",
                            listObject = multi
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Important information not found for itinerary no" + ItineraryNo,
                            listObject = multi
                        };
                    }


                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive important information, Error :" + e.Message
                };

            }
        }

        public async Task<BaseModel> GetCountryInfo(int ItineraryNo, string Countrycode)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var multi = await connection.QueryAsync<CountryInfo>(sql: DataConstants.DHRUVAPP_GetCountryInformation,
                                                      param: new { ItineraryNo = ItineraryNo, Countrycode = Countrycode },
                                                       commandType: CommandType.StoredProcedure);


                  //  var infoList = multi.ToList<CountryInfo>();

                    if(multi.AsList().Count > 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Country info retrived",
                            listObject = multi
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Country info not found",
                            listObject = multi
                        };

                    }

            
                
                }
            }catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = true,
                    Message = "Error "+e.Message,
                    listObject = null
                };
            }

        }

        public async Task<BaseModel> GetCompanyContactDetails(string ItineraryNo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var multi = await connection.QueryAsync(sql: DataConstants.Dhruv_App_GetContactDetails,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                       commandType: CommandType.StoredProcedure);

                    if (multi.ToList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Company contact details retrive successfully",
                            listObject = multi
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Company contact details not found for itinerary no" + ItineraryNo,
                            listObject = multi
                        };
                    }


                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive important information, Error :" + e.Message
                };

            }
        }

        public async Task<BaseModel> GetTLTours(string accountcode)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_tlconnString))
                {
                    var result = await connection.QueryAsync<Tour>(sql: DataConstants.DHRUVTLAPP_TLTours,
                                                param: new { TourLeadedCode = accountcode },
                                               commandType: CommandType.StoredProcedure);
                    if (result.ToList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Tour list retrived sucessfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Tour list not found for clientcode " + accountcode,
                            listObject = result
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive tour leader assigned tours, Error :" + e.Message
                };
            }
        }

        public async Task<BaseModel> GetSupplierContacts(string ItineraryNo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var multi = await connection.QueryAsync(sql: DataConstants.Dhruv_App_GetSupplierContactList,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                       commandType: CommandType.StoredProcedure);

                    if (multi.ToList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Supplier contact details retrive successfully",
                            listObject = multi
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Supplier contact details not found for itinerary no" + ItineraryNo,
                            listObject = multi
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive Supplier contact details, Error :" + e.Message
                };
            }
        }
    }
}
