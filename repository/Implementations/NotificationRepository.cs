﻿using System;
using System.Collections.Generic;
using System.Text;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Model.Entities.Notification;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using ISG.DhruvMobileAPI.Repository.Common;
using System.Data;
using System.Linq;

using System.Diagnostics;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly string _connectionString;
        public NotificationRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<bool> PushNotification(string to, string body, string title, string APISecretKey, string APISenderId)
        {
            Firebase firebase = new Firebase();
            HttpRequestMessage httpRequest = null;
            HttpClient httpClient = null;

            var payload = new
            {
                to = "eQvCdkOvM4s:APA91bEfHXYBVa5_TihFgZpRNsBOQn0KPiNJvIKQTvsBL91e6Hdqj1UQir2zm8zNhb4lVopJNHnc41enfddOxpKfCLtrJi1WF6G-mNWOOlFNqSerNXPfP-hW14UzG4GNnzmSCh_4Ktq2",
                priority = "high",
               // notification = new { title = "Working Good", body = "Welcome"},
                data = new { Latitude = 12.4333, Longitude = 23.4433, Datetime = "3rdJun2019" }
            };

            var authorizationKey = string.Format("key={0}", "AAAAlZJ7_P0:APA91bFsZL-mKOwtUV9D3UVDi0UEaCnp3h-LZ6Bg-bLuRkNqDxPlp-Q6k3--7jfuHbFXm9XeTejOq35jH7t8ZXqUxZVLny_0ybJktzLFPDF5PmoJVdyYQ9k4_I9gmGq_EF-po0oYpME4");
            var senderId = string.Format("id={0}", "642407726333");
            var jsonBody = JsonConvert.SerializeObject(payload);

            try
            {
                httpRequest = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send");
                httpRequest.Headers.TryAddWithoutValidation("Authorization", authorizationKey);
                httpRequest.Headers.TryAddWithoutValidation("Sender", senderId);
                httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");


                httpClient = new HttpClient();

                var result = await httpClient.SendAsync(httpRequest);
                var contents = await result.Content.ReadAsStringAsync();

                var resource = JObject.Parse(contents);
                foreach (var property in resource.Properties())
                {
                    Console.WriteLine("{0} - {1}", property.Name, property.Value);

                    if (property.Name == "success")
                        firebase.Result = Convert.ToBoolean(property.Value);
                }
            }
            catch(Exception e)
            {
                throw;
            }
            finally
            {
                httpRequest.Dispose();
                httpClient.Dispose();
            }
            return firebase.Result;
        }

        public bool RegisterUserForNotification(string id, string token)
        {
            try
            {
                var user = new UserMaster() { EmailId = id, FCMToken = token };
                int rowsAffectedCount = 0;

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    //Once add column FireBaseToken in table UserMaster or related table then uncomment this section
                    rowsAffectedCount += connection.Execute(
                    sql: @"UPDATE [dbo].[MyAppUsers] SET [FCMToken] = @FireBaseToken WHERE [EMailId] = @EmailId",
                    param: new
                    {
                        @FireBaseToken = user.FCMToken,
                        @EmailId = user.EmailId

                    },
                    transaction: transaction,null, commandType: CommandType.Text);

                    transaction.Commit();

                    bool result = rowsAffectedCount > 0 ? true : false;
                    return result;
                    
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
            }
        }

        public async Task<bool> SentNotificationMessage(string message, List<Client> listUser, string APISecretKey, string APISenderId)
        {
            try
            {
                bool result = false;
                //for (int i = 0; i < listUser.Count; i++)
                //{
                //    using (SqlConnection connection = new SqlConnection(_connectionString))
                //    {
                //        using (var multi = connection.QueryMultiple(sql: DataConstants.DHRUVTLAPP_GetUserFireBaseToken,
                //                                          param: new { EmailId = "ritam@isgesolutions.com" },
                //                                          commandType: CommandType.StoredProcedure))
                //        {
                //            List<UserMaster> model = multi.Read<UserMaster>().ToList();

                //            if (model[i].FCMToken!= null)
                //                result = await PushNotification(model[i].FCMToken.ToString(), message, "Dhruv", APISecretKey, APISenderId);
                //            else
                //                result = false;
                //        }
                //    }
                //}
                return result;
            }
            catch(Exception e)
            {
                Debug.WriteLine("Error "+e.Message);
                return false;
            }
            finally
            {
            }
        }

        
    }
}
