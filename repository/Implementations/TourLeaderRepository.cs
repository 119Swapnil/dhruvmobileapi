﻿using Dapper;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.TourLeader;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class TourLeaderRepository : ITourLeaderRepository
    {
        private string _connectionString;
        private string _tlconnString;
        

        public TourLeaderRepository(string connectionString, string tlconnString)
        {
            _connectionString = connectionString;
            _tlconnString = tlconnString;
        }

        public async Task<TourLeader> AuthenticateTL(TourLeader tourLeader)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_tlconnString))
                {
                    var result = await connection.QuerySingleAsync<TourLeader>(sql: DataConstants.DHRUVTLAPP_Login,
                                                param: new { TLusercode = tourLeader.TLusercode , Password = tourLeader.Password },
                                               commandType: CommandType.StoredProcedure);

                    //if (result != null)
                    //{
                    //    return new BaseModel()
                    //    {
                    //        Result = true,
                    //        Message = "Tour leader login sucessfully",
                    //        classobject = result
                    //    };
                    //}
                    //else
                    //{
                    //    return new BaseModel()
                    //    {
                    //        Result = false,
                    //        Message = "Tour leader list not found for proivded id " + tourLeader.TLusercode,
                    //        classobject = result
                    //    };

                    //}
                    return result;
                }
            }
            catch (Exception e)
            {
                //return new BaseModel()
                //{
                //    Result = false,
                //    Message = "Failed to retrive tourleader data"
                //};
                return null;
            }
        }

        public async Task<BaseModel> GetTourLeaderListAsync(int  itineraryno)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<TourLeader>(sql: DataConstants.DHRUV_APP_TourLeaderList,
                                                param: new { ItineraryNo = itineraryno },
                                               commandType: CommandType.StoredProcedure);

                    if(result.AsList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Tour leader list retrived sucessfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Tour leader list not found for proivded itneraryno "+itineraryno,
                            listObject = result
                        };

                    }
                    
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive tourleader data"
                };
            }
        }
    }
}
