﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Dapper;
using System.Linq;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;
using System.Threading.Tasks;
using ISG.DhruvMobileAPI.Model.Common;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class ItineraryRepository : IItineraryRepository
    {
        private readonly string _connectionString;
        public ItineraryRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<BaseModel> GetBriefItineraryAsync(int ItineraryNo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<ItineraryBrief>(sql: DataConstants.DHRUV_APP_GetItineraryBrief,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                      commandType: CommandType.StoredProcedure);

                    if (result.AsList().Count != 0)
                    {

                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Retrive brief itinerary list successfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Brief itinerary not found for provided itineraryno " + ItineraryNo,
                            listObject = result
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive brief itinerary, Error: " + e.Message,
                    listObject = null
                };
            }
        }

        public async Task<BaseModel> GetDayWiseItinerary(int ItineraryNo, int DayNo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<Itinerary>(sql: DataConstants.DHRUV_APP_GetItineraryDayWiseComponent,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                      commandType: CommandType.StoredProcedure);

                    if (result.AsList().Count != 0)
                    {

                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Retrive daywise itinerary list successfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Daywise itinerary not found for provided itineraryno " + ItineraryNo,
                            listObject = result
                        };
                    }
                }
            }catch(Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive daywise list, Error: " + e.Message,
                    listObject = null
                };
            }
        }

        public async Task<BaseModel> GetCompleteItineraryAsync(int ItineraryNo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var briefList = await connection.QueryAsync<ItineraryBrief>(sql: DataConstants.DHRUV_APP_GetItineraryBrief,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                      commandType: CommandType.StoredProcedure);

                    var breifitems = briefList.Distinct(new DistinctItemComparer());

                    var daywiseList = await connection.QueryAsync<Itinerary>(sql: DataConstants.DHRUV_APP_GetItineraryDayWiseComponent,
                                              param: new { ItineraryNo = ItineraryNo },
                                              commandType: CommandType.StoredProcedure);

                    foreach(var briefitem in breifitems)
                    {
                        briefitem.DayWiseItinerary = daywiseList.Where(i => i.ItinDate == briefitem.FromDate).ToList();
                    }

                    if (breifitems.ToList().Count != 0)
                    {

                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Retrive complete itinerary list successfully",
                            listObject = breifitems
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Complete itinerary not found for provided itineraryno " + ItineraryNo,
                            listObject = briefList
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive complete itinerary list, Error: " + e.Message,
                    listObject = null
                };
            }
        }

      
    }
}
