﻿using System;
using System.Collections.Generic;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System.Data.SqlClient;
using ISG.DhruvMobileAPI.Repository.Common;
using Dapper;
using System.Data;
using System.Linq;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class PointerRepository : IPointerRepository
    {
        private readonly string _connectionString;

        public PointerRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string GetPointerValueBy(string pointerCode, string companyCode, string branchCode, string defaultBranchCode)
        {
            string value = string.Empty;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var multi = connection.QueryMultiple(sql: DataConstants.DHRUVTLAPP_GetPointerValue,
                                        param: new { PointerCode = pointerCode, CompanyCode = companyCode, BranchCode = branchCode },
                                                  commandType: CommandType.StoredProcedure))
                {
                    value = multi.Read<string>().FirstOrDefault();
                }

                if (String.IsNullOrEmpty(value))
                {
                    using (var multi = connection.QueryMultiple(sql: DataConstants.DHRUVTLAPP_GetPointerValue,
                                                            param: new { PointerCode = pointerCode, CompanyCode = companyCode, BranchCode = defaultBranchCode },
                                                                      commandType: CommandType.StoredProcedure))
                    {
                        value = multi.Read<string>().FirstOrDefault();
                    }
                }
            }
            return value;
        }
    }
}
