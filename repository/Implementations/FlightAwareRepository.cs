﻿using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class FlightAwareRepository : IFlightAwareRepository
    {
        private HttpClient httpClient;

        public FlightAwareRepository()
        {
            httpClient = new HttpClient();
        }

        public async Task<object> FlightAwareVer3(string flightno)
        {
            try
            {
                var username = "isgmobapp";
                var apiKey = "fa93dae4f3c6f60d8923cd55c05dc53ef0777963";
                var credentials = Encoding.ASCII.GetBytes(username + ":" + apiKey);
               // var serializer = new DataContractJsonSerializer(typeof(FlightInfo));
                var client = new HttpClient();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                var streamTask = await client.GetStringAsync("http://flightxml.flightaware.com/json/FlightXML3/FlightInfoStatus?ident=" + flightno.Replace(" ", "") + "&howMany=3");

                Debug.WriteLine("Flight Aware Respnse " + streamTask);


                var flightinfo = JsonConvert.DeserializeObject<object>(streamTask);

               // Debug.WriteLine("Flight Aware Respnse " + flightinfo.FlightInfoStatusResult.flights.Count);
                return flightinfo;

            }
            catch (Exception e)
            {
                Debug.WriteLine("FlightAware Error " + e.Message);
                return null;
            }
        }
    }
}
