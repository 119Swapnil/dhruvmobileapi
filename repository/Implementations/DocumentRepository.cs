﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ISG.Common;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Document;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;
using Microsoft.AspNetCore.Http;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class DocumentRepository : IDocumentRepository
    {
        private string _connectionString;
        private int rowsAffectedCount;
        private int result;
        private List<Document> model;

        public string ConnString { get; }

        public DocumentRepository(string connectionString, string connString)
        {
            _connectionString = connectionString;
            ConnString = connString;
        }

        public async Task<bool> DeleteDocumentAsync(int seqNo)
        {
            try
            {
                bool result;

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    await connection.OpenAsync();
                    string query = "Delete from SECorrespondence where SeqNo = @SeqNo";
                    var id = await connection.ExecuteAsync(query, new { seqNo });
                    if (id > 0)
                        result = true;
                    else
                    {
                        result = false;
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Document>> GetDocumentAsync(int ItineraryNo)
        {
            try
            {
                List<Document> model;
                using (SqlConnection connection = new SqlConnection(ConnString))
                {
                    connection.Open();
                    using (var multi = await connection.QueryMultipleAsync(sql: DataConstants.DHRUV_APP_GetDocuments,
                                                      param: new { ItineraryNo = ItineraryNo },
                                                      commandType: CommandType.StoredProcedure))
                    {
                        model = multi.Read<Document>().ToList();

                        if (model != null)
                            return model;
                    }
                }
                return model;
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
            }
        }

        public async Task<BaseModel> SaveAccountDocumentAsync(Document accountDocument, string rootPath, IFormFile formFile)
        {
            string docPath = GetDocumentSavePath(accountDocument.BkgNo.ToString(), accountDocument.QuoteNo.ToString(), rootPath, accountDocument.DocumentType);

            string filePath = await SaveDocumentAsync(docPath, formFile);
            if (filePath.Contains("Error"))
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = filePath
                };
            }


            var path = filePath.Replace(rootPath, "");
            //var path = filePath.Split(rootPath + @"\");
            // var url = "https://www.dhruvbi.com/MyDhruvMobile/" + path[1] +"/"+formFile.FileName;
            accountDocument.DocPath = path.ReturnString();  //path[1].Replace("\\", "/");
            accountDocument.DocType = Path.GetExtension(formFile.FileName);
            accountDocument.CrUId = "MYDHRUVAPP";
            accountDocument.DocDesc = "document uploaded from mpbile app";
            accountDocument.PublishToMyDhruvInd = false;
            accountDocument.DhruvAppInd = true;
            //if (publishversion > 0)
            //    accountDocument.PublishVersion = publishversion;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //Once db final uncomment section 
                    bool isExists = await connection.ExecuteScalarAsync<bool>(
                             sql: @"select count(1) from SECorrespondence where BkgNo = @BkgNo 
                                    And QuoteNo = @QuoteNo
                                    And Accountcode = @Accountcode
                                                                        And FileName = @FileName
                                    And DocumentType = @DocumentType",
                             commandType: CommandType.Text,
                             param: new
                             {
                                 accountDocument.QuoteNo,
                                 accountDocument.BkgNo,
                                 accountDocument.Accountcode,
                                 //publishversion,
                                 accountDocument.FileName,
                                 accountDocument.DocumentType

                             }, transaction: transaction
                             );

                    if (isExists)
                    {
                        result = await connection.ExecuteAsync(
                                                    sql: @"UPDATE [dbo].[SECorrespondence]
                                    SET DocDesc = @DocDesc,
                                    DocPath = @DocPath,
                                    UpdUId = @CrUId,
                                    UpdDate = @CrDate,
                                    UpdTime = @CrTime,
                                    DocType = @DocType,
                                    FileName = @FileName
                                    WHERE Accountcode = @Accountcode
                                    And QuoteNo = @QuoteNo
                                    And FileName = @FileName
                                    And BkgNo = @BkgNo",
                                                    commandType: CommandType.Text,
                                                    param: new
                                                    {
                                                        // accountDocument.SeqNo,
                                                        accountDocument.DocDesc,
                                                        accountDocument.DocPath,
                                                        accountDocument.CrUId,
                                                        accountDocument.CrDate,
                                                        accountDocument.CrTime,
                                                        accountDocument.DocType,
                                                        accountDocument.FileName,
                                                        accountDocument.Accountcode,
                                                        accountDocument.BkgNo,
                                                        accountDocument.QuoteNo,

                                                        //accountDocument.UploadedByConsultant
                                                    }, transaction: transaction
                                                   );
                    }
                    else
                    {
                        result = await connection.ExecuteAsync(
                                                                   @"INSERT INTO[dbo].[SECorrespondence] ([DocDesc], [DocPath], [CrUId]
                                                , [CrDate]
                                                , [CrTime]
                                                , [ItineraryNo]  
                                                , [DocType]
                                                , [QuoteNo]
                                                , [BkgNo]
                                                , [Accountcode]
                                                , [FileName]    
                                                , [DocumentType]
                                                , [DhruvAppInd]
                                                , [PublishToMyDhruvInd]
                                                )
                    VALUES (@DocDesc, @DocPath, @CrUId, @CrDate,@CrTime, @ItineraryNo , @DocType, @QuoteNo, @BkgNo, @Accountcode,
                                @FileName, @DocumentType,@DhruvAppInd,@PublishToMyDhruvInd) 
                    SELECT CAST(SCOPE_IDENTITY() AS INT)",
                                                                    param: new
                                                                    {

                                                                        accountDocument.DocDesc,
                                                                        accountDocument.DocPath,
                                                                        accountDocument.CrUId,
                                                                        accountDocument.CrDate,
                                                                        accountDocument.CrTime,
                                                                        accountDocument.ItineraryNo,
                                                                        accountDocument.DocType,
                                                                        accountDocument.QuoteNo,
                                                                        accountDocument.BkgNo,
                                                                        accountDocument.Accountcode,
                                                                        accountDocument.FileName,
                                                                        accountDocument.DocumentType,
                                                                        accountDocument.DhruvAppInd,
                                                                        accountDocument.PublishToMyDhruvInd
                                                                    }, transaction);
                        //return result;
                    }
                    transaction.Commit();
                    connection.Close();
                    // Commit the transaction to the database.
                    if (result > 0)
                    {
                        model = await GetDocumentAsync(accountDocument.ItineraryNo);
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "document uploaded successfully row " + result + " Model size " + model.Count + " BkgNo " + accountDocument.BkgNo +
                            " QuoteNo " + accountDocument.QuoteNo + " ItineraryNo " + accountDocument.ItineraryNo,
                            listObject = model
                        };

                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "document uploaded successfully",
                            listObject = model
                        };
                    }



                }
                catch (Exception ex)
                {
                    //transaction.Rollback();
                    return new BaseModel()
                    {
                        Result = false,
                        Message = "Error " + ex.Message
                    };
                }
            }
        }

        private async Task<string> SaveDocumentAsync(string docPath, IFormFile formFile)
        {//
            try
            {
                //var FileName = Path.GetFileName(formFile.FileName);
                var _filepath = Path.Combine(docPath, DateTime.Now.Ticks.ToString() + Path.GetExtension(formFile.FileName));
                using (var stream = new FileStream(_filepath, FileMode.Create))
                {
                    await formFile.CopyToAsync(stream);
                }

                return _filepath;
            }
            catch (Exception e)
            {
                return "Error " + e.Message;
            }


        }

        private string GetDocumentSavePath(string BookingNo, string QuoteNo, string rootPath, string DocumentType)
        {
            var folder = string.Empty;
            if (!string.IsNullOrEmpty(BookingNo) && BookingNo.ReturnString().ToInt() > 0)
            {
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, "Booking")))
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, "Booking"));
                rootPath = System.IO.Path.Combine(rootPath, "Booking");
                //
                foreach (var ch in BookingNo.PadLeft(6, '0'))
                {
                    folder = System.IO.Path.Combine(folder, ch.ToString());
                    if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, folder)))
                        System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, folder));
                }
                folder = System.IO.Path.Combine(folder, BookingNo.ToString());
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, folder)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, folder));

                folder = System.IO.Path.Combine(folder, DocumentType.ReturnString().Replace(" ", ""));
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, folder)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, folder));
            }
            else if (!string.IsNullOrEmpty(QuoteNo) && QuoteNo.ReturnString().ToInt() > 0)
            {
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, "Quote")))
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, "Quote"));
                rootPath = System.IO.Path.Combine(rootPath, "Quote");
                //
                foreach (var ch in QuoteNo.PadLeft(6, '0'))
                {
                    folder = System.IO.Path.Combine(folder, ch.ToString());
                    if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, folder)))
                        System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, folder));
                }
                folder = System.IO.Path.Combine(folder, QuoteNo.ToString());
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, folder)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, folder));

                folder = System.IO.Path.Combine(folder, DocumentType.ReturnString().Replace(" ", ""));
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(rootPath, folder)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(rootPath, folder));
            }

            folder = System.IO.Path.Combine(rootPath, folder);
            return folder;

        }

        public async Task<BaseModel> GetCountryDetail(int itineraryNo)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnString))
                {
                    using (var multi = await connection.QueryMultipleAsync(sql: DataConstants.DHRUV_APP_GetCountryDetails,
                        param: new { ItineraryNo = itineraryNo },
                        commandType: CommandType.StoredProcedure))
                    {
                        List<Country> model = multi.Read<Country>().ToList();


                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Success",
                            listObject = model
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = e.Message
                };
            }
        }
    }
}
