﻿using Dapper;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;
using ISG.DhruvMobileAPI.Model.Entities.Flight;
using ISG.DhruvMobileAPI.Model.Entities.Itinerary;

using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class FlightRepository : IFlightRepository
    {
        private string _connectionString;

        public FlightRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public async Task<BaseModel> GetFlightListAsync(int itineraryno)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    IEnumerable<Flight> result = await connection.QueryAsync<Flight>(sql: DataConstants.DHRUV_APP_GetFlightList,
                                                                          param: new { ItineraryNo = itineraryno },
                                                                          commandType: CommandType.StoredProcedure);
                    if (result.AsList().Count != 0)
                    {

                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Retrive flight list successfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Flight not found for provided itineraryno " + itineraryno,
                            listObject = result
                        };
                    }


                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive flight list, Error: " + e.Message,
                    listObject = null
                };

            }
        }

        public async Task<BaseModel> GetFlightPassenger(int itineraryno)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<FlightPassenger>(sql: DataConstants.DHRUV_APP_GetFlightPassengers,
                                                      param: new { ItineraryNo = itineraryno },
                                                      commandType: CommandType.StoredProcedure);
                    if (result.AsList().Count != 0)
                    {

                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Retrive flight passenger list successfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Flight passenger not found for provided itineraryno " + itineraryno,
                            listObject = result
                        };
                    }


                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive flight passenger list, Error: " + e.Message,
                    listObject = null
                };
            }
        }



    }
}
