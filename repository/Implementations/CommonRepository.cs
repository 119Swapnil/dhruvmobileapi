﻿using System;
using System.Collections.Generic;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using System.Data.SqlClient;
using ISG.DhruvMobileAPI.Repository.Common;
using Dapper;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
  public class CommonRepository : ICommonRepository
  {
    private readonly string _connectionString;
    private string _presetsConnectionstring;

    public CommonRepository(string connection, String presetsConnectionstring)
    {
      _connectionString = connection;
      _presetsConnectionstring = presetsConnectionstring;
    }

    public int GetLatestPublishVersion(int itineraryNo)
    {
      DynamicParameters dp = new DynamicParameters();
      dp.Add("itineraryNo", itineraryNo);
      using (SqlConnection connection = new SqlConnection(_connectionString))
      {
        connection.Open();
        try
        {
          var version = connection.QuerySingle<int>(sql: @"Select max(PublishVersion) From BkgItineraryHeader
                                                                                Where ItineraryNo = @ItineraryNo",
          param: dp);
          return version;
        }
        catch (Exception e)
        {
          return 0;
        }
      }
    }

    public List<string> GetCompanyBranchCode(int itineraryNo, int publishVersion)
    {
      List<string> comapnyBranch = new List<string>();
      DynamicParameters dp = new DynamicParameters();
      dp.Add("ItineraryNo", itineraryNo);
      dp.Add("PublishVersion", publishVersion);
      using (SqlConnection connection = new SqlConnection(_connectionString))
      {
        connection.Open();

        try
        {
          var test = connection.QuerySingle(sql: @"SELECT BranchCode,CompanyCode FROM bkgitineraryheader
                                                                                Where ItineraryNo = @ItineraryNo and PublishVersion= @PublishVersion",
          param: dp);

          comapnyBranch.Add(test.CompanyCode);
          comapnyBranch.Add(test.BranchCode);

          return comapnyBranch;
        }
        catch (Exception e)
        {
          return null;
        }
      }
    }
    public string GetDhruvAppVirtualCabinetUrl(int itineraryNo)
    {
      List<string> companyBranchCode = new List<string>();
      DynamicParameters dp = new DynamicParameters();
      string branchCode = string.Empty, companyCode = string.Empty;
      int publishVersion = 0;

      publishVersion = GetLatestPublishVersion(itineraryNo);
      companyBranchCode = GetCompanyBranchCode(itineraryNo, publishVersion);

      if (companyBranchCode != null && companyBranchCode.Count > 0)
      {
        branchCode = companyBranchCode[1] != null ? companyBranchCode[1] : "";
        companyCode = companyBranchCode[0] != null ? companyBranchCode[0] : "";
      }

      dp.Add("CompanyCode", companyCode);
      dp.Add("BranchCode", branchCode);

      using (SqlConnection connection = new SqlConnection(_presetsConnectionstring))
      {
        connection.Open();
        try
        {
          var dhruvAppVirtualCabinetUrl = connection.QuerySingle<string>(sql: @"Select DhruvAppVirtualCabinetUrl From Branch
                                                           Where CompanyCode=@CompanyCode and BranchCode=@BranchCode",
          param: dp);

          return dhruvAppVirtualCabinetUrl;
        }
        catch (Exception e)
        {
          return e.Message;
        }
      }
    }

  }
}
