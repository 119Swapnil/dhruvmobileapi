﻿using Dapper;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Stay;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class StayRepository : IStayRepository
    {
        private string _connectionString;

        public StayRepository(string connectionString)
        {
            _connectionString = connectionString;    
        }

        public async Task<BaseModel> GetStayListAsync(int itineraryno)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<Stay>(sql: DataConstants.DHRUV_APP_GetStayList,
                                                param: new { ItineraryNo = itineraryno },
                                               commandType: CommandType.StoredProcedure);

                    if(result.AsList().Count !=0)
                    {
                        foreach(var item in result)
                        {
                           var gallerylist = await GetStayGalleryImagesAsync(item.CpntCode, itineraryno.ToString());
                            if (gallerylist.Result)
                            {
                                item.Stay_Gallery = gallerylist.listObject;
                            }

                           var facilitylist = await GetStayFacilityAsync(item.CpntCode);
                            if (facilitylist.Result)
                            {
                                item.Stay_Facility = facilitylist.listObject;
                            }
                        }

                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Stay list retrived sucessfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Stay list not found for provided itineraryno "+itineraryno,
                            listObject = result
                        };
                    }    
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive daywise list, Error: " + e.Message,
                    listObject = null
                };
            }

        }

        public async Task<BaseModel> GetStayGalleryImagesAsync(string hotelcode, string Itineraryno)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<StayGallery>(sql: DataConstants.DHRUV_APP_GetStayGalleryImages,
                                                param: new { hotelCode = hotelcode, ItineraryNo = Itineraryno },
                                               commandType: CommandType.StoredProcedure);

                    if (result.AsList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Stay gallery images retrived sucessfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Stay gallery images list not found for provided hotel code " + hotelcode,
                            listObject = result
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive stay gallery images, Error: " + e.Message,
                    listObject = null
                };
            }
        }

        public async Task<BaseModel> GetStayFacilityAsync(string hotelcode)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var result = await connection.QueryAsync<StayFacility>(sql: DataConstants.DHRUV_APP_GetStayFacility,
                                                param: new { HotelCode = hotelcode },
                                               commandType: CommandType.StoredProcedure);

                    if (result.AsList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Stay facility retrived sucessfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Stay facility not found for provided hotel code " + hotelcode,
                            listObject = result
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to stay facility, Error: " + e.Message,
                    listObject = null
                };
            }
        }



    }
}
