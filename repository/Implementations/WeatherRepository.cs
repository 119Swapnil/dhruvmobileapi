﻿using ISG.DhruvMobileAPI.Model.Entities.Weather;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class WeatherRepository : IWeatherRepository
    {
        private HttpClient httpClient;
        private string _baseurl;
        private string _apikey;

        public WeatherRepository(string basrurl, string apikey)
        {
            httpClient = new HttpClient();
            _baseurl = basrurl;
            _apikey = apikey;
        }


        

        public async Task<object> GetWeatherByGeoPosAsync(double lat, double lon, DateTime date)
        {
            var url = string.Format(_baseurl + "/data/2.5/forecast?lat={0}&lon={1}&mode=json&apikey={2}", lat, lon, _apikey);
            try
            {
                using (var response = await httpClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var deser = JsonConvert.DeserializeObject<object>(apiResponse);
                    
                    return deser;
                    }else return string.Empty;
                    
                }
                    

            }catch(Exception e)
            {
                return string.Empty;
            }
        }

        public async Task<object> AccuWeather5DaysAsync(string cityid)
        {
            //var locRes = await  GetLocationKeyAsync(lat, lng);

            var url = string.Format("http://dataservice.accuweather.com/forecasts/v1/daily/5day/{0}?apikey={1}&details=true&metric=true", cityid, "eErWSh2eivAuJQk7DVmtekAi4AwKeK0O");
            try
            {
                using (var response = await httpClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var deser = JsonConvert.DeserializeObject<object>(apiResponse);

                        return deser;
                    }
                    else return string.Empty;

                }


            }
            catch (Exception e)
            {
                return string.Empty;
            }

        }

        public async Task<List<string>> GetLocationKeyAsync(List<Position> listcities)
        {
            List<string> cityidlist = new List<string>();

            foreach (var item in listcities)
            {
                //http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=eErWSh2eivAuJQk7DVmtekAi4AwKeK0O&q=18.530823,%2073.847466
                //swsw591@gmail.com
                var url = string.Format("http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey={0}&q={1},{2}", "eErWSh2eivAuJQk7DVmtekAi4AwKeK0O", item.Latitude, item.Longitude);
                //wakchaure.swapnil.1992@gmail.com
                //var url = string.Format("http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey={0}&q={1},{2}", "IoHxCiGBBatJZXWuH1qDQ7CsAhvf2jNt", lat, lon);
                try
                {
                    using (var response = await httpClient.GetAsync(url))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string apiResponse = await response.Content.ReadAsStringAsync();
                            var deser = JsonConvert.DeserializeObject<AccuLocation>(apiResponse);

                            cityidlist.Add(deser.Key);
                        }
                        
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return cityidlist;
        }

        public async Task<object> AccuWeather12HoursAsync(string cityid)
        {
            //var locRes = await GetLocationKeyAsync(lat, lng);

            var url = string.Format("http://dataservice.accuweather.com/forecasts/v1/daily/5day/{0}?apikey={1}&details=true&metric=true", cityid, "eErWSh2eivAuJQk7DVmtekAi4AwKeK0O");
            try
            {
                using (var response = await httpClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var deser = JsonConvert.DeserializeObject<object>(apiResponse);

                        return deser;
                    }
                    else return string.Empty;
                }
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        
    }
}
