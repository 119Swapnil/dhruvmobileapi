﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.ClientEnt;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class ClientRepository : IClientRepository
    {
        private readonly string _connectionString;
        private readonly string _tlconnString;
        private readonly string salesstring;

        public ClientRepository(string connectionString, string tlconnString, string salesstring)
        {
            _connectionString = connectionString;
            _tlconnString = tlconnString;
            this.salesstring = salesstring;
        }

        public async Task<Client> AuthenticateUserAsync(string EmailId, string Password)
        {
            try
            {
                    bool exists;
                    using (SqlConnection connection = new SqlConnection(salesstring))
                    {
                        exists = connection.ExecuteScalar<bool>("select count(1) from MyAppUsers where EMailId=@emailid", new { emailid = EmailId });
                    }


                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        //var exists = connection.ExecuteScalar<bool>("select count(1) from MyAppUsers where EMailId=@emailid", new { emailid = EmailId });
                    
                    if (true)
                    {
                        IEnumerable<Client> result = await connection.QueryAsync<Client>(sql: DataConstants.DHRUV_APP_ValidateUserLogin,
                                                    param: new { EmailId = EmailId, Password = Password },
                                                   commandType: CommandType.StoredProcedure);
                        if (result.Count() > 0)
                        {

                            Client model = result.FirstOrDefault<Client>();
                            model.Result = true;
                            model.Message = "Login Sucessfully";
                            return model;
                        }
                        else
                        {
                            return new Client()
                            {
                                Result = false,
                                Message = "Invalid password for provided emailid - " + EmailId
                            };
                        }
                    }
                    else
                    {
                        return new Client()
                        {
                            Result = false,
                            Message = "Invalid emailId. Please enter valid emailid."
                        };
                    }
                }
            }
            catch (Exception e)
            {
                return new Client()
                {
                    Result = false,
                    Message = "Error " + e.Message
                };
            }
        }

        public async Task<BaseModel> GetClientsWithDetails(string tourcode, DateTime fromdate)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_tlconnString))
                {
                    var result = await connection.QueryAsync<Client>(sql: DataConstants.DHRUVTLAPP_ClientList,
                                                                    param: new { Brochure_Code_Short = tourcode, DepDate = fromdate},
                                                                   commandType: CommandType.StoredProcedure);

                    foreach(var item in result)
                    {
                        await GetClientsWithDetailsAsync(item);
                        

                    }

                    return new BaseModel()
                    {
                        Result = true,
                        Message = "pax count retrived sucessfully",
                        listObject = result
                    };

                }

            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive pax count, Error :" + e.Message
                };
            }
        }

        private async Task<object> GetClientsWithDetailsAsync(Client client)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_tlconnString))
                {
                    var result = await connection.QueryMultipleAsync(sql: DataConstants.DHRUVTLAPP_ClientDetails,
                                                                                        param: new { Accountcode = "0100128945" },
                                                                                       commandType: CommandType.StoredProcedure);

                    var passport_info =  result.Read<Passport>().FirstOrDefault();
                    var nok_info = result.Read<NextOfKin>().FirstOrDefault();
                    var nok_address = result.Read<Address>().FirstOrDefault();
                    var nok_contactno = result.Read<ContactNo>().ToList();

                    client.Passport_info = passport_info;
                    nok_info.NOFK_Address = nok_address;
                    nok_info.NOFK_ContactNo = nok_contactno;
                    client.NOFK_info = nok_info;
                    
                    return client;
                }
            }catch (Exception e)
            {
                return null;
            }
        }

        public async Task<BaseModel> GetPaxCountAsync(string accountcode, DateTime fromdate)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_tlconnString))
                {
                    PaxCount result = await connection.QuerySingleAsync<PaxCount>(sql: DataConstants.DHRUVTLAPP_ClientPaxCount,
                                                                    param: new { Brochure_Code_Short = accountcode, DepDate = fromdate},
                                                                   commandType: CommandType.StoredProcedure);

                    return new BaseModel()
                    {
                        Result = true,
                        Message = "pax count retrived sucessfully",
                        classobject = result
                    };

                }

            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive pax count, Error :" + e.Message
                };
            }
        }
    }
}

