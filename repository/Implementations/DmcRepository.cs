﻿using Dapper;
using ISG.DhruvMobileAPI.Model.Common;
using ISG.DhruvMobileAPI.Model.Entities.Dmc;
using ISG.DhruvMobileAPI.Model.Entities.TourLeader;
using ISG.DhruvMobileAPI.Model.RepositoryInterfaces;
using ISG.DhruvMobileAPI.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace ISG.DhruvMobileAPI.Repository.Implementations
{
    public class DmcRepository : IDmcRepository
    {
        public string _connectionString;
        private readonly string _tlconnString;

        public DmcRepository(string connectionString, string tlconnString)
        {
            _connectionString = connectionString;
            _tlconnString = tlconnString;
        }

        
        public async Task<BaseModel> GetDmcListWithDetailsAsync(string tourcode)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_tlconnString))
                {
                    var result = await connection.QueryAsync<Dmc>(sql: DataConstants.DHRUVTLAPP_DmcList,
                                                param: new { Brochure_Code_Short = tourcode },
                                               commandType: CommandType.StoredProcedure);

                    
                    foreach(var item in result)
                    {
                        item.Contactdetails = await GetDmcContactsdetailAsync(item.AccountCode);
                    }
                    
                    
                    if (result.AsList().Count != 0)
                    {
                        return new BaseModel()
                        {
                            Result = true,
                            Message = "Tour dmc retrive sucessfully",
                            listObject = result
                        };
                    }
                    else
                    {
                        return new BaseModel()
                        {
                            Result = false,
                            Message = "Tour dmc not found for proivded id " + tourcode,
                            listObject = result
                        };

                    }

                }
            }
            catch (Exception e)
            {
                return new BaseModel()
                {
                    Result = false,
                    Message = "Failed to retrive tourleader data"
                };
            }
        }

        private async Task<List<DmcContact>> GetDmcContactsdetailAsync(string accountCode)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_tlconnString))
                {
                    var result = await connection.QueryAsync<DmcContact>(sql: DataConstants.DHRUVTLAPP_DmcList,
                                                param: new { AccountCode = accountCode },
                                               commandType: CommandType.StoredProcedure);

                    return result.AsList();

                }

                }catch (Exception e)
            {
                return null;
            }
        }
    }
}
