﻿using System;
using System.Diagnostics;

namespace ISG.Common
{
	public class Timing
	{
		TimeSpan startingTime;
		TimeSpan duration;

		public Timing()
		{
			startingTime = new TimeSpan(0);
			duration = new TimeSpan(0);
		}

		public void StopTime()
		{
			duration = Process.GetCurrentProcess().Threads[0].UserProcessorTime.Subtract(startingTime);
		}

		public void StartTime()
		{
			GC.Collect();
			GC.WaitForPendingFinalizers();
			startingTime = Process.GetCurrentProcess().Threads[0].
			UserProcessorTime;
		}

		public TimeSpan Result()
		{
			return duration;
		}
	}

	/// example usage 
	/// 
	///	class test
	///	{
	///
	///		static void Main()
	///		{
	///			int[] nums = new int[100000];
	///			BuildArray(nums);
	///			 Timing tObj = new Timing();
	///			tObj.startTime();
	///			DisplayNums(nums);
	///			tObj.stopTime();
	///			 Console.WriteLine("time (.NET): " & tObj.Result.TotalSeconds);
	///		 }
	///
	///		static void BuildArray(int[] arr)
	///		 {
	///		  for (int i = 0; i < 100000; i++)
	///		    arr[i] = I;
	///		 }
	///	}
}
