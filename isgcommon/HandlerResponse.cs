﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISG.Common
{
 public class HandlerResponse
  {
    public bool  Success { get; set; }
    public string Result { get; set; }
    public string ErrorMessage { get; set; }
  }
}
