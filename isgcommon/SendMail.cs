﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISG.Common.Config;
using System.Net.Mail;
namespace ISG.Common
{
	public class SendMail
	{
		private string _sendMailBy = "";
		private string _toMail = "";
		private string _BCC = "";
		private string _cCId = "";
		private string _sendMailSubject = "";
		private bool _isBodyHtml = false;

		public bool IsBodyHtml
		{
			get
			{
				return _isBodyHtml;
			}
			set
			{
				_isBodyHtml = (bool)value;
			}
		}
		public string SendMailBy
		{
			get
			{
				return _sendMailBy;
			}

			set
			{
				_sendMailBy = value;
			}
		}
		public string ToMail
		{
			get
			{
				return _toMail;
			}

			set
			{
				_toMail = value;
			}
		}
		public string BCC
		{
			get
			{
				return _BCC;
			}

			set
			{
				_BCC = value;
			}
		}
		public string CCId
		{
			get
			{
				if (_cCId == "")
					return _cCId = "";
				else
					return _cCId;
			}

			set
			{
				if (_cCId != value)
				{
					_cCId = value;
				}
			}
		}
		public string SendMailSubject
		{
			get
			{
				return _sendMailSubject;
			}

			set
			{
				_sendMailSubject = value;
			}
		}

		#region Method

    public void SendMailMessage(string strMsg)
    {
      ////' Mail initialization
      MailMessage mailMsg = new MailMessage();
      SmtpClient client = new SmtpClient();
      try
      {
        mailMsg.From = new MailAddress(_sendMailBy.ReturnString());

        if (_toMail.ReturnString() != String.Empty)
        {
          if (_toMail.ReturnString().Contains(','))
          {
            foreach (var emailAddress in _toMail.Split(','))
            {
              mailMsg.To.Add(new MailAddress(emailAddress.ReturnString()));
            }
          }
          else if (_toMail.ReturnString().Contains(';'))
          {
            foreach (var emailAddress in _toMail.Split(';'))
            {
              mailMsg.To.Add(new MailAddress(emailAddress.ReturnString()));
            }
          }
          else
          {
            mailMsg.To.Add(new MailAddress(_toMail.ReturnString()));
          }
        }

        if (_cCId.ToString() != String.Empty)
        {
          if (_cCId.ReturnString().Contains(','))
          {
            foreach (var emailAddress in _cCId.Split(','))
            {
              mailMsg.CC.Add(new MailAddress(emailAddress.ReturnString()));
            }
          }
          else if (_cCId.ReturnString().Contains(';'))
          {
            foreach (var emailAddress in _cCId.Split(';'))
            {
              mailMsg.CC.Add(new MailAddress(emailAddress.ReturnString()));
            }
          }
          else
          {
            mailMsg.CC.Add(new MailAddress(_cCId.ReturnString()));
          }
        }

        if (_BCC.ToString() != String.Empty)
        {
          if (_BCC.ReturnString().Contains(','))
          {
            foreach (var emailAddress in _BCC.Split(','))
            {
              mailMsg.Bcc.Add(new MailAddress(emailAddress.ReturnString()));
            }
          }
          else if (_cCId.ReturnString().Contains(';'))
          {
            foreach (var emailAddress in _BCC.Split(';'))
            {
              mailMsg.Bcc.Add(new MailAddress(emailAddress.ReturnString()));
            }
          }
          else
          {
            mailMsg.Bcc.Add(new MailAddress(_BCC.ReturnString()));
          }
        }
        mailMsg.Subject = _sendMailSubject;
        mailMsg.Body = strMsg;
        mailMsg.IsBodyHtml = _isBodyHtml;
        mailMsg.Priority = MailPriority.Normal;
        client.Send(mailMsg);
      }
      catch
      {
        //throw;
      }
      finally
      {
        mailMsg = null;
        client = null;
      }

    }

		public void SendMailMessage(string strMsg, Attachment attachment)
		{
			////' Mail initialization
			MailMessage mailMsg = new MailMessage();
			SmtpClient client = new SmtpClient();
			try
			{
				mailMsg.From = new MailAddress(_sendMailBy.ReturnString());
				foreach (var emailAddress in _toMail.Split(';'))
				{
					mailMsg.To.Add(new MailAddress(emailAddress.ReturnString()));
				}

				if (_cCId.ToString() != String.Empty)
				{
					foreach (var emailAddress in _cCId.Split(';'))
					{
						mailMsg.CC.Add(new MailAddress(emailAddress.ReturnString()));
					}
				}
				if (_BCC.ToString() != String.Empty)
				{
					foreach (var emailAddress in _BCC.Split(';'))
					{
						mailMsg.Bcc.Add(new MailAddress(emailAddress.ReturnString()));
					}
				}
				mailMsg.Attachments.Add(attachment);
				mailMsg.Subject = _sendMailSubject;
				mailMsg.Body = strMsg;
				mailMsg.IsBodyHtml = _isBodyHtml;
				mailMsg.Priority = MailPriority.Normal;
				client.Send(mailMsg);
			}
			catch
			{
				//throw;
			}
			finally
			{
				mailMsg = null;
				client = null;
			}

		}

		#endregion

	}
}
