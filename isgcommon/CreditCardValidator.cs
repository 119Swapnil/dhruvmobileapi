﻿using ISG.Common.DB;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Json;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace ISG.Common
{
  public enum CardResultType
  {
    InvalidCard = 0,
    UnknownCard = 1,
    NotMatched = 2,
    Matched = 3,
    ServiceUnavailable = 4
  }

  public enum CardTypes
  {
    DebitCard = 0,
    CreditCard = 1,
    UnknownType = 2,
    UnAssigned = 3
  }

  public class CreditCardValidator
  {
    private DatabaseAccess _dataAccessPresets;

    public CreditCardValidator()
    {
      _dataAccessPresets = new DatabaseAccess(ConfigurationManager.ConnectionStrings["Presets"].ReturnString());
    }

    public CreditCardValidator(string connectionString)
    {
      _dataAccessPresets = new DatabaseAccess(connectionString);
    }

    ~CreditCardValidator()
    {
      _dataAccessPresets = null;
    }

    private class CardTypeInfo
    {
      public CardTypeInfo(string regEx, int length, string chargeCode, string chargeDesc, string compareText)
      {
        RegEx = regEx;
        Length = length;
        ChargeCode = chargeCode;
        ChargeDesc = chargeDesc;
        CompareText = compareText;
      }

      public string RegEx { get; set; }
      public int Length { get; set; }
      public string ChargeCode { get; set; }
      public string ChargeDesc { get; set; }
      public string CompareText { get; set; }
    }

    public class CardTypeJson
    {
      public string bin { get; set; }
      public string brand { get; set; }
      public string sub_brand { get; set; }
      public string country_code { get; set; }
      public string country_name { get; set; }
      public string bank { get; set; }
      public string card_type { get; set; }
      public string card_category { get; set; }
      public string latitude { get; set; }
      public string longitude { get; set; }
    }

    private static bool IsCardNumberValid(string cardNumber)
    {
      int i, checkSum = 0;

      // Compute checksum of every other digit starting from right-most digit
      for (i = cardNumber.Length - 1; i >= 0; i -= 2)
        checkSum += (cardNumber[i] - '0');

      // Now take digits not included in first checksum, multiple by two,
      // and compute checksum of resulting digits
      for (i = cardNumber.Length - 2; i >= 0; i -= 2)
      {
        int val = ((cardNumber[i] - '0') * 2);
        while (val > 0)
        {
          checkSum += (val % 10);
          val /= 10;
        }
      }

      // Number is valid if sum of both checksums MOD 10 equals 0
      return ((checkSum % 10) == 0);
    }

    private static string NormalizeCardNumber(string cardNumber)
    {
      if (cardNumber == null)
        cardNumber = String.Empty;

      StringBuilder sb = new StringBuilder();

      foreach (char c in cardNumber)
      {
        if (Char.IsDigit(c))
          sb.Append(c);
      }

      return sb.ToString();
    }

    public string GetCardTypeChargeDesc(string cardNumber, string SelectedCardType, out CardResultType resultType)
    {
      List<CardTypeInfo> cardTypeInfo;
      string chargeDesc = string.Empty;
      if (IsCardNumberValid(cardNumber))
      {
        cardTypeInfo = new List<CardTypeInfo>();
        cardNumber = NormalizeCardNumber(cardNumber);
        cardTypeInfo = GetCardValidators(SelectedCardType);

        if (cardTypeInfo.Count > 0)
        {
          foreach (CardTypeInfo info in cardTypeInfo)
          {
            if (cardNumber.Length == info.Length && Regex.IsMatch(cardNumber, info.RegEx))
            {
              resultType = CardResultType.Matched;
              chargeDesc = info.ChargeDesc;
            }
          }
          resultType = CardResultType.NotMatched;
        }
        else
        {
          resultType = CardResultType.UnknownCard;
        }
      }
      else
      {
        resultType = CardResultType.InvalidCard;
      }
      return chargeDesc;
    }

    public string GetCardTypeChargeCode(string cardNumber, string SelectedCardType, out CardResultType resultType)
    {
      List<CardTypeInfo> cardTypeInfo;
      if (IsCardNumberValid(cardNumber))
      {
        cardTypeInfo = new List<CardTypeInfo>();
        cardNumber = NormalizeCardNumber(cardNumber);
        cardTypeInfo = GetCardValidators(SelectedCardType);

        if (cardTypeInfo.Count > 0)
        {
          foreach (CardTypeInfo info in cardTypeInfo)
          {
            if (cardNumber.Length == info.Length && Regex.IsMatch(cardNumber, info.RegEx))
            {
              resultType = CardResultType.Matched;
              return info.ChargeCode;
            }
          }
          resultType = CardResultType.NotMatched;
        }
        else
        {
          resultType = CardResultType.UnknownCard;
        }
      }
      else
      {
        resultType = CardResultType.InvalidCard;
      }
      return string.Empty;
    }

    public CardTypeJson GetCardJson(string cardNumber)
    {
      CardTypeJson CardTypeJson;
      string url = string.Format("http://www.binlist.net/{0}/{1}", "json", cardNumber);
      WebRequest request = WebRequest.Create(url);
      WebResponse response = request.GetResponse();
      using (var reader = new StreamReader(response.GetResponseStream()))
      {
        string result = reader.ReadToEnd();
        CardTypeJson = Deserialize<CardTypeJson>(result);
      }
      response.Close();

      return CardTypeJson;
    }

    public T Deserialize<T>(string result)
    {
      //cast to specified objectType
      var obj = (T)new JavaScriptSerializer().Deserialize<T>(result);

      //return the object
      return obj;
    }

    private struct ColunmNameCardValidator
    {
      public const string RegEx = "RegEx";
      public const string Length = "Length";
      public const string ChargeCode = "ChargeCode";
      public const string ChargeDesc = "ChargeDesc";
      public const string IsDebitCard = "IsDebitCard";
      public const string CompareText = "CompareText";
    }

    private List<CardTypeInfo> GetCardValidators(string SelectedCardType)
    {
      List<CardTypeInfo> cardTypeInfo = new List<CardTypeInfo>();
      DataTable dt = new DataTable();
      StringBuilder sb = new StringBuilder();
      string query = string.Empty;

      sb.Append("SELECT");
      sb.Append(" CCV.StartsWithRegex RegEx,");
      sb.Append(" CCV.CardNumberLenght Length,");
      sb.Append(" CH.Charge_Code ChargeCode,");
      sb.Append(" CH.Charge_Desc ChargeDesc, ");
      sb.Append(" CCV.CompareText CompareText ");
      sb.Append(" FROM presets.dbo.CreditCardValidators CCV ");
      sb.Append(" INNER JOIN presets.dbo.ChargeCCVLink CCL ");
      sb.Append("ON CCV.Validator_Code = CCL.Validator_Code ");
      sb.Append(" INNER JOIN sales97.dbo.Charge CH ");
      sb.Append("ON CCL.Charge_Code = CH.Charge_Code ");
      sb.Append("WHERE  CH.Charge_Code = '" + SelectedCardType.ReturnString() + "' AND IsNull(CCL.FrzInd,0) = 0 ");
      sb.Append(" ORDER BY CCV.CardPriority");

      query = sb.ReturnString();

      try
      {
        dt = _dataAccessPresets.ExecuteSelectQuery(query);

        if (dt != null && dt.Rows.Count > 0)
        {
          foreach (DataRow dr in dt.Rows)
          {
            string RegEx = dt.Columns.Contains(ColunmNameCardValidator.RegEx) ?
                                        dr[ColunmNameCardValidator.RegEx].ReturnString() : string.Empty;
            int Length = dt.Columns.Contains(ColunmNameCardValidator.Length) ?
                                        dr[ColunmNameCardValidator.Length].ReturnString().ToInt() : 0;
            string ChargeCode = dt.Columns.Contains(ColunmNameCardValidator.ChargeCode) ?
                                        dr[ColunmNameCardValidator.ChargeCode].ReturnString() : string.Empty;
            string ChargeDesc = dt.Columns.Contains(ColunmNameCardValidator.ChargeDesc) ?
                                        dr[ColunmNameCardValidator.ChargeDesc].ReturnString() : string.Empty;
            string CompareText = dt.Columns.Contains(ColunmNameCardValidator.CompareText) ?
                                        dr[ColunmNameCardValidator.CompareText].ReturnString() : string.Empty;

            if (!string.IsNullOrWhiteSpace(RegEx)
                  && Length > 0
                  && !string.IsNullOrWhiteSpace(ChargeCode)
                  && !string.IsNullOrWhiteSpace(ChargeDesc))
            {
              cardTypeInfo.Add(new CardTypeInfo(RegEx, Length, ChargeCode, ChargeDesc, CompareText));
            }
          }
        }

        return cardTypeInfo;
      }
      catch (Exception ex)
      {
        throw new Exception("ISG.Common.CreditCardValidator >> GetCardValidators >> " + ex.Message, ex);
      }
      finally
      {
        dt = null;
      }
    }

    public CardResultType ValidateCardType(string cardNumber, string SelectedCardType, out CardTypes actualCardType)
    {
      CardTypes chargeType = GetChargeType(SelectedCardType);
      CardTypes cardType = GetCardType(cardNumber);
      if (cardType == CardTypes.UnknownType)
      {
        actualCardType = CardTypes.UnknownType;
        return CardResultType.UnknownCard;
      }
      else
      {
        if (chargeType == cardType)
        {
          actualCardType = cardType;
          return CardResultType.Matched;
        }
        else
        {
          actualCardType = cardType;
          return CardResultType.NotMatched;
        }
      }
    }

    public CardTypes GetChargeType(string SelectedCardType)
    {
      DataTable dt = new DataTable();
      StringBuilder sb = new StringBuilder();
      string query = string.Empty;

      sb.Append("SELECT TOP 1");
      sb.Append(" CH.IsDebitCard IsDebitCard ");
      sb.Append(" FROM sales97.dbo.Charge CH ");
      sb.Append(" WHERE  CH.Charge_Code = '" + SelectedCardType.ReturnString() + "' AND IsNull(CH.FrzInd,0) = 0 ");

      query = sb.ReturnString();

      try
      {
        dt = _dataAccessPresets.ExecuteSelectQuery(query);

        if (dt != null && dt.Rows.Count > 0)
        {
          bool IsDebitCard = dt.Columns.Contains(ColunmNameCardValidator.IsDebitCard)
                              ? dt.Rows[0][ColunmNameCardValidator.IsDebitCard].ReturnString().ToBool() : false;

          if (IsDebitCard == true)
            return CardTypes.DebitCard;
          else
            return CardTypes.CreditCard;
        }
        else
          return CardTypes.CreditCard;
      }
      catch (Exception ex)
      {
        throw new Exception("ISG.Common.CreditCardValidator >> GetCardValidators >> " + ex.Message, ex);
      }
      finally
      {
        dt = null;
      }

    }

    public CardTypes GetCardType(string cardNumber)
    {
      CardTypeJson cardTypeJson = GetCardJson(cardNumber);
      if (cardTypeJson != null)
      {
        if (cardTypeJson.card_type.Equals("debit", StringComparison.OrdinalIgnoreCase)
            || cardTypeJson.card_type.Equals("deb", StringComparison.OrdinalIgnoreCase)
            || cardTypeJson.card_type.Equals("debitcard", StringComparison.OrdinalIgnoreCase))
        {
          return CardTypes.DebitCard;
        }
        else if (cardTypeJson.card_type.Equals("credit", StringComparison.OrdinalIgnoreCase)
            || cardTypeJson.card_type.Equals("cred", StringComparison.OrdinalIgnoreCase)
            || cardTypeJson.card_type.Equals("creditcard", StringComparison.OrdinalIgnoreCase))
        {
          return CardTypes.CreditCard;
        }
        else
        {
          return CardTypes.UnknownType;
        }
      }
      else
      {
        return CardTypes.UnknownType;
      }
    }



    #region Neutrinoapi Card Validation

    public class CardTypeJsonForNeutrinoapi
    {
      public bool valid { get; set; }
      public string issuer { get; set; }
      public string card_type { get; set; }
      public string issuer_website { get; set; }
      public string card_category { get; set; }
      public string issuer_phone { get; set; }
      public string country_code { get; set; }
      public string card_brand { get; set; }
      public string country { get; set; }
      public string api_error { get; set; }
      public string api_error_msg { get; set; }
    }

    public CardResultType ValidateCardUsingNeutrinoapi(string cardNumber, string SelectedCardType, out CardTypes actualCardType, out string errMsg)
    {
      actualCardType = CardTypes.UnAssigned;
      CardTypeJsonForNeutrinoapi cardTypeJsonForNeutrinoapi = GetCardJsonFromNeutrinoapi(cardNumber);

      // Validate if card object is empty or lacking data.
      if (cardTypeJsonForNeutrinoapi != null && !string.IsNullOrWhiteSpace(cardTypeJsonForNeutrinoapi.card_brand) && !string.IsNullOrWhiteSpace(cardTypeJsonForNeutrinoapi.card_type))
      {
        // Validate if card object has valid card number.
        if (cardTypeJsonForNeutrinoapi.valid == true)
        {
          // Validate if card object has valid brand.
          CardResultType cardBrandResultType = ValidateCardBrand(SelectedCardType, cardTypeJsonForNeutrinoapi);
          if (cardBrandResultType == CardResultType.Matched)
          {
            // Validate if card object has card type.
            CardResultType cardTypeResultType = ValidateCardType(SelectedCardType, cardTypeJsonForNeutrinoapi, out actualCardType);
            if (cardTypeResultType == CardResultType.Matched)
            {
              errMsg = "";
              return CardResultType.Matched;
            }
            else if (cardTypeResultType == CardResultType.NotMatched)
            {
              if (actualCardType == CardTypes.CreditCard)
              {
                errMsg = "The card number entered is a Credit card.";
                return CardResultType.NotMatched;
              }
              else
              {
                errMsg = "The card number entered is a Debit card.";
                return CardResultType.NotMatched;
              }
            }
            else if (cardTypeResultType == CardResultType.ServiceUnavailable)
            {
              errMsg = "3rd Party service cannot be contacted to retrieve card information. Error Details: " + cardTypeJsonForNeutrinoapi.api_error_msg;
              return CardResultType.ServiceUnavailable;
            }
            else
            {
              // Should not hit here.
              errMsg = "The card number entered is invalid.";
              return CardResultType.InvalidCard;
            }
          }
          else if (cardBrandResultType == CardResultType.NotMatched)
          {
            errMsg = "The card number entered does not match the card type selected.";
            return CardResultType.NotMatched;
          }
          else if (cardBrandResultType == CardResultType.UnknownCard)
          {
            errMsg = "No records / Freezed records found to validate the card.";
            return CardResultType.UnknownCard;
          }
          else
          {
            // Should not hit here.
            errMsg = "The card number entered is invalid.";
            return CardResultType.InvalidCard;
          }
        }
        else
        {
          errMsg = "The card number entered is invalid.";
          return CardResultType.InvalidCard;
        }
      }
      else
      {
        errMsg = "3rd Party service cannot be contacted to retrieve card information. Error Details: " + cardTypeJsonForNeutrinoapi.api_error_msg;
        return CardResultType.ServiceUnavailable;
      }
    }

    public CardResultType ValidateCardBrand(string SelectedCardType, CardTypeJsonForNeutrinoapi cardTypeJsonForNeutrinoapi)
    {
      List<CardTypeInfo> cardTypeInfo;
      cardTypeInfo = new List<CardTypeInfo>();
      cardTypeInfo = GetCardValidators(SelectedCardType);

      if (cardTypeInfo.Count > 0)
      {
        foreach (CardTypeInfo info in cardTypeInfo)
        {
          if (info.CompareText.IndexOf(Regex.Replace(cardTypeJsonForNeutrinoapi.card_brand, @"[^0-9a-zA-Z]+", ""), StringComparison.OrdinalIgnoreCase) >= 0)
          {
            return CardResultType.Matched;
          }
        }
        return CardResultType.NotMatched;
      }
      else
      {
        return CardResultType.UnknownCard;
      }
    }

    public CardResultType ValidateCardType(string SelectedCardType, CardTypeJsonForNeutrinoapi cardTypeJsonForNeutrinoapi, out CardTypes actualCardType)
    {
      CardTypes chargeType = GetChargeType(SelectedCardType);
      CardTypes cardType = GetCardType(cardTypeJsonForNeutrinoapi);
      if (cardType == CardTypes.UnknownType)
      {
        actualCardType = CardTypes.UnknownType;
        return CardResultType.ServiceUnavailable;
      }
      else
      {
        if (chargeType == cardType)
        {
          actualCardType = cardType;
          return CardResultType.Matched;
        }
        else
        {
          actualCardType = cardType;
          return CardResultType.NotMatched;
        }
      }
    }

    public CardTypes GetCardType(CardTypeJsonForNeutrinoapi cardTypeJsonForNeutrinoapi)
    {
      if (cardTypeJsonForNeutrinoapi != null && !string.IsNullOrWhiteSpace(cardTypeJsonForNeutrinoapi.card_type))
      {
        if (Regex.Replace(cardTypeJsonForNeutrinoapi.card_type, @"[^0-9a-zA-Z]+", "").Equals("debit", StringComparison.OrdinalIgnoreCase)
            || Regex.Replace(cardTypeJsonForNeutrinoapi.card_type, @"[^0-9a-zA-Z]+", "").Equals("deb", StringComparison.OrdinalIgnoreCase)
            || Regex.Replace(cardTypeJsonForNeutrinoapi.card_type, @"[^0-9a-zA-Z]+", "").Equals("debitcard", StringComparison.OrdinalIgnoreCase))
        {
          return CardTypes.DebitCard;
        }
        else if (Regex.Replace(cardTypeJsonForNeutrinoapi.card_type, @"[^0-9a-zA-Z]+", "").Equals("credit", StringComparison.OrdinalIgnoreCase)
            || Regex.Replace(cardTypeJsonForNeutrinoapi.card_type, @"[^0-9a-zA-Z]+", "").Equals("cred", StringComparison.OrdinalIgnoreCase)
            || Regex.Replace(cardTypeJsonForNeutrinoapi.card_type, @"[^0-9a-zA-Z]+", "").Equals("creditcard", StringComparison.OrdinalIgnoreCase))
        {
          return CardTypes.CreditCard;
        }
        else
        {
          return CardTypes.UnknownType;
        }
      }
      else
      {
        return CardTypes.UnknownType;
      }
    }

    public CardTypeJsonForNeutrinoapi GetCardJsonFromNeutrinoapi(string cardNumber)
    {
      if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["NeutrinoapiUserId"].ReturnString())
          && !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["NeutrinoapiApiKey"].ReturnString()))
      {
        cardNumber = NormalizeCardNumber(cardNumber);
        using (var client = new HttpClient())
        {
          var req = new List<KeyValuePair<string, string>>();
          req.Add(new KeyValuePair<string, string>("user-id", ConfigurationManager.AppSettings["NeutrinoapiUserId"].ReturnString()));
          req.Add(new KeyValuePair<string, string>("api-key", ConfigurationManager.AppSettings["NeutrinoapiApiKey"].ReturnString()));
          req.Add(new KeyValuePair<string, string>("bin-number", cardNumber));

          var content = new FormUrlEncodedContent(req);
          var response = client.PostAsync("https://neutrinoapi.com/bin-lookup", content);
          var responseStr = response.Result.Content.ReadAsStringAsync();

          System.Json.JsonObject result = (System.Json.JsonObject)JsonValue.Parse(responseStr.Result);
          CardTypeJsonForNeutrinoapi cardTypeJsonForNeutrinoapi = new CardTypeJsonForNeutrinoapi()
          {
            valid = result.ContainsKey("valid") ? result["valid"].ReturnString().ToBool() : false,
            issuer = result.ContainsKey("issuer") ? result["issuer"].ReturnString() : string.Empty,
            card_type = result.ContainsKey("card-type") ? result["card-type"].ReturnString() : string.Empty,
            issuer_website = result.ContainsKey("issuer-website") ? result["issuer-website"].ReturnString() : string.Empty,
            card_category = result.ContainsKey("card-category") ? result["card-category"].ReturnString() : string.Empty,
            issuer_phone = result.ContainsKey("issuer-phone") ? result["issuer-phone"].ReturnString() : string.Empty,
            country_code = result.ContainsKey("country-code") ? result["country-code"].ReturnString() : string.Empty,
            card_brand = result.ContainsKey("card-brand") ? result["card-brand"].ReturnString() : string.Empty,
            country = result.ContainsKey("country") ? result["country"].ReturnString() : string.Empty,
            api_error = result.ContainsKey("api-error") ? result["api-error"].ReturnString() : string.Empty,
            api_error_msg = result.ContainsKey("api-error-msg") ? result["api-error-msg"].ReturnString() : string.Empty
          };
          return cardTypeJsonForNeutrinoapi;
        }
      }
      else
      {
        return new CardTypeJsonForNeutrinoapi();
      }
    }

    #endregion

  }
}
