﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISG.Common.UserAgent
{
  public interface IUserAgentService
  {
    string GetClientInfo(string userAgent);
  }
}
