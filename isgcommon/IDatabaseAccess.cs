﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ISG.Common
{
  public interface IDatabaseAccess
  {
    DataTable ExecuteSelectQuery(String _query, SqlParameter[] sqlParameter);
    DataTable ExecuteSelectQuery(String _query);
    DataView ExecuteSelectQueryWithFilterExpressionAndOrderByClause(String _query, string _filterExpression, string _orderByExpresseion);
    int ExecuteInsertQuery(String _query, SqlParameter[] sqlParameter);
    int ExecuteUpdateQuery(String _query, SqlParameter[] sqlParameter);
    int ExecuteDeleteQuery(String _query, SqlParameter[] sqlParameter);
    int ExecuteDeleteQuery(String _query);
    Int32 ExecuteInsertQueryGetId(String _query, SqlParameter[] sqlParameter);
    string ExecuteScalarQuery(String _query, SqlParameter[] sqlParameter);
    DataSet ExecuteSelectQueryReturnDataSet(String _query);
    DataSet ExecuteSelectQueryReturnDataSet(String _query, SqlParameter[] sqlParameter);
    int ExecuteStoredProcedureNonQueryGetId(String spName, object parameter);
    int ExecuteStoredProcedureReaderGetId(String spName, SqlParameter[] sqlParameter);
    DataTable ExecuteSelectQueryStoreProcedure(String spName, object parameter);
    DataTable ExecuteSelectQueryStoreProcedure(String spName);
    int ExecuteStoredProcedureNonQuery(String _spName, SqlParameter[] sqlParameter);
    DataSet ExecuteSelectQueryStoreProcedureGetDataSet(String spName, object parameter);
    DataSet ExecuteSelectQueryStoreProcedureGetDataSet(String spName, object parameter, int CommandTimeout);
    DataSet ExecuteSelectQueryStoreProcedureGetDataSet(String spName);
    bool ExecuteMergeDeleteFromTable(DataTable newTableValues, DataTable oldTableValues, string tableName, string databaseName, List<string> primaryKeyColumNames, List<string> ignoreColumNames, bool skipDelete);
  }
}
