﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ISG.Common.Serialization
{
	
	public class GenericXMLSerializer<T>
	{
		XmlSerializer oSerializer;
		String ClassName;
		String BaseDirectory;		

		public GenericXMLSerializer()
		{
			oSerializer = new XmlSerializer(typeof(T));
			BaseDirectory = "";			
		}

		public GenericXMLSerializer(String oBaseDirectory)
		{
			oSerializer = new XmlSerializer(typeof(T));
			BaseDirectory = oBaseDirectory;
		}

		public String FileName(T oObj)
		{
			ClassName = oObj.GetType().Name;
			return BaseDirectory + @"\" + ClassName + ".xml";
		}

		public void Save(T oObj)
		{
			TextWriter oWriter = new StreamWriter(FileName(oObj));
			oSerializer.Serialize(oWriter, oObj);
			oWriter.Close();
		}

		public string SaveToString(T oObj)
		{
			StringWriter oWriter = new StringWriter();
			oSerializer.Serialize(oWriter, oObj);
			return oWriter.ToString();
		}

		public T Load(T oObj)
		{
			XmlSerializer oSerializer = new XmlSerializer(typeof(T));
			TextReader oReader = new StreamReader(FileName(oObj));
			T NewObject = (T)oSerializer.Deserialize(oReader);
			oReader.Close();
			return NewObject;
		}

		public T LoadFromString(string xmlString)
		{
			XmlSerializer oSerializer = new XmlSerializer(typeof(T));
			StringReader oReader = new StringReader(xmlString);
			T NewObject = (T)oSerializer.Deserialize(oReader);			
			return NewObject;
		}
	}

	
}
