﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace ISG.Common
{
	public class Caching
	{
		public static string GetCacheKey(NameValueCollection keyValues, string memcacheKeyPrefix)
		{
			keyValues.Sort();
			StringBuilder sb = new StringBuilder();

			sb.Append(memcacheKeyPrefix);

			foreach (string key in keyValues.AllKeys)
			{
				if (key!=null)
					sb.AppendFormat("{0}{1}", key.Trim(), keyValues[key].Trim());
			}

			return sb.ToString();
		}

		
	}
}
