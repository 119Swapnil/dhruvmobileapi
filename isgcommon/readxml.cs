﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ISG.Common.XML
{
  public static class ReadXml
  {
    public static DataView GetFilteredDataFromxml(string xmlFilePath, string filterExpression)
    {
      DataSet objDataSet = new DataSet();
      DataView objDataView;
      try
      {
        objDataSet.ReadXml(xmlFilePath);
        objDataSet.Tables[0].DefaultView.RowFilter = filterExpression;
        objDataView = objDataSet.Tables[0].DefaultView;
        return objDataView;
      }

      finally
      {
        objDataSet = null;
        objDataView = null;
      }
    }

    public static DataView GetDataFromxml(string xmlFilePath)
    {
      DataSet objDataSet = new DataSet();
      DataView objDataView;
      try
      {
        objDataSet.ReadXml(xmlFilePath);
        objDataView = objDataSet.Tables[0].DefaultView;
        return objDataView;
      }

      finally
      {
        objDataSet = null;
        objDataView = null;
      }
    }

    public static DataSet GetDataSetFromxml(string xmlFilePath)
    {
        DataSet objDataSet = new DataSet();
        ////DataView objDataView;
        try
        {
            objDataSet.ReadXml(xmlFilePath);
            //objDataView = objDataSet.Tables[0].DefaultView;
            return objDataSet;
        }

        finally
        {
            objDataSet = null;
            //objDataView = null;
        }
    }
    public static DataView GetFilteredDataFromxmlwithOrder(string xmlFilePath, string filterExpression, string orderByExpresseion)
    {
        DataSet objDataSet = new DataSet();
        DataView objDataView;
        try
        {
            objDataSet.ReadXml(xmlFilePath);
            objDataSet.Tables[0].DefaultView.RowFilter = filterExpression;
            objDataView = objDataSet.Tables[0].DefaultView;
            objDataView.Sort = orderByExpresseion;
            return objDataView;
        }

        finally
        {
            objDataSet = null;
            objDataView = null;
        }
    }

    public static DataView GetDataFromxmlWithOrder(string xmlFilePath, string orderByExpresseion)
    {
        DataSet objDataSet = new DataSet();
        DataView objDataView;
        try
        {
            objDataSet.ReadXml(xmlFilePath);
            objDataView = objDataSet.Tables[0].DefaultView;
            objDataView.Sort = orderByExpresseion;
            return objDataView;
        }

        finally
        {
            objDataSet = null;
            objDataView = null;
        }
    }

    public static DataView GetDataFromxml(string xmlFilePath,string xmlTablename)
    {
      DataSet objDataSet = new DataSet();
      DataView objDataView;
      try
      {
        objDataSet.ReadXml(xmlFilePath);
        objDataView = objDataSet.Tables[xmlTablename].DefaultView;
        return objDataView;
      }

      finally
      {
        objDataSet = null;
        objDataView = null;
      }
    }
  }
}
