﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using ISG.Common.Config;
using System.IO;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Security.Cryptography;
using ISG.Common.DB;
using System.Configuration;

namespace ISG.Common
{
  public static partial class CommonUtilities
  {
    public enum FormatType
    {
      MMMMDDYYYY,
      DateForSql,
      DateTimeForSql,
      DDMMYYYY,
      DDMMYYYY_HHMM,
      DDMMMYYYY,
      HHMMSS,
      DDDMMMYYYY,
      DDDMMMYYYY_HHMM,
      DDDMMMYYYY_HHMMSS,
      DDDMMMYYYY_withoutseparator,
      DDDDMMMYYYY,
      DDDDMMMYYYY_withoutseparator,
      YYYYMMDD,
      YYYY_MM_DD,
      DDMMMYYYYDDD,
      HHMM,
      DDDMMMYY,
      DDDMM,
      DDDMMMYY_yearwithpostrophe,
      DDMMYYYY_withoutseperator,
      DD_MM_YYYY,
      DDMMMMYYYY,
      HHMMSS_withoutseperator,
      DDMMYYYYTIME,
      DOW_Abbr,
      DOW,
      DDMMMYYYY_hyphen,
      DDMMMYYYYTIME_hyphen,
      DDMMMYY,
      DDMMMCommaHHMM,
      d,
      t,
      g
    }

    public enum DateInterval
    {
      Day,
      DayOfYear,
      Hour,
      Minute,
      Month,
      Quarter,
      Second,
      Weekday,
      WeekOfYear,
      Year
    }

    public static string GetFormatOfDateTime(FormatType formatType)
    {
      switch (formatType)
      {      
        case FormatType.MMMMDDYYYY:
          return "MMMM dd yyyy";
        case FormatType.DateForSql:
          return "MM/dd/yyyy";
        case FormatType.DateTimeForSql:
          return "MM/dd/yyyy hh:mm:ss";
        case FormatType.DDMMYYYY:
          return "dd/MM/yyyy";
        case FormatType.DDMMYYYY_HHMM:
          return "dd/MM/yyyy HH:mm";
        case FormatType.DDMMMYYYY:
          return "dd MMM yyyy";
        case FormatType.DDMMMYYYY_hyphen:
          return "dd-MMM-yyyy";
        case FormatType.DDMMMYYYYTIME_hyphen:
          return "dd-MMM-yyyy HH:mm";
        case FormatType.HHMMSS:
          return "HH:mm:ss";
        case FormatType.HHMM:
          return "HH:mm";
        case FormatType.DDDMMMYYYY:
          return "ddd, dd MMM yyyy";
        case FormatType.DDDMMMYYYY_HHMM:
          return "ddd, dd MMM yyyy HH:mm";
        case FormatType.DDDMMMYYYY_HHMMSS:
          return "ddd dd MMM yyyy HH:mm:ss";
        case FormatType.DDDMMMYYYY_withoutseparator:
          return "ddd dd MMM yyyy";
        case FormatType.DDMMYYYYTIME:
          return "dd/MM/yyyy HH:mm:ss";
        case FormatType.DDDDMMMYYYY:
          return "dddd, dd MMMM yyyy";
        case FormatType.DDDDMMMYYYY_withoutseparator:
          return "dddd dd MMMM yyyy";
        case FormatType.YYYYMMDD:
          return "yyyyMMdd";
        case FormatType.YYYY_MM_DD:
          return "yyyy-MM-dd";
        case FormatType.DDDMMMYY:
          return "ddd dd MMM yy";
        case FormatType.DDDMM:
          return "ddd, dd MMM";
        case FormatType.DDDMMMYY_yearwithpostrophe:
          return "ddd dd MMM -yy";
        case FormatType.DDMMYYYY_withoutseperator:
          return "ddMMyyyy";
        case FormatType.DD_MM_YYYY:
          return "dd-MM-yyyy";
        case FormatType.DDMMMMYYYY:
          return "dd MMMM yyyy";
        case FormatType.HHMMSS_withoutseperator:
          return "HHmmss";
        case FormatType.DOW_Abbr:
          return "ddd";
        case FormatType.DOW:
          return "dddd";
        case FormatType.DDMMMYYYYDDD:
          return " dd-MMM-yyyy (dddd)";
        case FormatType.DDMMMYY:
          return "dd MMM yy";
        case FormatType.DDMMMCommaHHMM:
          return "dd MMM, HH:mm";
        case FormatType.d:
          return "d";
        case FormatType.t:
          return "t";
        case FormatType.g:
          return "g";
        default:
          return "";
      }
    }

    public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
    {
      int place = Source.LastIndexOf(Find);

      if (place == -1)
        return Source;

      string result = Source.Remove(place, Find.Length).Insert(place, Replace);
      return result;
    }

    public static string ReplaceFirstOccurrence(string Source, string Find, string Replace)
    {
      int Place = Source.IndexOf(Find);
      string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
      return result;
    }
    public static string RemoveLeadingSeperator(this string objString, string seperator)
    {
      if (!string.IsNullOrEmpty(objString))
      {
        if (objString.Substring(0, 1) == seperator)
        {
          objString = objString.Remove(0, 1);
        }
      }
      return objString.Trim();
    }

    public static string FormateNullableDate(this Nullable<DateTime> objDateTime, FormatType formatString)
    {
      var format = GetFormatOfDateTime(formatString);
      if (objDateTime.HasValue && !string.IsNullOrWhiteSpace(format))
      {

        return objDateTime.Value.ToString(format);
      }
      return "";
    }

    public static string FormatDate(this Nullable<DateTime> objDateTime, FormatType formatString)
    {
      var format = GetFormatOfDateTime(formatString);
      if (objDateTime.HasValue && !string.IsNullOrWhiteSpace(format))
      {

        return objDateTime.Value.ToString(format);
      }
      return "";
    }

    public static string FormatDate(this DateTime objDateTime, FormatType formatString)
    {
      var format = GetFormatOfDateTime(formatString);
      if (!string.IsNullOrWhiteSpace(format))
        return objDateTime.ToString(format);

      return "";
    }

    public static DateTime NullableDateTimeToDateTime(this Nullable<DateTime> objDateTime)
    {
      if (objDateTime == null)
        return new DateTime();
      else
        return DateTime.Parse(objDateTime.ToString());
    }

    public static UInt16 ToUInt16(this string objString)
    {
      UInt16 number = 0;

      if (objString != null)
      {
        UInt16.TryParse(objString.ToString(), out number);
      }
      return number;
    }

    public static uint ToUInt32(this string objString)
    {
      UInt32 number = 0;

      if (objString != null)
      {
        UInt32.TryParse(objString.ToString(), out number);
      }
      return number;
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static string Ordinal(this int day)
    {
      switch (day)
      {
        case 1:
        case 21:
        case 31:
          return "st";
        case 2:
        case 22:
          return "nd";
        case 3:
        case 23:
          return "rd";
        default:
          return "th";
      }
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static int ToInt(this string objString)
    {
      int number = 0;

      if (objString != null)
      {
        int.TryParse(objString.ToString(), out number);
      }
      return number;
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static Int64 ToLong(this string objString)
    {
      Int64 number = 0;

      if (objString != null)
      {
        Int64.TryParse(objString.ToString(), out number);
      }
      return number;
    }

    public static string Right(this string objString, int NoOfChar)
    {
      string temp = objString.ToString().Trim();
      if (temp.Length < NoOfChar)
        return temp;
      else
        return temp.Substring(temp.Length - NoOfChar + 1, NoOfChar);
    }

    public static string Left(this string objString, int NoOfChar)
    {
      string temp = objString.ToString().Trim();
      if (temp.Length < NoOfChar)
        return temp;
      else
        return temp.Substring(1, NoOfChar);
    }

    //public static Nullable<DateTime> ToDateTime(this string objString)
    //{
    //  if ((objString != null))
    //  {
    //    try
    //    {
    //      System.DateTime dt = System.DateTime.Parse(objString);
    //      string[] expectedFormats = {
    //        "dd/MM/yyyy HH:mm:ss", 
    //        "dd/MM/yyyy HH:mm:ss tt", 
    //        "dd/MM/yyyy H:mm:ss", 
    //                  "dd/MM/yyyy H:mm:ss tt",
    //        "MM/dd/yyyy H:mm:ss",
    //        "MM/dd/yyyy",
    //                  "dd/MM/yyyy",
    //        "dddd, dd MMMM yyyy",
    //        "dddd, dd MMMM yyyy HH:mm:ss",
    //        "ddd, dd MMM yyyy HH:mm:ss",
    //        "yyyy-MM-ddTHH:mm:ss",
    //        "HH:mm:ss",
    //        "yyyy MMMM",
    //        "MM-MMMM, ddd-dddd"
    //      };
    //      DateTime dtTemp = DateTime.ParseExact(objString, expectedFormats, null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
    //      return Convert.ToDateTime(dtTemp);
    //    }
    //    catch
    //    {
    //      // Not a date, handle appropriately
    //      return null;
    //    }
    //  }
    //  else
    //  {
    //    return null;
    //  }
    //}
    [System.Diagnostics.DebuggerStepThrough]
    public static Nullable<DateTime> ToDateTime(this string objString, CultureInfo culture)
    {
      DateTime _date = new DateTime();
      if (objString.Trim() != string.Empty)
      {
        try
        {
          if (!DateTime.TryParse(objString, culture, DateTimeStyles.None, out _date))
          {
            return (Nullable<DateTime>)null;
          }

        }
        catch (Exception)
        {
          return (Nullable<DateTime>)null;
        }
      }
      return (Nullable<DateTime>)_date;
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static Nullable<DateTime> ToDateTime(this string objString)
    {
      DateTime? retDate = new Nullable<DateTime>();
      if (objString.Trim() != string.Empty)
      {
        try
        {
          DateTime _date = new DateTime();
          if (DateTime.TryParse(objString, System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out _date))
          {
            retDate = _date;
          }
        }
        catch (Exception)
        {
          //
        }
      }
      return retDate;
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static bool ToBool(this string objString)
    {
      if (string.IsNullOrEmpty(objString))
        return false;

      if ((objString.Contains("1")) || (objString.Trim().ToUpper().Contains("TRUE")))
        return true;
      else
        return false;
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static Guid ToGuid(this string objString)
    {
      if (objString != null)
        return new Guid(objString);
      else
        return new Guid();
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static Double ToDouble(this string objString)
    {
      Double number = 0;

      if (objString != null)
      {
        if (!objString.Contains("."))
        {
          objString += ".0";
        }
        Double.TryParse(objString.ToString(), out number);
      }
      return number;
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static short ToShort(this string objString)
    {
      short number = 0;

      if (objString != null)
      {
        short.TryParse(objString.ToString(), out number);
      }
      return number;
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static decimal ToDecimal(this string objString)
    {
      decimal number = 0;

      if (objString != null)
      {
        decimal.TryParse(objString.ToString(), out number);
      }
      return number;
    }

    public static byte[] ToByteArray(this string str)
    {
      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
      return encoding.GetBytes(str);
    }

    [DebuggerStepThrough]
    public static string ReturnString(this object obj)
    {
      if (obj == null || Convert.IsDBNull(obj))
        return "";
      else
        return obj.ToString().Trim();
      //.Replace("http://staging.steppestravel.co.uk:8084/","http://d4agkz4ifvt5n.cloudfront.net/");
      //the above change was done for Steppes Preset 22_01_2015
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static object CheckNullforSQL(this object obj)
    {
      if (obj == null || Convert.IsDBNull(obj) || obj.ReturnString() == "")
        return DBNull.Value;
      else
        return obj;
    }

    /// <summary>
    /// Function to clean a string for inserting it in javascript.
    /// </summary>
    /// <param name="str">the string being cleaned.</param>
    public static string CleanStringForJavascript(this string value)
    {
      // define the cleaner
      string[] dirtyChars = new string[2] { "\"", "'" };
      string[] carriageReturn = new string[1] { "\r" };

      // clean user input (if it finds any of the values above, 
      // it will replace it with whatever is in the quotes - in this example, it prefixes it with a slash)
      foreach (string s in dirtyChars)
      {
        value = value.Replace(s, @"\" + s);
      }

      // removes all carriage returns
      foreach (string s in carriageReturn)
      {
        value = value.Replace(s, "");
      }


      return value;
    }

    /// <summary>
    /// Receives string and returns the string with its letters reversed.
    /// </summary>
    public static string ReverseString(this string value)
    {
      char[] arr = value.ToCharArray();
      Array.Reverse(arr);
      return new string(arr);
    }

    /// <summary>
    /// Function to return a bool in a format acceptable by SQL.
    /// </summary>
    /// <param name="str">the string being cleaned.</param>
    public static int boolForSQL(this bool value)
    {
      return value ? 1 : 0;
    }


    /// <summary>
    /// Function to check Field lenght.
    /// </summary>
    /// <param name="strToCheck">the string to check.</param>
    /// <param name="maxLength">Max length required.</param>
    public static bool CheckFieldlenght(string strToCheck, int minLength, int maxLength)
    {
      if (strToCheck.Length > minLength && strToCheck.Length <= maxLength)
      {
        return true;
      }
      else
      {
        return false;
      }

    }
    /// <summary>
    /// Function to check string contains only alpha.
    /// </summary>
    /// <param name="strToCheck">the string to check.</param>


    public static bool IsAlpha(string strToCheck)
    {
      Regex objAlphaPattern = new Regex("[^a-zA-Z]");
      return !objAlphaPattern.IsMatch(strToCheck);
    }

    /// <summary>
    /// Function to check string contains only numbers.
    /// </summary>
    /// <param name="strToCheck">the string to check.</param>
    public static bool IsNumeric(string strNumber)
    {
      Regex objNotNaturalPattern = new Regex("[^0-9]");
      Regex objNaturalPattern = new Regex("0*[1-9][0-9]*");
      return !objNotNaturalPattern.IsMatch(strNumber) &&
      objNaturalPattern.IsMatch(strNumber);
    }

    /// <summary>
    /// Calculate age based on date of birth and current system date.
    /// </summary>
    /// <param name="DOB">date of birth</param>
    /// <param name="isMonth">out parameter to pass back if the value being returned is in years or months</param>
    /// <returns>age as an int</returns>
    public static int calculateAge(DateTime DOB, ref bool isMonth)
    {
      return calculateAge(DOB, DateTime.Today, ref isMonth);
    }

    /// <summary>
    /// Calculate age based on date of birth and current date passed to the function.
    /// </summary>
    /// <param name="DOB">date of birth </param>
    /// /// <param name="currentDate">current date (maybe different from system date) </param>
    /// <returns>age as an int</returns>
    public static int calculateAge(DateTime DOB, DateTime dateAsOn, ref bool isMonth)
    {
      isMonth = false;
      int age = 0;

      if (dateAsOn.Year != DOB.Year)
      {
        // format the date to yyyymmdd and subtract the date of birth from the current date then drop the last 4 digits 
        // eg 20080814 - 19800703 = 281111 drop last 4 digits --> age = 28
        string ageString = ((dateAsOn.FormatDate(FormatType.YYYYMMDD)).ToInt() - (DOB.FormatDate(FormatType.YYYYMMDD)).ToInt()).ToString();

        age = ageString.Length > 4 ? ageString.ReverseString().Substring(4).ReverseString().ToInt() : 0;
      }

      if (age > 0)
      {
        return age;
      }
      else
      {
        isMonth = true;
        return ((dateAsOn.Month - 1) + (DOB.Month - 12 - 1) + dateAsOn.Day > DOB.Day ? 1 : 0);
      }
    }

    /// <summary>
    /// Calculate age based on date of birth and current system date.
    /// </summary>
    /// <param name="DOB">date of birth</param>
    /// <param name="isMonth">out parameter to pass back if the value being returned is in years or months</param>
    /// <returns>age as a nullable int</returns>
    public static int? calculateAge(DateTime? DOB, ref bool isMonth)
    {
      return calculateAge(DOB, DateTime.Today, ref isMonth);
    }

    /// <summary>
    /// Calculate age based on date of birth and current date passed to the function.
    /// </summary>
    /// <param name="DOB">date of birth </param>
    /// /// <param name="currentDate">current date (maybe different from system date) </param>
    /// <returns>age as a nullable int</returns>
    public static int? calculateAge(DateTime? DOB, DateTime dateAsOn, ref bool isMonth)
    {
      isMonth = false;
      int? age = null;
      if (DOB.HasValue)
      {
        if (dateAsOn.Year != DOB.Value.Year)
        {
          // format the date to yyyymmdd and subtract the date of birth from the current date then drop the last 4 digits 
          // eg 20080814 - 19800703 = 281111 drop last 4 digits --> age = 28  
          string ageString = ((dateAsOn.FormatDate(FormatType.YYYYMMDD)).ToInt() - (DOB.Value.FormatDate(FormatType.YYYYMMDD)).ToInt()).ToString();

          age = ageString.Length > 4 ? ageString.ReverseString().Substring(4).ReverseString().ToInt() : 0;
        }

        if (age > 0)
        {
          return age;
        }
        else
        {
          isMonth = true;
          return ((dateAsOn.Month - 1) + (DOB.Value.Month - 12 - 1) + dateAsOn.Day > DOB.Value.Day ? 1 : 0);
        }
      }
      return age;
    }

    public static double GetRoundOff(double value)
    {
      return Math.Round(value, AppConfig.RoundToDecimalPlaces);
    }

    /// <summary>
    /// Converts string to Title Case
    /// Title Case converts every first letter of a word in a sentence to upper case, and the rest to lower case.
    /// </summary>
    /// <param name="value">String to be converted</param>
    /// <returns>String in title case</returns>
    public static string ToTitleCase(this string value)
    {
      return new CultureInfo("en-US", false).TextInfo.ToTitleCase(value.ToLower());
    }

    public static void FillDropdownWithRange(int min, int max, DropDownList dropDown)
    {
      for (int i = min; i < max + 1; i++)
      {
        dropDown.Items.Add(i.ToString());
      }
    }

    /// <summary>
    /// gets the current requests ip address
    /// </summary>
    /// <returns></returns>
    public static string GetClientIPAddress()
    {
      // get the current request
      HttpRequest CurrentRequest = System.Web.HttpContext.Current.Request;

      string ip = CurrentRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];

      // the HTTP_X_FORWARDED_FOR may contain an array of IP, this can happen if you connect through a proxy
      // to avoid this problem you can parse the HTTP_X_FORWARDED_FOR for the last entery IP.      
      if (!string.IsNullOrEmpty(ip))
      {
        string[] ipRange = ip.Split(',');
        int le = ipRange.Length - 1;
        ip = ipRange[le];
      }
      // the HTTP_X_FORWARDED_FOR may not return anything in that case use REMOTE_ADDR
      else
      {
        ip = CurrentRequest.ServerVariables["REMOTE_ADDR"];
      }

      return ip;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <param name="paramNamePrefix"></param>
    /// <returns></returns>
    public static string[] GenerateINClauseParamArray(this string[] value, string paramNamePrefix)
    {
      string[] paramNames = value.Select(
                          (s, i) => "@" + paramNamePrefix + i.ToString()
                  ).ToArray();
      return paramNames;
    }
    /// <summary>
    /// Prefix a root qualified link with the web root passed in
    /// </summary>
    /// <param name="value"></param>
    /// <param name="webRoot"></param>
    /// <returns></returns>
    public static string PrefixWebRoot(this string value, string webRoot)
    {
      return value.Replace("~", webRoot);
    }

    /// <summary>
    /// Prefix a root qualified link with the current requests web root
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string PrefixWebRoot(this string value)
    {
      return value.Replace("~", GetWebRoot());
    }

    /// <summary>
    /// Get current web root of the request
    /// </summary>
    /// <returns></returns>
    public static string GetWebRoot()
    {
      return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute("~");
    }

    /// <summary>
    /// Gets the value of the string if it is not empty or null and defaults when not. 
    /// </summary>
    /// <param name="value"></param>
    /// <param name="defaultValue"></param>
    /// <returns></returns>
    public static string GetValueOrDefault(this string value, string defaultValue)
    {
      return string.IsNullOrEmpty(value) ? defaultValue : value;
    }

    public static string GetCurrentPageName()
    {
      string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
      System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
      string sRet = oInfo.Name;
      return sRet;
    }

    public static DateTime? TryConvertStringToDatetime(string dateString, CultureInfo culture, DateTimeStyles style)
    {
      DateTime parsedDate;
      if (DateTime.TryParse(dateString, culture, style, out parsedDate))
        return parsedDate;

      return null;
    }
    //cuurently currency code is hard coded
    //please change afterward
    public static string FormatForCurrency(this double value)
    {
      return String.Format("{0:£#,##0.00;- £#,##0.00;0}", value);
    }

    public static string FormatForTwoDecimalPlaces(this double value)
    {
      return String.Format("{0:0.00}", value);
    }

    public static string ToNumber(this decimal value, int decimalPlaces)
    {
      return string.Format(string.Format("{{0:n{0}}}", decimalPlaces), value);
    }
    public static string ToNumber(this double value, int decimalPlaces)
    {
      return string.Format(string.Format("{{0:n{0}}}", decimalPlaces), value);
    }
    public static string ToCurrency(this decimal value, int decimalPlaces)
    {
      return string.Format(string.Format("{{0:c{0}}}", decimalPlaces), value);
    }

    public static bool IsLessThanToday(this DateTime value)
    {
      return value < System.DateTime.Today;
    }

    /// <summary>
    /// adds separators in a date string like 28022011 or 280211
    /// 28022011 -> 28/02/2011 
    /// 280211 -> 28/02/11
    /// </summary>
    /// <param name="dateString"></param>
    /// <returns></returns>
    public static string AddseparatorsInDateString(this string dateString)
    {
      if (!(dateString.Trim().Length == 6 || dateString.Trim().Length == 8))
      {
        throw new ArgumentException("Invalid Length - should be 6/8 ", "dateString");
      }

      return dateString.Insert(2, "/").Insert(5, "/");
    }

    /// <summary>
    /// adds separators in a date string like 1108 or 1108AM
    /// 1108 -> 11:08 
    /// 1108AM -> 11:08AM 
    /// </summary>
    /// <param name="timeString"></param>
    /// <returns></returns>
    public static string AddseparatorsInTimeString(this string timeString)
    {
      if (!(timeString.Trim().Length == 4 || timeString.Trim().Length == 6))
      {
        throw new ArgumentException("Invalid Length - should be 4/6", "timeString");
      }

      return timeString.Insert(2, ":");
    }


    // Paging in LINQ
    //used by LINQ to SQL
    public static IQueryable<TSource> Page<TSource>(this IQueryable<TSource> source, int page, int pageSize)
    {
      return source.Skip((page - 1) * pageSize).Take(pageSize);
    }

    //used by LINQ
    public static IEnumerable<TSource> Page<TSource>(this IEnumerable<TSource> source, int page, int pageSize)
    {
      return source.Skip((page - 1) * pageSize).Take(pageSize);
    }
    //check wether the appsetting key present or not
    public static bool CheckAppSettingKeyExist(string key)
    {
      if (!string.IsNullOrEmpty(key))
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    //this function returns unique string with length less than 25 
    //As we are not sure so added if the length is greater than 25 then substring the string
    public static string GetUniqueString()
    {
      string strSessionID = "";
      StringBuilder sb = new StringBuilder();
      sb.Append(System.Convert.ToBase64String(Guid.NewGuid().ToByteArray()));
      strSessionID = sb.ReturnString();
      if (strSessionID.Length >= 25)
      {
        strSessionID = strSessionID.Substring(0, 25);
      }
      return strSessionID;
    }

    public static bool CheckFileOnRemoteServer(string strFileName)
    {
      bool bolReturnValue = false;
      //
      try
      {
        WebRequest request = HttpWebRequest.Create(strFileName);
        request.Method = "HEAD"; // Just get the document headers, not the data.
        request.Credentials = System.Net.CredentialCache.DefaultCredentials;
        //
        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
          if (response.StatusCode == HttpStatusCode.OK)
          {
            bolReturnValue = true;
          }
        }
      }
      catch (Exception)
      {
        bolReturnValue = false;
      }
      //
      return bolReturnValue;
    }

    public static string BuildJavaScriptAssociativeArray(NameValueCollection oList, string varName, string scriptName)
    {
      StringBuilder sb = new StringBuilder();

      if (oList != null)
      {
        sb.Append(" var " + varName + " = new Array();");
        foreach (string key in oList.Keys)
        {
          sb.Append(" " + varName + "[\"" + key + "\"] = \"" + oList.GetValues(key)[0].ToString() + "\";");
        }
      }

      return sb.ToString();

    }

    public static DateTime GetLastModifiedDate(string filePath)
    {
      if (File.Exists(filePath))
        return File.GetLastWriteTime(filePath);
      return DateTime.Now;
    }

    public static NameValueCollection Sort(this NameValueCollection nameValueCollection)
    {
      // Create a temporary collection the same size as the original
      NameValueCollection sortedNameValueCollection = new NameValueCollection(nameValueCollection.Count);

      // Sort the keys
      string[] keys = nameValueCollection.AllKeys;
      Array.Sort(keys);

      foreach (string key in keys)
      {
        // Sort the values
        string[] values = nameValueCollection[key].Split(',');
        Array.Sort(values);

        // Add each value to the temporary collection
        foreach (string value in values)
        {
          sortedNameValueCollection.Add(key, value);
        }
      }

      return sortedNameValueCollection;
      //// Clear the original collection
      //nameValueCollection.Clear();

      //// Add the sorted entries back
      //nameValueCollection.Add(tempNameValueCollection);
    }

    public static DataTable GetDistinctRecords(DataTable dt, string[] Columns)
    {
      DataTable dtUniqRecords = new DataTable();
      dtUniqRecords = dt.DefaultView.ToTable(true, Columns);
      return dtUniqRecords;
    }

    public static string Truncate(this string value, int maxLength)
    {
      return value.ReturnString().Length <= maxLength ? value : value.Substring(0, maxLength);
    }

    public static string ReplaceKeywords(string content, IDictionary<string, string> keywords)
    {
      var replacedString = string.Empty;
      if (!string.IsNullOrWhiteSpace(content))
      {
        replacedString = content;
        foreach (var key in keywords)
        {
          var keyName = string.Empty;
          if (!key.Key.StartsWith("{"))
            keyName = "{" + key.Key + "}";
          else
            keyName = key.Key;

          if (!string.IsNullOrWhiteSpace(key.Value) && replacedString.IndexOf(keyName, StringComparison.OrdinalIgnoreCase) > -1)
            replacedString = Regex.Replace(replacedString, keyName, key.Value, RegexOptions.IgnoreCase);
        }
      }
      return replacedString;
    }

    public static string GetSQlQueryByReplacingValueOfParameters(string _query, List<SqlParameter> sqlparameters)
    {

      try
      {
        if (!string.IsNullOrWhiteSpace(_query))
        {
          foreach (SqlParameter param in sqlparameters)
          {
            _query = _query.Replace(param.ParameterName.ReturnString(), "'" + param.Value.ReturnString() + "'");
          }
        }
        else
        {
          return "";
        }
        return _query;
      }
      finally
      {

      }
    }

    public static string ReplaceInvalidFileNameChars(this string s, string replacement = "")
    {
      return Regex.Replace(s, "[" + Regex.Escape(new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars())) + "]",
          replacement, //can even use a replacement string of any length
          RegexOptions.IgnoreCase);

      //below code was not working correctly.. was replacing digit 1 as well
      /*      return Regex.Replace(s,
              "[" + Regex.Escape(
                Path.VolumeSeparatorChar +
                Path.DirectorySeparatorChar +
                Path.AltDirectorySeparatorChar +
                ":" + //added to cover Windows & Mac in case code is run on UNIX
                "\\" + //added for future platforms "/" + //same as previous one
                "<" +
                ">" +
                "|" +
                "\b" +
                "" +
                "\t" + //based on characters not allowed on Windows
                new string(Path.GetInvalidPathChars()) + //seems to miss *, ? and "
                "*" +
                "?" +
                "\""
                ) + "]",
              replacement, //can even use a replacement string of any length
              RegexOptions.IgnoreCase);
            //not using System.IO.Path.InvalidPathChars (deprecated insecure API)
       * */
    }

    [System.Diagnostics.DebuggerStepThrough]
    public static string ToXmlString(this object obj)
    {
      return ToXmlString(obj, null, "");
    }
    [System.Diagnostics.DebuggerStepThrough]
    public static string ToXmlString(this object obj, System.Xml.Serialization.XmlSerializerNamespaces namespaces, string defaultNamespace)
    {
      var xmlSerializer = new System.Xml.Serialization.XmlSerializer(obj.GetType(), defaultNamespace);

      var memStream = new MemoryStream();
      var sw = new StreamWriter(memStream, Encoding.UTF8);
      xmlSerializer.Serialize(sw, obj, namespaces);
      return Encoding.UTF8.GetString(memStream.ToArray());
    }

    public static string Encrypt(string input, string key)
    {
      try
      {
        //string key = ConfigurationManager.AppSettings["EncryptionKey"].ReturnString();
        byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
        TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
        tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
        tripleDES.Mode = CipherMode.ECB;
        tripleDES.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = tripleDES.CreateEncryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
        tripleDES.Clear();
        string encryptedText = Convert.ToBase64String(resultArray, 0, resultArray.Length);
        encryptedText = encryptedText.Replace("/", "-").Replace("+", "_").Replace("=", ",").Replace("?", ".").Replace("&", ";");
        return encryptedText;
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static string Decrypt(string input, string key)
    {
      try
      {
        //string key = ConfigurationManager.AppSettings["EncryptionKey"].ReturnString();
        string decryptText = input.Replace("-", "/").Replace("_", "+").Replace(",", "=").Replace(".", "?").Replace(";", "&");
        byte[] inputArray = Convert.FromBase64String(decryptText);
        TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
        tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
        tripleDES.Mode = CipherMode.ECB;
        tripleDES.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = tripleDES.CreateDecryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
        tripleDES.Clear();
        return UTF8Encoding.UTF8.GetString(resultArray);
      }
      catch
      {
        return "";
      }
    }

    public static long DateDiff(DateInterval intervalType, System.DateTime dateOne, System.DateTime dateTwo)
    {
      switch (intervalType)
      {
        case DateInterval.Day:
        case DateInterval.DayOfYear:
          System.TimeSpan spanForDays = dateTwo - dateOne;
          return (long)spanForDays.TotalDays;
        case DateInterval.Hour:
          System.TimeSpan spanForHours = dateTwo - dateOne;
          return (long)spanForHours.TotalHours;
        case DateInterval.Minute:
          System.TimeSpan spanForMinutes = dateTwo - dateOne;
          return (long)spanForMinutes.TotalMinutes;
        case DateInterval.Month:
          return ((dateTwo.Year - dateOne.Year) * 12) + (dateTwo.Month - dateOne.Month);
        case DateInterval.Quarter:
          long dateOneQuarter = (long)System.Math.Ceiling(dateOne.Month / 3.0);
          long dateTwoQuarter = (long)System.Math.Ceiling(dateTwo.Month / 3.0);
          return (4 * (dateTwo.Year - dateOne.Year)) + dateTwoQuarter - dateOneQuarter;
        case DateInterval.Second:
          System.TimeSpan spanForSeconds = dateTwo - dateOne;
          return (long)spanForSeconds.TotalSeconds;
        case DateInterval.Weekday:
          System.TimeSpan spanForWeekdays = dateTwo - dateOne;
          return (long)(spanForWeekdays.TotalDays / 7.0);
        case DateInterval.WeekOfYear:
          System.DateTime dateOneModified = dateOne;
          System.DateTime dateTwoModified = dateTwo;
          while (dateTwoModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
          {
            dateTwoModified = dateTwoModified.AddDays(-1);
          }
          while (dateOneModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
          {
            dateOneModified = dateOneModified.AddDays(-1);
          }
          System.TimeSpan spanForWeekOfYear = dateTwoModified - dateOneModified;
          return (long)(spanForWeekOfYear.TotalDays / 7.0);
        case DateInterval.Year:
          return dateTwo.Year - dateOne.Year;
        default:
          return 0;
      }
    }

    public static string CleanExtraReturnKeysAtEndV2(string strCountryVisaInfo)
    {
      return strCountryVisaInfo.ReturnString();
    }

    public static string CleanExtraReturnKeysAtBeginningV2(string strCountryVisaInfo)
    {
      return strCountryVisaInfo.ReturnString();
    }

    public static string GetCompanyInfo(string strCompanyCode, string strCompanyURL)
    {
      return strCompanyCode.ReturnString();
    }
    public static string ReplaceLanguageSpecificWords(string content, string countryCode, string countryCodeDefault, DatabaseAccess databaseAccess)
    {
      var replacedString = string.Empty;
      if (!string.IsNullOrWhiteSpace(content))
      {
        var plainContent = Regex.Replace(content, "<(.|\n)*?>", "");
        //var pattern = new Regex(@"\{@(.*?)\}");
        var pattern = new Regex(@"\w{4,}\b");
        var matchWords = pattern.Matches(plainContent);
        if (matchWords != null && matchWords.Count > 0)
        {
          Dictionary<string, string> matchWordsWithValues = GetMatchWordsValue(matchWords, countryCode, countryCodeDefault, databaseAccess);

          replacedString = content;
          foreach (var key in matchWordsWithValues)
          {
            if (!string.IsNullOrWhiteSpace(key.Value) && replacedString.IndexOf(key.Key, StringComparison.OrdinalIgnoreCase) > -1)
              replacedString = Regex.Replace(replacedString, key.Key, key.Value, RegexOptions.IgnoreCase); ;
          }
        }
        else
        {
          replacedString = content;
        }
      }
      return replacedString;
    }

    private static Dictionary<string, string> GetMatchWordsValue(MatchCollection matchWords, string countryCode, string countryCodeDefault, DatabaseAccess databaseAccess)
    {
      Dictionary<string, string> returnValue = new Dictionary<string, string>();
      string query = string.Empty;
      string words = string.Empty;
      DataTable dtWord = null;
      if (matchWords != null && matchWords.Count > 0)
      {
        List<string> lstWords = new List<string>();
        
        foreach (Match word in matchWords)
        {
          lstWords.Add("'" + word.Value + "'");
        }
        query = @"select * from presets.dbo.TranslationWords where word in (" + string.Join(",", lstWords) + ")";

        dtWord = databaseAccess.ExecuteSelectQuery(query);
        if (dtWord != null && dtWord.Rows.Count > 0)
        {
          foreach (DataRow dataRow in dtWord.Rows)
          {
            string value = string.Empty;

            if (dataRow.Table.Columns.Contains("Translation_" + countryCode.ReturnString()))
            {
              if (!string.IsNullOrWhiteSpace(dataRow["Translation_" + countryCode].ReturnString()))
              {
                value = dataRow["Translation_" + countryCode].ReturnString();
              }
              else
              {
                value = dataRow["word"].ReturnString();
              }
            }
            else if (dataRow.Table.Columns.Contains("Translation_" + countryCodeDefault.ReturnString()))
            {
              if (!string.IsNullOrWhiteSpace(dataRow["Translation_" + countryCodeDefault].ReturnString()))
              {
                value = dataRow["Translation_" + countryCodeDefault].ReturnString();
              }
              else
              {
                value = dataRow["word"].ReturnString();
              }
            }
            else
            {
              value = dataRow["word"].ReturnString();
            }
            if (!returnValue.ContainsKey(dataRow["Word"].ReturnString()))
            {
              returnValue.Add(dataRow["Word"].ReturnString(), value.ReturnString());
            }
          }

        }
      }
      return returnValue;
    }

    public static string SwapLastTwoCharWithFirstTwo(string input)
    {//atleast 4 chars required
      try
      {
        StringBuilder output = new StringBuilder();
        char[] characters = input.ReturnString().ToCharArray();
        if (characters.Length < 4)
        {
          return input.ReturnString();
        }
        else
        {
          for (int i = 0; i < characters.Length; i++)
          {
            if (i == 0)
              output.Append(characters[characters.Length - 2]);
            else if (i == 1)
              output.Append(characters[characters.Length - 1]);
            else if (i == characters.Length - 2)
              output.Append(characters[0]);
            else if (i == characters.Length - 1)
              output.Append(characters[1]);
            else
            {
              output.Append(characters[i]);
            }
          }
        }
        return output.ReturnString();
      }
      catch (Exception)
      {
        return input.ReturnString();
      }
    }

    public static string RemoveFromEnd(this string s, string suffix)
    {
      if (s.EndsWith(suffix,StringComparison.OrdinalIgnoreCase))
      {
        return s.Substring(0, s.Length - suffix.Length);
      }
      else
      {
        return s;
      }
    }
    public static bool BreakOngoingProcess()
    {
      if (ConfigurationManager.AppSettings["BreakOngoingProcess"].ReturnString().Equals("y",StringComparison.OrdinalIgnoreCase))
      {
        return true;
      }
      else
      {
        return false;
      }

    }
  }
}
