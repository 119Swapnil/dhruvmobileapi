﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISG.Common.Config;
using System.Net.Mail;
using System.Net;
using ISG.Common;
using System.Configuration;
using RestSharp;

namespace ISG.Common
{
  public class MailGunResponse
  {
    public string message;
    public string id;
  }
  public class SendMailV1
  {
    private string _sendMailBy = "";
    private string _toMail = "";
    private string _BCC = "";
    private string _cCId = "";
    private string _sendMailSubject = "";
    private bool _isBodyHtml = false;
    private string _fileName = "";
    private System.IO.Stream _inputStream = null;
    private string _sendMailByDisplayName = "";
    private MailPriority _priority = MailPriority.Normal;

    public bool IsBodyHtml
    {
      get
      {
        return _isBodyHtml;
      }
      set
      {
        _isBodyHtml = (bool)value;
      }
    }
    public string SendMailBy
    {
      get
      {
        return _sendMailBy;
      }

      set
      {
        _sendMailBy = value;
      }
    }

    public string SendByMailDisplayName
    {
      get
      {
        return _sendMailByDisplayName;
      }
      set
      {
        _sendMailByDisplayName = value;
      }
    }
    public string ToMail
    {
      get
      {
        return _toMail;
      }

      set
      {
        _toMail = value;
      }
    }
    public string BCC
    {
      get
      {
        return _BCC;
      }

      set
      {
        _BCC = value;
      }
    }
    public string CCId
    {
      get
      {
        if (_cCId == "")
          return _cCId = "";
        else
          return _cCId;
      }

      set
      {
        if (_cCId != value)
        {
          _cCId = value;
        }
      }
    }
    public string SendMailSubject
    {
      get
      {
        return _sendMailSubject;
      }

      set
      {
        _sendMailSubject = value;
      }
    }
    public string FileName
    {
      get
      {
        return _fileName;
      }

      set
      {
        _fileName = value;
      }
    }

    public System.IO.Stream InputStream
    {
      get
      {
        return _inputStream;
      }

      set
      {
        _inputStream = value;
      }
    }

    public List<Attachment> Attachments
    {
      get;
      set;
    }

    public List<string> InlineAttachments
    {
      get;
      private set;
    }

    public string TempDirectoryUri
    {
      get;
      set;
    }
    public string MailContent
    {
      get;
      private set;
    }
    public MailPriority Priority
    {
      get
      {
        return _priority;
      }
      set
      {
        _priority = value;
      }
    }
    #region Method
    public bool SendMailMessage(string strMsg, ref string messageId, string strMsgTest = "")
    {
      return SendMailMessage(strMsg, "", ref messageId, strMsgTest);
    }
    public bool SendMailMessage(string strMsg, string bookingNo, ref string messageId, string strMsgTest = "")
    {
      return SendMailMessage(strMsg, bookingNo, false, ref messageId, strMsgTest);
    }
    public bool SendMailMessage(string strMsg, bool useDefaultCredentials, ref string messageId, string strMsgTest = "")
    {
      return SendMailMessage(strMsg, "", useDefaultCredentials, ref messageId, strMsgTest);
    }
    public bool SendMailMessage(string strMsg, string bookingNo, bool useDefaultCredentials, ref string messageId, string strMsgTest = "")
    {
      return SendMailMessage(strMsg, bookingNo, useDefaultCredentials, "", "", ref messageId, strMsgTest);
    }
    public bool SendMailMessage(string strMsg, string bookingNo, string userName, string password, ref string messageId, string strMsgTest = "")
    {
      return SendMailMessage(strMsg, bookingNo, true, userName, password, ref messageId, strMsgTest);
    }
    public bool SendMailMessage(string strMsg, string userName, string password, ref string messageId, string strMsg_PlaintText = "", string strMsgTest = "")
    {
      return SendMailMessage(strMsg, "", true, userName, password, ref messageId, strMsg_PlaintText, strMsgTest);
    }

    private bool SendMailMessage(string strMsg, string bookingNo, bool useDefaultCredentials, string userName, string password, ref string messageId, string strMsg_PlaintText = "", string strMsgTest = "")
    {
      if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["MailGunUrl"].ReturnString()))
      {
        return SendMailMessageViaSMTP(strMsg, bookingNo, useDefaultCredentials, userName, password, ref messageId, strMsg_PlaintText);
      }
      else
      {
        return SendMailMessageViaMailGun(strMsg, ref messageId, strMsg_PlaintText);
      }
    }

    public string SendMailMessage(string htmlContent, string textContent)
    {
      var messageId = string.Empty;
      this.SendMailMessageViaMailGun(htmlContent, textContent, ref messageId);
      return messageId;
    }

    private bool SendMailMessageViaMailGun(string htmlContent, string textContent, ref string messageId)
    {
      try
      {
        this.InlineAttachments = new List<string>();
        this.MailContent = string.Empty;
        if (!string.IsNullOrWhiteSpace(_sendMailBy.ReturnString()))
        {
          RestClient client = new RestClient();
          client.BaseUrl = ConfigurationManager.AppSettings["MailGunUrl"].ReturnString();
          client.Authenticator = new HttpBasicAuthenticator("api", ConfigurationManager.AppSettings["MailGunApiKey"].ReturnString());

          RestRequest request = new RestRequest();
          request.AddParameter("domain", ConfigurationManager.AppSettings["MailGunDomain"].ReturnString(), ParameterType.UrlSegment);
          request.Resource = "{domain}/messages";
          var address = string.Empty;

          if (_sendMailBy.IndexOf('<') > 0 && _sendMailBy.IndexOf('>') > 0)
          {
            address = _sendMailBy.ReturnString();
          }
          else
          {
            var displayName = !string.IsNullOrEmpty(_sendMailByDisplayName.ReturnString()) ? _sendMailByDisplayName.ReturnString() : _sendMailBy.ReturnString();
            address = string.Format("{0} <{1}>", displayName, _sendMailBy.ReturnString());
          }
          request.AddParameter("from", address);

          if (!string.IsNullOrWhiteSpace(ToMail))
          {
            request.AddParameter("to", string.Join(",", ToMail.Split(';')));
          }
          if (!string.IsNullOrWhiteSpace(CCId))
          {
            request.AddParameter("cc", string.Join(",", CCId.Split(';')));
          }
          if (!string.IsNullOrWhiteSpace(BCC))
          {
            request.AddParameter("bcc", string.Join(",", BCC.Split(';')));
          }
          request.AddParameter("subject", _sendMailSubject.ReturnString());
          if (!string.IsNullOrEmpty(htmlContent))
          {
            htmlContent = processConentForInlineImages(htmlContent, request);
            request.AddParameter("html", htmlContent);
          }

          if (!string.IsNullOrEmpty(textContent))
          {
            request.AddParameter("text", textContent);
          }
          //
          this.MailContent = !string.IsNullOrEmpty(htmlContent) ? htmlContent : textContent;

          if (Attachments != null && Attachments.Count > 0)
          {
            foreach (Attachment attach in Attachments)
            {
              var buffer = ReadToEnd(attach.ContentStream);
              request.AddFile("attachment", buffer, attach.Name);
            }
          }

          switch (Priority)
          {
            case MailPriority.Low:
              request.AddParameter("h:X-Priority", 5);
              break;
            case MailPriority.High:
              request.AddParameter("h:X-Priority", 1);
              break;
          }
          request.AddParameter("h:x-mailgun-native-send", true);
          

          request.Method = Method.POST;
          client.Timeout = ConfigurationManager.AppSettings["SMTPTimeout"].ReturnString().ToInt();
          var response = client.Execute(request);

          if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
          {
            var content = Newtonsoft.Json.JsonConvert.DeserializeObject<MailGunResponse>(response.Content);
            messageId = content.id.ReturnString();
            return true;
          }
        }
        else
        {
          return false;
        }
      }
      finally
      {
        //close attachment handle
        if (Attachments != null && Attachments.Count > 0)
        {
          foreach (var attachment in Attachments)
          {
            attachment.Dispose();
          }
        }
      }
      return false;
    }

    private bool SendMailMessageViaMailGun(string strMsg, ref string messageId, string strMsg_PlaintText = "")
    {
      if (this.IsBodyHtml)
        return this.SendMailMessageViaMailGun(strMsg, strMsg_PlaintText, ref messageId);
      else
        return this.SendMailMessageViaMailGun("", strMsg, ref messageId);
    }

    private string getInlineFileContentId(ref int imageCtr, string ext)
    {
      var contentId = string.Empty;

      do
      {
        imageCtr++;
        contentId = string.Format("image{0}{1}", imageCtr.ToString().PadLeft(3, '0'), ext);

      } while (Attachments != null && Attachments.Count > 0 && Attachments.Where(a => a.Name.Equals(contentId, StringComparison.OrdinalIgnoreCase)).Any());

      return contentId;
    }

    private string GetDefaultExtension(string mimeType)
    {
      string result;
      Microsoft.Win32.RegistryKey key;
      object value;

      key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"MIME\Database\Content Type\" + mimeType, false);
      value = key != null ? key.GetValue("Extension", null) : null;
      result = value != null ? value.ToString() : string.Empty;

      return result;
    }

    private string processConentForInlineImages(string Content, RestRequest request)
    {
      if (!string.IsNullOrEmpty(Content.ReturnString()))
      {
        var tempFolder = !string.IsNullOrWhiteSpace(this.TempDirectoryUri) ? this.TempDirectoryUri : System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp", Guid.NewGuid().ReturnString().ToLower());
        var imageCtr = 0;
        var htmlImg = new HtmlAgilityPack.HtmlDocument();
        htmlImg.LoadHtml(Content);
        var imgs = htmlImg.DocumentNode.SelectNodes("//img[@src]");
        var regExp = @"(^https?:\/\/.+\/api\/mail\/attachment\/\d+)+\?(.+)";
        if (imgs != null && imgs.Count > 0)
        {
          System.Net.WebClient wc = new System.Net.WebClient();
          foreach (var img in imgs)
          {
            if (img.Attributes != null && img.Attributes.Count > 0 && img.Attributes.Contains("src"))
            {
              var isInline = img.Attributes.Contains("data-inline") ? img.Attributes["data-inline"].Value.ToBool() : false;
              var src = string.Empty;
              if (img.Attributes.Contains("data-src-local"))
              {
                src = img.Attributes["data-src-local"].Value;
                if (string.IsNullOrWhiteSpace(src) || !(isInline || System.Text.RegularExpressions.Regex.IsMatch(src, regExp)))
                {
                  src = string.Empty;
                }
              }
              if (string.IsNullOrEmpty(src))
              {
                src = img.Attributes["src"].Value;
                if (string.IsNullOrWhiteSpace(src) || !(isInline || System.Text.RegularExpressions.Regex.IsMatch(src, regExp)))
                {
                  src = string.Empty;
                }
              }
              if (!string.IsNullOrWhiteSpace(src))
              {
                //var ext = string.Empty;
                //var matches = System.Text.RegularExpressions.Regex.Match(src, regExp).Groups;
                //if (matches.Count > 2)
                //  ext = matches[2].ReturnString().Trim();

                //inline atttachments
                var sContents = wc.DownloadData(src);

                var ext = string.Empty;
                if (System.Text.RegularExpressions.Regex.IsMatch(src, regExp))
                {
                  var matches = System.Text.RegularExpressions.Regex.Match(src, regExp).Groups;
                  if (matches.Count > 2)
                    ext = matches[2].ReturnString().Trim();
                }
                else if (!string.IsNullOrEmpty(wc.ResponseHeaders["Content-Type"]))
                {
                  var contentType = wc.ResponseHeaders["Content-Type"].ReturnString();
                  ext = this.GetDefaultExtension(contentType);
                }
                var contentId = getInlineFileContentId(ref imageCtr, ext);
                var tempFileUri = System.IO.Path.Combine(tempFolder, contentId);
                if (!System.IO.Directory.Exists(tempFolder))
                {
                  System.IO.Directory.CreateDirectory(tempFolder);
                }
                System.IO.File.WriteAllBytes(tempFileUri, sContents);

                this.InlineAttachments.Add(tempFileUri);

                request.AddFile("inline", sContents, contentId);
                img.Attributes["src"].Value = string.Format("cid:{0}", contentId);
                if (img.Attributes.Contains("data-src-local"))
                {
                  img.Attributes.Remove("data-src-local");
                }
                if (img.Attributes.Contains("data-inline"))
                {
                  img.Attributes.Remove("data-inline");
                }
              }
            }
          }
        }
        return htmlImg.DocumentNode.InnerHtml;
      }
      return Content;
    }

    //method for converting stream to byte[]
    private byte[] ReadToEnd(System.IO.Stream stream)
    {
      long originalPosition = stream.Position;
      stream.Position = 0;

      try
      {
        byte[] readBuffer = new byte[4096];

        int totalBytesRead = 0;
        int bytesRead;

        while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
        {
          totalBytesRead += bytesRead;

          if (totalBytesRead == readBuffer.Length)
          {
            int nextByte = stream.ReadByte();
            if (nextByte != -1)
            {
              byte[] temp = new byte[readBuffer.Length * 2];
              Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
              Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
              readBuffer = temp;
              totalBytesRead++;
            }
          }
        }

        byte[] buffer = readBuffer;
        if (readBuffer.Length != totalBytesRead)
        {
          buffer = new byte[totalBytesRead];
          Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
        }
        return buffer;
      }
      finally
      {
        stream.Position = originalPosition;
      }
    }

    private bool SendMailMessageViaSMTP(string strMsg, string bookingNo, bool useDefaultCredentials, string userName, string password, ref string messageId, string strMsg_PlaintText = "")
    {
      ////' Mail initialization
      MailMessage mailMsg = new MailMessage();
      SmtpClient client;
      // SmtpClient 

      try
      {

        if (!string.IsNullOrEmpty(_sendMailByDisplayName.ReturnString()))
          mailMsg.From = new MailAddress(_sendMailBy.ReturnString(), _sendMailByDisplayName.ReturnString());
        else
          mailMsg.From = new MailAddress(_sendMailBy.ReturnString());

        foreach (var emailAddress in ToMail.Split(';'))
        {
          mailMsg.To.Add(new MailAddress(emailAddress.ReturnString()));
        }

        if (!string.IsNullOrWhiteSpace(CCId.ReturnString()))
        {
          foreach (var emailAddress in CCId.Split(';'))
          {
            mailMsg.CC.Add(new MailAddress(emailAddress.ReturnString()));
          }
        }

        if (!string.IsNullOrWhiteSpace(BCC.ReturnString()))
        {
          foreach (var emailAddress in BCC.Split(';'))
          {
            mailMsg.Bcc.Add(new MailAddress(emailAddress.ReturnString()));
          }
        }

        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SendMailBCC"].ReturnString()))
        {
          mailMsg.Bcc.Add(ConfigurationManager.AppSettings["SendMailBCC"].ReturnString());
        }

        mailMsg.Subject = _sendMailSubject;
        mailMsg.Body = strMsg;
        mailMsg.IsBodyHtml = _isBodyHtml;

        if (!string.IsNullOrEmpty(strMsg_PlaintText))
        {
          AlternateView textView = AlternateView.CreateAlternateViewFromString(strMsg_PlaintText);
          textView.ContentType = new System.Net.Mime.ContentType("text/plain");
          mailMsg.AlternateViews.Add(textView);
        }
        if (_isBodyHtml && !string.IsNullOrEmpty(strMsg))
        {
          AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strMsg);
          htmlView.ContentType = new System.Net.Mime.ContentType("text/html");
          mailMsg.AlternateViews.Add(htmlView);
        }
        mailMsg.Priority = Priority;

        if (!useDefaultCredentials)
        {
          client = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["MailServer"].ReturnString(), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Port"].ReturnString()));
          client.UseDefaultCredentials = false;
          client.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SendMailUsername"].ReturnString(), System.Configuration.ConfigurationManager.AppSettings["SendMailPassword"].ReturnString());
          ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
          if (System.Configuration.ConfigurationManager.AppSettings["EnableSsl"].ReturnString().ToLower() == "y")
          {
            client.EnableSsl = true;
          }
          else
          {
            client.EnableSsl = false;
          }
        }
        else
        {
          client = new SmtpClient();
          if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
          {
            client.UseDefaultCredentials = false;
            if (client.Credentials == null)
              client.Credentials = new System.Net.NetworkCredential(userName, password);
            else
            {
              var credential = (NetworkCredential)client.Credentials;
              credential.UserName = userName;
              credential.Password = password;
            }
          }
        }
        // single file
        if (_fileName != "")
          mailMsg.Attachments.Add(new Attachment(_inputStream, _fileName));

        if (Attachments != null && Attachments.Count > 0)
        {
          foreach (Attachment attach in Attachments)
            mailMsg.Attachments.Add(attach);
        }

        try
        {
          messageId = string.Format("<{0}@{1}>", Guid.NewGuid().ToString().Replace("-", "").ToUpper(), mailMsg.From.Host);
          mailMsg.Headers.Add("Message-ID", messageId);
          client.Timeout = System.Configuration.ConfigurationManager.AppSettings["SMTPTimeout"].ReturnString().ToInt();
          client.Send(mailMsg);
          return true;
        }
        catch (Exception ex)
        {
          throw new Exception(ex.Message);
        }
        finally
        {
          client.Dispose();
          //close attachment handle
          if (Attachments != null && Attachments.Count > 0)
          {
            foreach (var attachment in Attachments)
              attachment.Dispose();
            //if (attachment.ContentStream != null && attachment.ContentStream.Length > 0)
            //  attachment.ContentStream.Close();
          }
          mailMsg.Dispose();
        }
      }
      finally
      {
        mailMsg = null;
        client = null;
      }
    }
    #endregion
  }
}