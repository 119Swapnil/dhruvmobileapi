﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace ISG.Common.DB
{
  public static class ConnectionConfiguration
  {
		private static string _travelclub = ConfigurationManager.ConnectionStrings["Travelclub"].ReturnString();

    public static string ConTravelclub
    {
			get { return _travelclub; }
    }   
  }
}
