﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System;
using System.Linq;
using System.Text;

namespace ISG.Common.DB
{
  public class DatabaseAccess : IDatabaseAccess
  {
    private SqlDataAdapter objAdapter;
    private SqlConnection objConnection;

    /// <constructor>
    /// Initialize Connection
    /// </constructor>
    public DatabaseAccess(string connectionString)
    {
      objAdapter = new SqlDataAdapter();
      objConnection = new SqlConnection(connectionString);
    }


    /// <method>
    /// Open Database Connection if Closed or Broken
    /// </method>
    private SqlConnection OpenConnection()
    {
      if (objConnection.State == ConnectionState.Closed || objConnection.State ==
                  ConnectionState.Broken)
      {
        objConnection.Open();
      }
      return objConnection;
    }

    /// <method>
    /// Select Query
    /// </method>
    public DataTable ExecuteSelectQuery(String _query, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      DataTable objDataTable = new DataTable();
      objDataTable = null;
      DataSet objDataSet = new DataSet();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.Parameters.AddRange(sqlParameter);
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        if (objDataSet != null && objDataSet.Tables.Count > 0)
          objDataTable = objDataSet.Tables[0];
        return objDataTable;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataTable = null;
        objDataSet = null;
      }

    }

    public DataTable ExecuteSelectQuery(String _query)
    {
      SqlCommand objCommand = new SqlCommand();
      DataTable objDataTable = new DataTable();
      objDataTable = null;
      DataSet objDataSet = new DataSet();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        if (objDataSet != null && objDataSet.Tables.Count > 0)
          objDataTable = objDataSet.Tables[0];
        return objDataTable;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataTable = null;
        objDataSet = null;
      }

    }


    public DataView ExecuteSelectQueryWithFilterExpressionAndOrderByClause(String _query, string _filterExpression, string _orderByExpresseion)
    {
      SqlCommand objCommand = new SqlCommand();
      DataTable objDataTable = new DataTable();
      objDataTable = null;
      DataSet objDataSet = new DataSet();
      DataView objDataView = new DataView();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        if (objDataSet != null && objDataSet.Tables.Count > 0)
        {
          objDataTable = objDataSet.Tables[0];
          objDataTable.DefaultView.RowFilter = _filterExpression;
          objDataView = objDataTable.DefaultView;
          objDataView.Sort = _orderByExpresseion;
        }
        return objDataView;
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataTable = null;
        objDataSet = null;
      }

    }

    /// <method>
    /// Insert Query
    /// </method>
    public int ExecuteInsertQuery(String _query, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.Parameters.AddRange(sqlParameter);
        objAdapter.InsertCommand = objCommand;
        return objCommand.ExecuteNonQuery();
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();

        }
        objCommand = null;
      }
    }

    /// <method>
    /// Update Query
    /// </method>
    public int ExecuteUpdateQuery(String _query, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.Parameters.AddRange(sqlParameter);
        objAdapter.UpdateCommand = objCommand;
        return objCommand.ExecuteNonQuery();
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();

        }
        objCommand = null;
      }

    }

    /// <method>
    /// Delete Query
    /// </method>
    public int ExecuteDeleteQuery(String _query, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.Parameters.AddRange(sqlParameter);
        objAdapter.DeleteCommand = objCommand;
        return objCommand.ExecuteNonQuery();
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();

        }
        objCommand = null;
      }

    }

    public int ExecuteDeleteQuery(String _query)
    {
      SqlCommand objCommand = new SqlCommand();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objAdapter.DeleteCommand = objCommand;
        return objCommand.ExecuteNonQuery();
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();

        }
        objCommand = null;
      }

    }


    public Int32 ExecuteInsertQueryGetId(String _query, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.Parameters.AddRange(sqlParameter);
        objAdapter.InsertCommand = objCommand;
        return Convert.ToInt32(objCommand.ExecuteScalar());
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
      }
    }

    public string ExecuteScalarQuery(String _query, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      object temp;
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.Parameters.AddRange(sqlParameter);
        objAdapter.SelectCommand = objCommand;
        temp = objCommand.ExecuteScalar();
        return (temp == null ? string.Empty : temp.ToString());
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
      }
    }

    #region "Stored Procedure"

    public DataTable ExecuteSelectQueryStoreProcedure(String spName, object parameter)
    {
      SqlCommand objCommand = new SqlCommand();
      DataTable objDataTable = new DataTable();
      objDataTable = null;
      DataSet objDataSet = new DataSet();
      try
      {
        //start of coding for listing sp
        SqlParameter[] paralist = (SqlParameter[])parameter;
        string[] parmetername = new string[20];
        string[] parmeterValue = new string[20];

        for (int i = 0; i < paralist.Length; i++)
        {
          parmetername[i] = paralist[i].ParameterName;
          parmeterValue[i] = paralist[i].Value.ToString();
        }
        Writespname(spName, parmetername, parmeterValue);
        //end of coding for listing sp
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = spName;
        objCommand.CommandType = CommandType.StoredProcedure;
        SqlParameter[] sqlParameter = (SqlParameter[])parameter;
        objCommand.Parameters.AddRange(sqlParameter);
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        if (objDataSet != null && objDataSet.Tables.Count > 0)
          objDataTable = objDataSet.Tables[0];
        return objDataTable;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand.Parameters.Clear();
        objCommand = null;
        objDataSet = null;
        objDataTable = null;
      }
    }

    public DataTable ExecuteSelectQueryStoreProcedure(String spName)
    {

      SqlCommand objCommand = new SqlCommand();
      DataTable objDataTable = new DataTable();
      objDataTable = null;
      DataSet objDataSet = new DataSet();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = spName;
        objCommand.CommandType = CommandType.StoredProcedure;
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        if (objDataSet != null && objDataSet.Tables.Count > 0)
          objDataTable = objDataSet.Tables[0];
        return objDataTable;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand.Parameters.Clear();
        objCommand = null;
        objDataSet = null;
        objDataTable = null;
      }

    }

    public int ExecuteStoredProcedureNonQuery(String _spName, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.CommandText = _spName;
        objCommand.Parameters.AddRange(sqlParameter);
        objAdapter.UpdateCommand = objCommand;
        return objCommand.ExecuteNonQuery();
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
      }
    }

    public DataTable ExecuteStoredProcedureReader(String _spName, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      DataTable objDataTable = new DataTable();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.CommandText = _spName;
        objCommand.Parameters.AddRange(sqlParameter);
        //objCommand.ExecuteReader();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataTable);
        return objDataTable;
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();

        }
        objCommand = null;
        objDataTable = null;
      }

    }

    public DataSet ExecuteStoreProcedure(String spName, object parameter)
    {
      SqlCommand objCommand = new SqlCommand();
      DataSet objDataSet = new DataSet();
      try
      {

        //start of coding for listing sp
        SqlParameter[] paralist = (SqlParameter[])parameter;
        string[] parmetername = new string[20];
        string[] parmeterValue = new string[20];

        for (int i = 0; i < paralist.Length; i++)
        {
          parmetername[i] = paralist[i].ParameterName;
          parmeterValue[i] = paralist[i].Value.ToString();
        }
        Writespname(spName, parmetername, parmeterValue);
        //end of coding for listing sp



        objCommand.Connection = OpenConnection();
        objCommand.CommandText = spName;
        objCommand.CommandType = CommandType.StoredProcedure;
        SqlParameter[] sqlParameter = (SqlParameter[])parameter;
        objCommand.Parameters.AddRange(sqlParameter);
        objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        return objDataSet;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataSet = null;
      }
    }

    public DataSet ExecuteStoreProcedure(String spName)
    {
      SqlCommand objCommand = new SqlCommand();
      DataSet objDataSet = new DataSet();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = spName;
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        return objDataSet;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataSet = null;
      }
    }

    public DataSet ExecuteSelectQueryStoreProcedureGetDataSet(String spName, object parameter)
    {
      SqlCommand objCommand = new SqlCommand();
      DataSet objDataSet = new DataSet();
      try
      {
        //start of coding for listing sp
        SqlParameter[] paralist = (SqlParameter[])parameter;
        string[] parmetername = new string[20];
        string[] parmeterValue = new string[20];

        for (int i = 0; i < paralist.Length; i++)
        {
          parmetername[i] = paralist[i].ParameterName;
          parmeterValue[i] = paralist[i].Value.ToString();
        }
        Writespname(spName, parmetername, parmeterValue);
        //end of coding for listing sp


        objCommand.Connection = OpenConnection();
        objCommand.CommandText = spName;
        objCommand.CommandType = CommandType.StoredProcedure;
        SqlParameter[] sqlParameter = (SqlParameter[])parameter;
        objCommand.Parameters.AddRange(sqlParameter);
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        return objDataSet;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand.Parameters.Clear();
        objCommand = null;
        objDataSet = null;
      }
    }

    public DataSet ExecuteSelectQueryStoreProcedureGetDataSet(String spName, object parameter, int CommandTimeout)
    {
      SqlCommand objCommand = new SqlCommand();
      DataSet objDataSet = new DataSet();
      try
      {

        //start of coding for listing sp
        SqlParameter[] paralist = (SqlParameter[])parameter;
        string[] parmetername = new string[20];
        string[] parmeterValue = new string[20];

        for (int i = 0; i < paralist.Length; i++)
        {
          parmetername[i] = paralist[i].ParameterName;
          parmeterValue[i] = paralist[i].Value.ToString();
        }
        Writespname(spName, parmetername, parmeterValue);
        //end of coding for listing sp


        objCommand.Connection = OpenConnection();
        objCommand.CommandText = spName;
        objCommand.CommandType = CommandType.StoredProcedure;
        SqlParameter[] sqlParameter = (SqlParameter[])parameter;
        objCommand.Parameters.AddRange(sqlParameter);
        objCommand.CommandTimeout = CommandTimeout;
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        return objDataSet;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand.Parameters.Clear();
        objCommand = null;
        objDataSet = null;
      }
    }

    public DataSet ExecuteSelectQueryStoreProcedureGetDataSet(String spName)
    {
      SqlCommand objCommand = new SqlCommand();
      DataSet objDataSet = new DataSet();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = spName;
        objCommand.CommandType = CommandType.StoredProcedure;
        //objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        return objDataSet;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataSet = null;
      }
    }

    /// <summary>
    /// This function executes a non query in a stored procedure and returns the sequence number of the inserted/updated row.
    /// </summary>
    /// <param name="spName">Stored procedure name.</param>
    /// <param name="parameter">SQL Parameters.</param>
    /// <returns>Identity</returns>
    public int ExecuteStoredProcedureNonQueryGetId(String spName, object parameter)
    {
      SqlConnection conn = new SqlConnection();
      SqlCommand objCommand = new SqlCommand();

      try
      {
        conn = OpenConnection();
        objCommand = new SqlCommand(spName, conn);
        SqlParameter[] sqlParameter = (SqlParameter[])parameter;

        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.CommandText = spName;
        objCommand.Parameters.Add("@Identity", SqlDbType.Int);
        objCommand.Parameters["@Identity"].Direction = ParameterDirection.Output;
        objCommand.Parameters.AddRange(sqlParameter);
        objCommand.ExecuteNonQuery();
				return (int)objCommand.Parameters["@Identity"].Value;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand.Parameters.Clear();
        objCommand = null;
      }
    }

    public int ExecuteStoredProcedureReaderGetId(String _spName, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();
      DataTable objDataTable = new DataTable();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.CommandText = _spName;
        objCommand.Parameters.AddRange(sqlParameter);
        //objCommand.ExecuteReader();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataTable);
        if (objDataTable.Columns.Contains("Column1") && !string.IsNullOrWhiteSpace(objDataTable.Rows[0]["Column1"].ReturnString()))
        {
          return objDataTable.Rows[0]["Column1"].ReturnString().ToInt();
        }
        else
          return 1;
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();

        }
        objCommand = null;
        objDataTable = null;
      }

    }

    #endregion

    public DataSet ExecuteSelectQueryReturnDataSet(String _query)
    {
      SqlCommand objCommand = new SqlCommand();

      DataSet objDataSet = new DataSet();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        return objDataSet;
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataSet = null;
      }
    }

    static void Writespname(string spName, string[] parametername, string[] parametervalue)
    {


      try
      {
        string[] spParameter = new string[20];
        string finalparameter = "";
        // Read the file as one string.
        string text = System.IO.File.ReadAllText(@"D:\\spfile.txt");

        string path = @"D:\\spfile.txt";

        // This text is added only once to the file.

        // Create a file to write to.
        //spname = already stored txt data
        string createText = text + Environment.NewLine;
        File.WriteAllText(path, createText);

        // This text is always added, making the file longer over time
        // if it is not deleted.
        for (int i = 0; i < parametername.Length; i++)
        {
          if (parametername[i] == null)
          {
            break;
          }

          spParameter[i] = parametername[i] + '=' + "'" + parametervalue[i] + "'";
          if (i == 0)
          {
            finalparameter = spParameter[0];
          }
          else
          {
            if (!string.IsNullOrEmpty(finalparameter))
            {
              finalparameter = finalparameter + "," + spParameter[i];
            }
          }
        }

        //  string appendText = "exec " + spName + spParameter + Environment.NewLine;
        string appendText = "exec " + spName + '\t' + finalparameter;
        File.AppendAllText(path, appendText);

        // Open the file to read from.
        string readText = File.ReadAllText(path);
        Console.WriteLine(readText);
      }
      catch (Exception e) { }



    }

    public DataSet ExecuteSelectQueryReturnDataSet(String _query, SqlParameter[] sqlParameter)
    {
      SqlCommand objCommand = new SqlCommand();

      DataSet objDataSet = new DataSet();
      try
      {
        objCommand.Connection = OpenConnection();
        objCommand.CommandText = _query;
        objCommand.Parameters.AddRange(sqlParameter);
        objCommand.ExecuteNonQuery();
        objAdapter.SelectCommand = objCommand;
        objAdapter.Fill(objDataSet);
        return objDataSet;
      }

      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
        objCommand = null;
        objDataSet = null;
      }
    }

    #region "Publish"
    private List<SqlParameter> GetSqlParametersFromTable(DataRow dataRow, DataColumnCollection dataColumnCollection)
    {
      List<SqlParameter> sqlParameters = new List<SqlParameter>();
      try
      {
        foreach (DataColumn column in dataColumnCollection)
        {
          if (column.DataType.Name.Equals("DateTime", StringComparison.OrdinalIgnoreCase))
          {
            var param = new SqlParameter("@" + column.ColumnName, SqlDbType.DateTime);
            if (dataRow[column.ColumnName].ReturnString().ToDateTime().HasValue)
              param.Value = dataRow[column.ColumnName].ReturnString().ToDateTime();
            else
              param.Value = DBNull.Value;

            sqlParameters.Add(param);
          }
          else
          {
            sqlParameters.Add(new SqlParameter("@" + column.ColumnName, dataRow[column.ColumnName]));
          }
        }
        return sqlParameters;
      }
      finally
      {
        sqlParameters = null;
      }
    }

    public bool ExecuteMergeDeleteFromTable(DataTable newTableValues, DataTable oldTableValues, string tableName, string databaseName, List<string> primaryKeyColumNames, List<string> ignoreColumNames, bool skipDelete = false)
    {
      StringBuilder query = new StringBuilder();
      List<SqlParameter> sqlparameters = new List<SqlParameter>();
      List<DataColumn> primaryKeyOldTable = new List<DataColumn>();
      List<DataColumn> primaryKeyNewTable = new List<DataColumn>();
      try
      {
        foreach (DataColumn column in oldTableValues.Columns)
        {
          if (primaryKeyColumNames.Any(s => column.ColumnName.ReturnString().Equals(s, StringComparison.OrdinalIgnoreCase)))
          {
            primaryKeyOldTable.Add(column);
          }
        }

        foreach (DataColumn column in newTableValues.Columns)
        {
          if (primaryKeyColumNames.Any(s => column.ColumnName.ReturnString().Equals(s, StringComparison.OrdinalIgnoreCase)))
          {
            primaryKeyNewTable.Add(column);
          }
        }

        oldTableValues.PrimaryKey = primaryKeyOldTable.ToArray();
        newTableValues.PrimaryKey = primaryKeyNewTable.ToArray();
        // Accept changes.
        oldTableValues.AcceptChanges();
        using (DataTableReader changeReader = new DataTableReader(newTableValues))
          oldTableValues.Load(changeReader, LoadOption.Upsert);
        //OldTableValues.Merge(newTableValues);

        DataTable dtChanges = oldTableValues.GetChanges(DataRowState.Added);

        if (dtChanges != null)
        {
          ExecuteInsertIntoTableFromTable(dtChanges, tableName, databaseName, ignoreColumNames != null && ignoreColumNames.Count() > 0);
          dtChanges.Clear();
        }
        dtChanges = oldTableValues.GetChanges(DataRowState.Modified);

        if (dtChanges != null)
        {
          ExecuteUpdateIntoTableFromTable(dtChanges, tableName, databaseName, primaryKeyColumNames, ignoreColumNames);
          dtChanges.Clear();
        }

        if (!skipDelete)
        {
          foreach (DataRow rowOld in oldTableValues.Rows)
          {
            bool isPresent = false;
            foreach (DataRow rowNew in newTableValues.Rows)
            {
              int columnsMatch = 0;
              foreach (string column in primaryKeyColumNames)
              {
                if (rowNew[column].ReturnString().Equals(rowOld[column].ReturnString(), StringComparison.OrdinalIgnoreCase))
                {
                  ++columnsMatch;
                }
              }
              if (columnsMatch == primaryKeyColumNames.Count)
              {
                isPresent = true;
                break;
              }
            }
            if (!isPresent)
            {
              rowOld.Delete();
            }
          }

          dtChanges = oldTableValues.GetChanges(DataRowState.Deleted);

          if (dtChanges != null)
          {
            ExecuteDeleteIntoTableFromTable(dtChanges, tableName, databaseName, primaryKeyColumNames);
            dtChanges.Clear();
          }
        }
        return true;
      }
      catch (Exception ex)
      {
        throw new Exception("DataAccess > ExecuteMergeDeleteFromTable > TableName:" + tableName.ReturnString() + ex.Message, ex);
      }
      finally
      {
        query.Clear();
        sqlparameters = null;
      }
    }

    public int ExecuteInsertIntoTableFromTable(DataTable table, string tableName, string databaseName, bool identityColumnsPresent)
    {
      StringBuilder query = new StringBuilder();
      List<SqlParameter> sqlparameters = new List<SqlParameter>();

      try
      {
        if (identityColumnsPresent)
          query.Append("SET IDENTITY_INSERT " + databaseName.ReturnString() + tableName.ReturnString() + " ON ");

        query.Append("insert into " + databaseName.ReturnString() + tableName.ReturnString());
        query.Append(" ( ");
        foreach (DataColumn column in table.Columns)
        {
          //if (column.ColumnName.ReturnString().ToLower() == "update" || column.ColumnName.ReturnString().ToLower() == "1460x640" || column.ColumnName.ReturnString().ToLower() == "order")
          query.Append("[" + column.ColumnName.ReturnString() + "],");
          //else
          //query.Append(column.ColumnName.ReturnString() + ",");
        }
        query.Remove(query.Length - 1, 1);
        query.Append(" ) Values (");
        foreach (DataColumn column in table.Columns)
        {
          query.Append("@" + column.ColumnName.ReturnString() + ",");
        }
        query.Remove(query.Length - 1, 1);
        query.Append(" )");

        if (identityColumnsPresent)
          query.Append("SET IDENTITY_INSERT " + databaseName.ReturnString() + tableName.ReturnString() + " OFF ");

        foreach (DataRow dataRow in table.Rows)
        {
          SqlCommand objCommand = new SqlCommand();
          sqlparameters = GetSqlParametersFromTable(dataRow, table.Columns);
          objCommand.Connection = OpenConnection();
          objCommand.CommandText = query.ReturnString();
          objCommand.Parameters.AddRange(sqlparameters.ToArray());
          objAdapter.InsertCommand = objCommand;
          objCommand.ExecuteNonQuery();
          objCommand.Parameters.Clear();
          objCommand = null;
        }
        return table.Rows.Count;
      }
      catch (Exception ex)
      {
        throw new Exception("DataAccess > ExecuteInsertIntoTableFromTable > TableName:" + tableName.ReturnString() + ex.Message, ex);
      }
      finally
      {
        if (objConnection.State == ConnectionState.Open)
        {
          objConnection.Close();
        }
      }
    }

    private int ExecuteUpdateIntoTableFromTable(DataTable dtChanges, string tableName, string databaseName, List<string> primaryKeyColumNames, List<string> ignoreColumNames)
    {
      StringBuilder query = new StringBuilder();
      bool ignoreColPresent = false;

      int noOfRowsAffected = 0;
      try
      {
        DataTable schema = dtChanges;
        if (ignoreColumNames != null && ignoreColumNames.Count > 0)
          ignoreColPresent = true;

        foreach (DataRow dataRow in dtChanges.Rows)
        {
          query.Clear();
          List<SqlParameter> sqlparameters = new List<SqlParameter>();

          query.Append("Update " + databaseName + tableName + " set  ");
          foreach (DataColumn column in schema.Columns)
          {
            // check if this is required - needs to add Insert identity
            //primaryKeyColumNames.Any(p => column.ColumnName.Equals(p, StringComparison.OrdinalIgnoreCase) || (
            if (!ignoreColPresent || (ignoreColPresent && !ignoreColumNames.Any(s => column.ColumnName.ReturnString().Equals(s, StringComparison.OrdinalIgnoreCase))))
            {
              query.Append("[" + column.ColumnName + "]= @" + column.ColumnName + ",");
            }
            if (column.DataType.Name.Equals("DateTime", StringComparison.OrdinalIgnoreCase))
            {
              var param = new SqlParameter("@" + column.ColumnName, SqlDbType.DateTime);
              if (dataRow[column.ColumnName].ReturnString().ToDateTime().HasValue)
                param.Value = dataRow[column.ColumnName].ReturnString().ToDateTime();
              else
                param.Value = DBNull.Value;

              sqlparameters.Add(param);
            }
            else
              sqlparameters.Add(new SqlParameter("@" + column.ColumnName, dataRow[column.ColumnName]));
          }
          query.Remove(query.Length - 1, 1);
          query.Append(" where  ");

          foreach (string primaryKey in primaryKeyColumNames)
          {
            query.Append(" " + primaryKey + "= @" + primaryKey + " and");
          }
          query.Remove(query.Length - 4, 4);
          if (primaryKeyColumNames == null || primaryKeyColumNames.Count == 0)
          {
            throw new Exception("primary key must have values");
          }
          else
          {
            noOfRowsAffected += ExecuteUpdateQuery(query.ReturnString(), sqlparameters.ToArray());
          }
          sqlparameters = null;
        }
        return noOfRowsAffected;
      }
      catch (Exception ex)
      {
        throw new Exception("DataAccess > ExecuteUpdateIntoTableFromTable > TableName:" + tableName.ReturnString() + ex.Message, ex);
      }
      finally
      {
        query.Clear();
      }
    }

    private int ExecuteDeleteIntoTableFromTable(DataTable dtChanges, string tableName, string databaseName, List<string> primaryKeyColumNames)
    {
      StringBuilder query = new StringBuilder();

      int noOfRowsAffected = 0;
      try
      {
        DataTable schema = dtChanges;

        foreach (DataRow dataRow in dtChanges.Rows)
        {
          query.Clear();
          List<SqlParameter> sqlparameters = new List<SqlParameter>();

          query.Append("Delete from  " + databaseName + tableName);
          foreach (DataColumn column in schema.Columns)
          {
            //query.Append("[" + column.ColumnName + "]= @" + column.ColumnName + ",");

            if (column.DataType.Name.Equals("DateTime", StringComparison.OrdinalIgnoreCase))
            {
              var param = new SqlParameter("@" + column.ColumnName, SqlDbType.DateTime);
              if (dataRow[column.ColumnName, DataRowVersion.Original].ReturnString().ToDateTime().HasValue)
                param.Value = dataRow[column.ColumnName, DataRowVersion.Original].ReturnString().ToDateTime();
              else
                param.Value = DBNull.Value;

              sqlparameters.Add(param);
            }
            else
              sqlparameters.Add(new SqlParameter("@" + column.ColumnName, dataRow[column.ColumnName, DataRowVersion.Original]));
          }

          query.Append(" where  ");
          foreach (string primaryKey in primaryKeyColumNames)
          {
            query.Append(" " + primaryKey + "= @" + primaryKey + " and");
          }
          query.Remove(query.Length - 4, 4);

          if (primaryKeyColumNames == null || primaryKeyColumNames.Count == 0)
          {
            throw new Exception("primary key must have values");
          }
          else
          {
            noOfRowsAffected += ExecuteDeleteQuery(query.ReturnString(), sqlparameters.ToArray());
          }
          sqlparameters = null;
        }
        return noOfRowsAffected;
      }
      catch (Exception ex)
      {
        throw new Exception("DataAccess > ExecuteDeleteIntoTableFromTable > TableName:" + tableName.ReturnString() + ex.Message, ex);
      }
      finally
      {
        query.Clear();
      }
    }
    #endregion

  }
}
