﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace ISG.Common.Config
{
  public static class AppConfig
  {

    private static string _currencySymbolForDecimal = ConfigurationManager.AppSettings["CurrencySymbolForDecimal"].ReturnString().Trim();
    public static string CurrencySymbolForDecimal
    {
      get
      { return _currencySymbolForDecimal; }
    }

    private static string _currencySymbol = ConfigurationManager.AppSettings["CurrencySymbol"].ReturnString().Trim().ToUpper();
    public static string CurrencySymbol
    {
      get
      { return _currencySymbol; }
    }

    private static int _roundToDecimalPlaces = ConfigurationManager.AppSettings["RoundToDecimalPlaces"].ReturnString().ToInt();
    public static int RoundToDecimalPlaces
    {
      get
      { return _roundToDecimalPlaces; }
    }

    private static string _mailFormat = ConfigurationManager.AppSettings["MailFormat"].ReturnString();
    public static string MailFormat
    {
      get
      { return _mailFormat; }
    }

 


  }
}
