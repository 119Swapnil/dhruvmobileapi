﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISG.Common
{
  public enum CardType
  {
    MasterCard, BankCard, Visa, AmericanExpress, Discover, DinersClub, JCB, Others
  };

  public sealed class CardValidator
  {
    private CardValidator() { } // static only

    public static bool Validate(CardType cardType, string cardNumber)
    {
      byte[] number = new byte[16]; // number to validate

      // Remove non-digits
      int len = 0;
      for (int i = 0; i < cardNumber.Length; i++)
      {
        if (char.IsDigit(cardNumber, i))
        {
          if (len == 16) return false; // number has too many digits
          number[len++] = byte.Parse(cardNumber[i].ToString());
        }
      }

      // Validate based on card type, first if tests length, second tests prefix
      switch (cardType)
      {
        case CardType.MasterCard:
          if (len != 16)
            return false;
          if (number[0] != 5 || number[1] == 0 || number[1] > 5)
            return false;
          break;

        case CardType.BankCard:
          if (len != 16)
            return false;
          if (number[0] != 5 || number[1] != 6 || number[2] > 1)
            return false;
          break;

        case CardType.Visa:
          if (len != 16 && len != 13)
            return false;
          if (number[0] != 4)
            return false;
          break;

        case CardType.AmericanExpress:
          if (len != 15)
            return false;
          if (number[0] != 3 || (number[1] != 4 && number[1] != 7))
            return false;
          break;

        case CardType.Discover:
          if (len != 16)
            return false;
          if (number[0] != 6 || number[1] != 0 || number[2] != 1 || number[3] != 1)
            return false;
          break;

        case CardType.DinersClub:
          if (len != 14)
            return false;
          if (number[0] != 3 || (number[1] != 0 && number[1] != 6 && number[1] != 8)
             || number[1] == 0 && number[2] > 5)
            return false;
          break;

        case CardType.JCB:
          if (len != 16 && len != 15)
            return false;
          if (number[0] != 3 || number[1] != 5)
            return false;
          break;
      }

      // Use Luhn Algorithm to validate
      //LUHN Formula (Mod 10) for Validation of Credit Card Number

      //The following steps are required to validate the Credit Card number:

      //Step 1: Double the value of alternate digits of the Credit Card number beginning with the second digit from the right (the first right--hand digit is the check digit.)

      //Step 2: Add the individual digits comprising the products obtained in Step 1 to each of the unaffected digits in the original number.

      //Step 3: The total obtained in Step 2 must be a number ending in zero (30, 40, 50, etc.) for the account number to be validated.

      //For example, to validate the Credit Card number 49927398716:

      //Step 1:

      //        4 9 9 2 7 3 9 8 7 1 6
      //         x2  x2  x2  x2  x2 
      //------------------------------
      //         18   4   6  16   2




      //Step 2: 4 +(1+8)+ 9 + (4) + 7 + (6) + 9 +(1+6) + 7 + (2) + 6

      //Step 3: Sum = 70 : Card number is validated

      //Note: Card is valid because the 70/10 yields no remainder. 

      int sum = 0;
      for (int i = len - 1; i >= 0; i--)
      {
        if (i % 2 == len % 2)
        {
          int n = number[i] * 2;
          sum += (n / 10) + (n % 10);
        }
        else
          sum += number[i];
      }
      return (sum % 10 == 0);
    }

    public static CardType GetCardTypeFromCardTypeCode(string cardTypeCode)
    {
      CardType cardType;
      switch (cardTypeCode)
      {
        case "MC":
          cardType = CardType.MasterCard;
          break;
        case "VISA":
          cardType = CardType.Visa;
          break;
        case "AMEX":
          cardType = CardType.AmericanExpress;
          break;
        case "DC":
          cardType = CardType.DinersClub;
          break;
        case "JCB":
          cardType = CardType.JCB;
          break;
        default:
          cardType = CardType.Others;
          break;
      }
      return cardType;
    }
  }







}
